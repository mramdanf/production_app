<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_helper {

	const LBL_ON_PROGRESS = 'ON PROGRESS';
	const LBL_COMPLETE = 'COMPLETE';
	const SUCCESS = "success";
	const FAILED = "failed";
	const PRINTER_IP = "10.69.69.3";
	private $CI;

	function __construct()
	{
		$this->CI =& get_instance();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function shoot_api($data, $fields = null)
	{

		$ch = curl_init($data['url']);
		
		$username = $data['username'];
		$password = $data['password'];

		//url-ify the data for the POST
		$fields_string = '';
		if ($fields != null){
			foreach($fields as $key=>$value) { 
				$fields_string .= $key.'='.$value.'&'; 
			}
			rtrim($fields_string, '&');
		}
			

		curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);

		if ($fields != null) {
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		}
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$return = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
		curl_close($ch);

		if($status == 200){
			$data = json_decode($return, TRUE);
		} 
		else {
			$data = 'status_not_200';
		}

		return $data;

	}

	// will return date like -> 06 Nov 2016
	function dateSimpleForm($data = "")
	{
		return date("d M Y", strtotime($data));
	}

	// will return date like 2016-11-03
	function dateDbForm($data = "")
	{
		return date("Y-m-d", strtotime($data));
	}

	function dateTimeDbForm($data = "")
	{
		if($data == "")
		{
			return date("Y-m-d H:i:s");

		} else 
		{
			return date("Y-m-d H:i:s", strtotime($data));
		}
		
	}

	function dateTimeSimpleForm($data = "")
	{
		return date("d M Y H:i:s", strtotime($data));
	}

	function printArr($data)
	{
		echo "<pre>";
		print_r($data);
	}

	/*
		set_flash_data for alert
		flag: indicate type of alert
	*/
	function setFlash($flag = "", $msg)
	{
		if($flag != "")
		{
			if($flag == SELF::SUCCESS)
			{
				$this->CI->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>$msg,
				        'type'=>'alert-success'
				    )
				);
			} else
			{
				$this->CI->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>$msg,
				        'type'=>'alert-danger'
				    )
				);
			}
		}
	}

	/*
		print array to error log
		@params: array $data array to print
	*/
	function print_log($data)
	{
		log_message('error', print_r($data, TRUE));
	}

}

/* End of file app_helper.php */
/* Location: ./application/controllers/admin/app_helper.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once __DIR__ . '\escpos-php-development\autoload.php';

use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;

class Struct_Print
{
	private $CI;
	private $printer;
	private $ip_address = "10.69.69.3";
	private $port = "9100";
	private $connector;
	private $myutility;
	
	
	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('utility');
		// $this->myutility = $this->CI->utility;

	}

	private function connect()
	{
		$this->connector = new NetworkPrintConnector($this->ip_address, $this->port);
		$this->printer = new Printer($this->connector);
	}

	public function check_connection()
	{
		if (!$this->connector OR !$this->printer OR !is_a($this->printer, 'Mike42\Escpos\Printer')) 
		{
	    	throw new Exception("Tried to create receipt without being connected to a printer.");
	    }
	}

	public function close_after_exception()
	{
		if (isset($this->printer) && is_a($this->printer, 'Mike42\Escpos\Printer')) 
		{
		  $this->printer->close();
		}

		$this->connector = null;
		$this->printer = null;
		// $this->emc_printer = null;
	}

	// Calls printer->text and adds new line
  	private function add_line($text = "")
  	{
  		$this->printer->text($text."\n");
  	}

  	/*
		used on: 
			1. Print from dashboard cashier
			2. Print ketika pelunasan tagihan, end_cust_order page
  	*/
  	public function single_struct($order_id = NULL)
  	{
  		$this->CI->load->model('end_cust/m_income');

  		$order = $this->CI->m_income->get_order($order_id);

  		// its similar to ->row(), cz $order is array
  		$order = $order[0];

  		$title_heaven = "Heavenshard";
		$shop_add = "Jl. Kemang Utara IX No. 48-B, Duren Tiga,\n Pancoran Jakarta Selatan.";

  		$this->connect();

  		// struct header
		$this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
		$this->printer->setJustification(Printer::JUSTIFY_CENTER);
		$this->add_line($title_heaven);
		$this->printer->selectPrintMode();
		$this->add_line($shop_add);
		$this->printer->feed();		

		// Nomor order, waktu order, dan kasir
		$this->printer->setJustification(Printer::JUSTIFY_CENTER);
		$this->add_line("$order->order_id");

		$order->order_date = date("d/m/Y H:i:s", strtotime($order->order_date));
		$this->add_line("$order->order_date");

		$cashier = ($order->shift==0) ? utility::SHIFT_1 : utility::SHIFT_2;
		$this->add_line("Kasir: $cashier");
		$this->printer->feed();

		// Products
		$this->printer->setJustification(Printer::JUSTIFY_LEFT);
		$this->CI->load->library('Product');
		foreach ($order->subs as $order_detail) 
		{
			$this->printer -> text(new product(
					$order_detail->prod_name . " " . $order_detail->name, 
					$order_detail->detail_qty,
					$order_detail->harga,
					$order_detail->prod_subtotal
				)
			);

			// printing  disc. and decor

			// discount and decor was there
			if($order_detail->discount && $order_detail->total_decor)
			{
				$this->printer->text(new additional(
						"Decor",
						$order_detail->total_decor,
						"Disc.",
						$order_detail->discount
					)
				);

			} else if($order_detail->total_decor) // just decor
			{
				$this->printer->text(new additional(
						"Decor",
						$order_detail->total_decor
					)
				);

			} else if($order_detail->discount) // just discount
			{
				$this->printer->text(new additional(
						"Disc.",
						$order_detail->discount
					)
				);
			}
		}

		// Line devider
		$this->add_line(str_pad("-", 48, "-"));

		// decoration
		foreach ($order->subs as $order_detail) 
		{
			if(count($order_detail->decors) > 0)
			{
				$this->add_line($order_detail->prod_name . " " . $order_detail->name);

				foreach ($order_detail->decors as $decor) 
				{
					$this->printer -> text(new product(
							$decor->prod_name . " " . $decor->name, 
							$decor->qty,
							$decor->harga,
							$decor->sub_total
						)
					);
				}
			}
		}

		if($order->total_decor_qty > 0)		
		{
			// Line devider
			$this->add_line(str_pad("-", 48, "-"));
		}

		// order resume
		$this->printer->text(new product("Total Produk",$order->total_prod_qty,"",$order->dirty_total_prod));
		$this->printer->text(new product("Total Decor",$order->total_decor_qty,"",$order->dirty_total_decor));
		$this->printer->text(new item("Total Disc.",$order->total_disc));
		$this->printer->text(new item("Biaya Kirim",$order->ongkir));
		$this->printer->text(new item("Grand Total",$order->grand_total));
		$this->printer->text(new item("Tunai",$order->order_paid));
		$this->printer->text(new item("Kembali",$order->order_paid_return));

		if($order->order_type != 'Pickup')
		{
			// Line devider
			$this->add_line(str_pad("-", 48, "-"));

			$order_deliv_date = $this->CI->utility->dateTimeSimpleForm($order->order_deliv_date);
			$this->printer->text(new delivery("Nama Pembeli", $order->order_name));
			$this->printer->text(new delivery("Tlp Pembeli", $order->order_phone));
			$this->printer->text(new delivery("Nama Penerima", $order->order_recipient));
			$this->printer->text(new delivery("Tlp Penerima", $order->order_phone_recipient));
			$this->printer->text(new delivery("Tgl Pengiriman", $order_deliv_date));
			$this->printer->text(new delivery("Alamat", $order->order_add));
		}

		$this->printer->feed(2);

		$this->printer->setJustification(Printer::JUSTIFY_CENTER);
		$this->printer->setTextSize(1,1);
		$this->add_line("Terimakasih telah berbelanja di Heavenshard");
		$this->add_line("Informasi mengenai produk kami dapat \n dilihat pada www.heavenshard.com");

		$this->printer->cut();
		$this->printer->pulse();
		$this->printer->close();
  	}

  	/*
		used on:
			1. Cashier closing
			2. Print order dari end_cust_order page
		@params: mode-> 1: will print header and footer, 0: will not print
					header and footer
  	*/
  	public function multiple_struct($order_ids = NULL, $mode = 0)
  	{
  		$orders = $this->CI->m_income->get_orders();

  		// filtering $orders by selected $order_ids
  		// $result contain filtered orders
  		$result = array(); 
		foreach ($orders as $order) 
		{
			if(in_array($order->order_id, $order_ids))
			{
				$result [] = $order;
			}
		}

		$title_heaven = "Heavenshard";
		$shop_add = "Jl. Kemang Utara IX No. 48-B, Duren Tiga,\n Pancoran Jakarta Selatan.";

		/*$this->CI->utility->print_log($order_ids);
		$this->CI->utility->print_log($result);*/

  		$this->connect();

		foreach ($result as $order) 
		{
			if($mode)
			{
				// struct header
				$this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
				$this->printer->setJustification(Printer::JUSTIFY_CENTER);
				$this->add_line($title_heaven);
				$this->printer->selectPrintMode();
				$this->add_line($shop_add);
				$this->printer->feed();
			}			

			// Nomor order, waktu order, dan kasir
			$this->printer->setJustification(Printer::JUSTIFY_CENTER);
			$this->add_line("$order->order_id");

			$order->order_date = date("d/m/Y H:i:s", strtotime($order->order_date));
			$this->add_line("$order->order_date");

			$cashier = ($order->shift==0) ? "Shift Pagi":"Shift Siang";
			$this->add_line("Kasir: $cashier");
			$this->printer->feed();

			// Products
			$this->printer->setJustification(Printer::JUSTIFY_LEFT);
			foreach ($order->subs as $order_detail) 
			{
				$this->printer -> text(new product(
						$order_detail->prod_name . " " . $order_detail->name, 
						$order_detail->detail_qty,
						$order_detail->harga,
						$order_detail->prod_subtotal
					)
				);

				// printing  disc. and decor

				// discount and decor was there
				if($order_detail->discount && $order_detail->total_decor)
				{
					$this->printer->text(new additional(
							"Decor",
							$order_detail->total_decor,
							"Disc",
							$order_detail->discount
						)
					);

				} else if($order_detail->total_decor) // just decor
				{
					$this->printer->text(new additional(
							"Decor",
							$order_detail->total_decor
						)
					);

				} else if($order_detail->discount) // just discount
				{
					$this->printer->text(new additional(
							"Disc",
							$order_detail->discount
						)
					);
				}
			}

			// Line devider
			$this->add_line(str_pad("-", 48, "-"));

			// decoration
			foreach ($order->subs as $order_detail) 
			{
				if(count($order_detail->decors) > 0)
				{
					$this->add_line($order_detail->prod_name . " " . $order_detail->name);

					foreach ($order_detail->decors as $decor) 
					{
						$this->printer -> text(new product(
								$decor->prod_name . " " . $decor->name, 
								$decor->qty,
								$decor->harga,
								$decor->sub_total
							)
						);
					}
				}
			}

			if($order->total_decor_qty > 0)
			{				
				// Line devider
				$this->add_line(str_pad("-", 48, "-"));
			}

			// order resume
			$this->printer
				->text(new product("Total Produk",$order->total_prod_qty,"",$order->dirty_total_prod));
			$this->printer
				->text(new product("Total Decor",$order->total_decor_qty,"",$order->dirty_total_decor));
			$this->printer->text(new item("Total Disc.",$order->total_disc));
			$this->printer->text(new item("Biaya Kirim",$order->ongkir));
			$this->printer->text(new item("Grand Total",$order->grand_total));
			$this->printer->text(new item("Tunai",$order->order_paid));
			$this->printer->text(new item("Kembali",$order->order_paid_return));

			if($order->order_type != 'Pickup')
			{
				// Line devider
				$this->add_line(str_pad("-", 48, "-"));

				$order_deliv_date = $this->CI->utility->dateTimeSimpleForm($order->order_deliv_date);
				$this->printer->text(new delivery("Nama Pembeli", $order->order_name));
				$this->printer->text(new delivery("Tlp Pembeli", $order->order_phone));
				$this->printer->text(new delivery("Nama Penerima", $order->order_recipient));
				$this->printer->text(new delivery("Tlp Penerima", $order->order_phone_recipient));
				$this->printer->text(new delivery("Tgl Pengiriman", $order_deliv_date));
				$this->printer->text(new delivery("Alamat", $order->order_add));
				$this->printer->text(new delivery("Biaya Kirim", $order->ongkir));
			}

			$this->printer->feed(2);

			if ($mode) 
			{
				$this->printer->setJustification(Printer::JUSTIFY_CENTER);
				$this->printer->setTextSize(1,1);
				$this->add_line("Terimakasih telah berbelanja di Heavenshard");
				$this->add_line("Informasi mengenai produk kami dapat \n dilihat pada www.heavenshard.com");
				$this->printer->selectPrintMode(); // reset
			}
		}

		$this->printer->cut();
		$this->printer->close();			
  	}


}

class product
{
    private $product_name;
    private $qty;
    private $price;
    private $sub_total;

    public function __construct($product_name='', $qty="", 
    		$price="", $sub_total="")
    {
        $this->product_name = $product_name;
        $this->qty = $qty;
        $this->price = $price;
        $this->sub_total = $sub_total;
    }

    public function __toString()
    {
        $product_name_col = 25;
        $qty_col = 3;
        $price_col = 10;
        $sub_total_col = 10;

        $product_name_res = str_pad($this->product_name, $product_name_col, " ");
        $qty_res = str_pad($this->qty, $qty_col, " ");
        $price_res = str_pad($this->price, $price_col, " ",STR_PAD_LEFT);
        $sub_total_res = str_pad($this->sub_total, $sub_total_col, " ",STR_PAD_LEFT);

        return "$product_name_res$qty_res$price_res$sub_total_res\n";


    }
}

class item
{
    private $name;
    private $price;
    private $dollarSign;

    public function __construct($name = '', $price = '', $dollarSign = false)
    {
        $this -> name = $name;
        $this -> price = $price;
        $this -> dollarSign = $dollarSign;
    }

    public function __toString()
    {
        $rightCols = 10;
        $leftCols = 38;
        if ($this -> dollarSign) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> name, $leftCols) ;

        $sign = ($this -> dollarSign ? '$ ' : '');
        $right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}

class additional
{
    private $decor;
    private $price_decor;
    private $disc;
    private $price_disc;

    public function __construct($decor='', $price_decor=0, 
    	$disc='', $price_disc=0)
    {
        $this->decor = $decor;
        $this->price_decor = $price_decor;
        $this->disc = $disc;
        $this->price_disc = $price_disc;
    }

    public function __toString()
    {
    	$decor_cols = 6; // including one space after word (Decor )
		$price_decor_cols = 9; // 1,000,000
		$disc_cols = 6;
		$price_disc_cols = 9;

        $decor_res = str_pad($this->decor, $decor_cols, " ");
        $price_decor_res = str_pad($this->price_decor, $price_decor_cols, " ");
        $disc_res = str_pad($this->disc, $disc_cols, " ",STR_PAD_LEFT);
        $price_disc_res = str_pad($this->price_disc, $price_disc_cols, " ",STR_PAD_LEFT);

        if($this->decor != "" && $this->disc != "")
        {
        	return "$decor_res$price_decor_res$disc_res$price_disc_res\n";

        }else if ($this->decor != "")
        {
        	return "$decor_res$price_decor_res\n";

        }else if($this->disc != "")
        {
        	return "$disc_res$price_disc_res\n";
        }
    }
}

class Delivery
{
    private $text;
    private $data;

    public function __construct($text='', $data='')
    {
        $this->text = $text;
        $this->data = $data;
    }

    public function __toString()
    {
        $text_cols = 17; // including one space after word (Decor )
        $data_cols = 31; 

        $text_res = str_pad($this->text, $text_cols, " ");
        $data_res = str_pad($this->data, $data_cols, " ");

        return "$text_res$data_res\n";


    }
}
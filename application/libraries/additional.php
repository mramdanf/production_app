<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Additional
{
    private $decor;
    private $price_decor;
    private $disc;
    private $price_disc;

    public function __construct($decor='', $price_decor=0, 
        $disc='', $price_disc=0)
    {
        $this->decor = $decor;
        $this->price_decor = $price_decor;
        $this->disc = $disc;
        $this->price_disc = $price_disc;
    }

    public function __toString()
    {
        $decor_cols = 6; // including one space after word (Decor )
        $price_decor_cols = 9; // 1,000,000
        $disc_cols = 6;
        $price_disc_cols = 9;

        $decor_res = str_pad($this->decor, $decor_cols, " ");
        $price_decor_res = str_pad($this->price_decor, $price_decor_cols, " ");
        $disc_res = str_pad($this->disc, $disc_cols, " ",STR_PAD_LEFT);
        $price_disc_res = str_pad($this->price_disc, $price_disc_cols, " ",STR_PAD_LEFT);

        return "$decor_res$price_decor_res$disc_res$price_disc_res\n";


    }
}
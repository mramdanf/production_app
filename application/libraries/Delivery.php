<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Delivery
{
    private $text;
    private $data;

    public function __construct($text='', $data='')
    {
        $this->text = $text;
        $this->data = $data;
    }

    public function __toString()
    {
        $text_cols = 15; // including one space after word (Decor )
        $data_cols = 33; 

        $text_res = str_pad($this->text, $text_cols, " ");
        $data_res = str_pad($this->data, $data_cols, " ");

        return "$text_res$data_res\n";


    }
}
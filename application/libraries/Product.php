<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Product
{
	
	private $product_name;
    private $qty;
    private $price;
    private $sub_total;

    public function __construct($product_name='', $qty="", 
    		$price="", $sub_total="")
    {
        $this->product_name = $product_name;
        $this->qty = $qty;
        $this->price = $price;
        $this->sub_total = $sub_total;
    }

    public function __toString()
    {
        $product_name_col = 25;
        $qty_col = 3;
        $price_col = 10;
        $sub_total_col = 10;

        $product_name_res = str_pad($this->product_name, $product_name_col, " ");
        $qty_res = str_pad($this->qty, $qty_col, " ");
        $price_res = str_pad($this->price, $price_col, " ",STR_PAD_LEFT);
        $sub_total_res = str_pad($this->sub_total, $sub_total_col, " ",STR_PAD_LEFT);

        return "$product_name_res$qty_res$price_res$sub_total_res\n";


    }
}
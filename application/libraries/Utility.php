<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Utility
{
	private $CI;

	const PICKUP = "Pickup";
	const SHIFT_1 = "Shift Satu";
	const SHIFT_2 = "Shift Dua";
	const SUCCESS = "success";
	const FAILED = "failed";

	function __construct()
	{
		$this->CI =& get_instance();
		date_default_timezone_set("Asia/Jakarta");
	}

	/*
		print array to error log
		@params: array $data array to print
	*/
	function print_log($data)
	{
		log_message('error', print_r($data, TRUE));
	}

	/*
		change datetime format from 2016-11-26 12:00:00
		to 26 Nov 2016 12:00:00
		@params: datetime $data date format like: 2016-11-26 12:00:00
	*/
	function dateTimeSimpleForm($data = "")
	{
		return date("d M Y H:i:s", strtotime($data));
	}

	/*
		set_flash_data for alert
		flag: indicate type of alert
	*/
	function set_flash($flag = "", $msg)
	{
		if($flag != "")
		{
			if($flag == SELF::SUCCESS)
			{
				$this->CI->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>$msg,
				        'type'=>'alert-success'
				    )
				);
			} else
			{
				$this->CI->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>$msg,
				        'type'=>'alert-danger'
				    )
				);
			}
		}
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fitur extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/m_fitur');
	}

	public function index()
	{
		$data['fiturs'] = $this->m_fitur->get_fiturs();
		$this->load->view('admin/app_header');
		$this->load->view('admin/fitur', $data);
		$this->load->view('admin/app_footer');
	}

	function activate($id)
	{
		if($this->m_fitur->activate($id))
		{
			$this->session->set_flashdata(
			    'alert', 
			    array(
			        'msg'=>'Fitur berhasil diaktifkan.',
			        'type'=>'alert-success'
			    )
			);
		} else
		{
			$this->session->set_flashdata(
			    'alert', 
			    array(
			        'msg'=>'Terjadi error. fitur gagal diaktifkan',
			        'type'=>'alert-danger'
			    )
			);
		}

		redirect('admin/fitur');
	}

	function inactivate($id)
	{
		if($this->m_fitur->inactivate($id))
		{
			$this->session->set_flashdata(
			    'alert', 
			    array(
			        'msg'=>'Fitur berhasil dinon-aktifkan.',
			        'type'=>'alert-success'
			    )
			);
		} else
		{
			$this->session->set_flashdata(
			    'alert', 
			    array(
			        'msg'=>'Terjadi error. fitur gagal dinon-aktifkan',
			        'type'=>'alert-danger'
			    )
			);
		}

		redirect('admin/fitur');
	}

}

/* End of file Fitur.php */
/* Location: ./application/controllers/admin/Fitur.php */
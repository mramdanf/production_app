<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign_in extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('admin/model_dashboard');
	}

	public function index(){
		$priv = $this->session->userdata('privilage');
		if ($priv != 'admin') {
			$this->load->view('admin/admin_signin');
		} else {
			redirect(base_url('admin/dashboard'));
		}
	}

	function check_admin(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$res = $this->model_dashboard->check_admin($username, $password);

		if ($res == 'admin_found') {
			$this->session->set_userdata('privilage', 'admin');
			redirect(base_url('admin/dashboard'));
		} else {
			$this->session->set_flashdata('alert_msg', 'Username atau password salah.');
			redirect(base_url('admin/sign_in'));

		}
	}

	function tes_data(){
		$this->session->unset_userdata('privilage');
	}

	


}

/* End of file sign_in.php */
/* Location: ./application/controllers/admin/sign_in.php */
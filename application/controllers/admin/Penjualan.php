<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	function __construct(){
		parent::__construct();

		$priv = $this->session->userdata('privilage');
		if ($priv != 'admin') {
			redirect(base_url('admin/sign_in'));
		}

		$this->load->model('admin/model_penjualan');
	}

	public function index()
	{
		$data['penjualan'] = $this->model_penjualan->penjualan();

		$this->load->view('admin/app_header');
		$this->load->view('admin/penjualan', $data);
		$this->load->view('admin/app_footer');
	}

	function report_penjualan(){

		$data['penjualan'] = $this->model_penjualan->penjualan();

		$html=$this->load->view('admin/report_penjualan', $data, true); 

		$pdfFilePath = "report_penjualan.pdf";
		$this->load->library('m_pdf');
		$pdf = $this->m_pdf->load();
		$pdf->WriteHTML($html);
		$pdf->Output();
	}

}

/* End of file Penjualan.php */
/* Location: ./application/controllers/admin/Penjualan.php */
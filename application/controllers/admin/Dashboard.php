<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('admin/model_dashboard');

		$priv = $this->session->userdata('privilage');
		if ($priv != 'admin') {
			redirect(base_url('admin/sign_in'));
		}
	}

	function admin_pass($new_pass='')
	{
		$res=$this->db->update('user_table', array('password'=>md5($new_pass)), array('username'=>'heaven'));
		echo (true == $res) ? 'update sukses' : 'update gagal';
	}

	public function index()
	{
		$data['users'] = $this->model_dashboard->users();

		$this->load->view('admin/app_header');
		$this->load->view('admin/dashboard', $data);
		$this->load->view('admin/app_footer');
		
	}

	function del_user(){
		$user_table_id = $this->input->post('user_table_id');
		$first_name = $this->input->post('first_name');

		$this->model_dashboard->del_user($user_table_id);

		$this->session->set_flashdata('alert_msg', 'User <strong>'.$first_name.'</strong> berhasil dihapus.');
	}

	function add_user(){
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$username = $this->input->post('username');
		$password = 123;
		$privilage = $this->input->post('privilage');

		$input_user = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'username' => $username,
			'password' => md5($password),
			'privilage' => $privilage
			);

		$is_username_existing = $this->model_dashboard->check_existing_username($username);
		if ($is_username_existing) {
			$this->session->set_flashdata('alert_msg', 'Gagal menambahkan user, Username <strong>'.$username.'</strong> sudah terdaftar.');
			$this->session->set_flashdata('alert_type', 'danger');
		} else {
			$this->model_dashboard->add_user($input_user);
			$this->session->set_flashdata('alert_msg', 'User <strong>'.$first_name.'</strong> berhasil ditambahkan.');
		}

		
		redirect(base_url('admin/dashboard'));
	}

	function edit_user($user_table_id){

		$data['user_details'] = $this->model_dashboard->user_details($user_table_id);

		$this->load->view('admin/app_header');
		$this->load->view('admin/user_details', $data);
		$this->load->view('admin/app_footer');
	}

	function do_edit(){
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$username = $this->input->post('username');
		$privilage = $this->input->post('privilage');
		$user_table_id = $this->input->post('user_table_id');

		$update_user = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'username' => $username,
			'privilage' => $privilage
			);

		$is_username_existing = $this->model_dashboard->check_update_username($username, $user_table_id);
		if ($is_username_existing) {
			$this->session->set_flashdata('alert_msg', 'Gagal merubah user, Username <strong>'.$username.'</strong> sudah terdaftar.');
			$this->session->set_flashdata('alert_type', 'danger');
		} else {
			$this->model_dashboard->do_edit($update_user, $user_table_id);
			$this->session->set_flashdata('alert_msg', 'Data User berhasil diupdate.');
		}

		
		redirect(base_url('admin/dashboard'));
	}

	function sign_out(){
		$this->session->unset_userdata('privilage');
		redirect(base_url('admin/sign_in'));
	}

	function reset_pass($user_table_id){
		$data = array(
			'password' => md5(123)
			);
		$this->model_dashboard->reset_pass($user_table_id, $data);
		$this->session->set_flashdata('alert_msg', 'Reset password berhasil, password kembali ke default yaitu (<storng>123</strong>).');
		redirect(base_url('admin/dashboard'));
	}

	function tes_data(){
		// echo md5("sukses");
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/admin/Dashboard.php */
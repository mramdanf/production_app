<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/m_discount');
	}

	public function index()
	{
		$data['discounts'] = $this->m_discount->get_discounts();
		$this->load->view('admin/app_header');
		$this->load->view('admin/discount', $data);
		$this->load->view('admin/app_footer');
	}

	function form($id = NULL)
	{
		if($id != NULL)
		{
			$data['discount'] = $this->m_discount->get_discount($id)->row();
			$this->load->view('admin/app_header');
			$this->load->view('admin/discount_form', $data);
			$this->load->view('admin/app_footer');
		}
	}

	function modify()
	{
		$id = $this->input->post('id');
		$data['jumlah'] = $this->input->post('jumlah');
		$data['jenis'] = $this->input->post('jenis');

		date_default_timezone_set("Asia/Jakarta");
		$data['last_update'] = date("Y-m-d H:m:s");
		
		if($id != NULL) 
		{
			if($this->m_discount->update_discount($id, $data))
			{
				$this->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>'Data discount berhasil diupdate.',
				        'type'=>'alert-success'
				    )
				);
			} else 
			{
				$this->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>'Terjadi error, data disocount gagal diupdate.',
				        'type'=>'alert-danger'
				    )
				);
			}
		}

		redirect('admin/discount');
	}

	function activate($id = NULL)
	{
		if($id != NULL)
		{
			if($this->m_discount->do_activate($id))
			{
				$this->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>'Discount berhasil diaktifkan.',
				        'type'=>'alert-success'
				    )
				);
			} else 
			{
				$this->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>'Terjadi error, discount gagal diaktifkan.',
				        'type'=>'alert-success'
				    )
				);
			}
		}

		redirect('admin/discount');
	}

	function inactivate($id = NULL)
	{
		if($id != NULL)
		{
			if($this->m_discount->do_unactivate($id))
			{
				$this->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>'Discount berhasil non-aktifkan.',
				        'type'=>'alert-success'
				    )
				);
			} else 
			{
				$this->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>'Terjadi error, discount gagal non-aktifkan.',
				        'type'=>'alert-success'
				    )
				);
			}
		}

		redirect('admin/discount');
	}

}

/* End of file Discount.php */
/* Location: ./application/controllers/admin/Discount.php */
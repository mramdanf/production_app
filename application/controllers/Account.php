<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('model_account');
	}

	public function index()
	{
		$this->load->view('change_pass');
	}

	function change_pass(){
		$new_password = $this->input->post('new_password');
		$old_password = $this->input->post('old_password');
		$username = $this->input->post('username');

		$data = array(
			'password' => md5($new_password)
			);

		$is_username_registered = $this->model_account->check_username_registered($username);
		if ($is_username_registered) {
			$this->model_account->change_pass($username, $data);
			$this->session->set_flashdata('alert_msg', 'Password berhasil dirubah, silahkan coba untuk login.');
		} else {
			$this->session->set_flashdata('alert_msg', 'Change password failed, username tidak terdaftar.');
			$this->session->set_flashdata('alert_type', 'danger');
		}

		redirect(base_url('account'));
	}

}

/* End of file Account.php */
/* Location: ./application/controllers/Account.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_Visual extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('production/m_prodvis');
	}

	public function index()
	{
		// if $this->input->get("categorie_id"); set $categorie_id to 1 (default value)
		$categorie_id = $this->input->get("categorie_id");
		$categorie_id = ($categorie_id) ? $categorie_id : 2;

		$collections = $this->m_prodvis->product_collections($categorie_id);

		// categories for dropdown
		$this->load->model('production/model_products');
		$data['categories'] = $this->model_products->get_categories();
		
		$data["categorie_id"] = $categorie_id;
		
		// $data['products'] for tbl-products
		$data['products'] = $collections["products"];
		
		// for header of tbl-products
		$data['properties'] = $collections["properties"];

		$header_data['title'] = "Produksi | Products Visual";

		$this->load->view('production/app_header', $header_data);
		$this->load->view('production/products_visual', $data);
		$this->load->view('production/app_footer');
	}

}

/* End of file Products_Visual.php */
/* Location: ./application/controllers/production/Products_Visual.php */
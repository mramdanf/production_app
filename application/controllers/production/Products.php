<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('production/model_products');

		$priv = $this->session->userdata('privilage');
		if ($priv != 'Produksi') 
		{
			redirect(base_url('production/sign_in'));
		}
	}

	public function index()
	{
		$data['products'] = $this->model_products->data_products();
		$data['categories'] = $this->model_products->get_categories();

		$header_data['title'] = "Produksi | Products";

		$this->load->view('production/app_header', $header_data);
		$this->load->view('production/products', $data);
		$this->load->view('production/app_footer');
	}

	function del_product()
	{
		$prod_id = $this->input->post('prod_id');
		$bel_id = $this->input->post('bel_id');
		$prod_name = $this->input->post('prod_name');

		$this->model_products->del_product($prod_id, $bel_id);

		$this->session->set_flashdata(
		    'alert', 
		    array(
		        'msg'=>'Produk <strong>'.$prod_name.'</strong> berhasil hapus',
		        'type'=>'alert-success'
		    )
		);
	}

	function form($bel_id = NULL)
	{
		$data['categories'] = $this->model_products->get_categories();

		if($bel_id != NULL)
		{
			$data['product'] = $this->model_products->prod_details($bel_id)->row();
		}

		$header_data['title'] = "Produksi | Products";

		$this->load->view('production/app_header', $header_data);
		$this->load->view('production/form_product', $data);
		$this->load->view('production/app_footer');
	}

	function modify()
	{
		// ini id di tabel belong_to
		$id = $this->input->post('id');

		// for table products
		$prod_name = $this->input->post('prod_name');
		$prod_name = ucwords(strtolower($prod_name));

		$harga =  $this->input->post('harga');
		if(preg_match("/^[0-9,]+$/", $harga)) $harga = str_replace(',','',$harga);

		$stocks = $this->input->post('stocks');
		$description = $this->input->post('description');
		$description = ucfirst(strtolower($description));

		$max_bel_id = $this->input->post('bel_id');
		date_default_timezone_set("Asia/Jakarta");
		
		$input_product = array(
			'prod_name' => $prod_name,
			'harga' => $harga,
			'stocks' => $stocks,
			'discount' => $this->input->post('discount')/100,
			'description' => $description,
			'last_update' => date("Y-m-d H:i:s")
			);

		$row_found = $this->model_products->prod_details($max_bel_id)->num_rows();

		if($id == NULL) // tambah produk
		{
			// cek dulu bel_idnya (karena bel_id diinput manual)	
			if($row_found <= 0)
			{
				if($this->model_products->add_product($input_product))
				{
					// for table belong_to
					$cat_id = filter_var($this->input->post('cat_name'), FILTER_SANITIZE_NUMBER_INT);
					$product_id = $this->model_products->max_prod_id();

					$input_belong = array(
						'categori_id' => $cat_id,
						'bel_id' => $max_bel_id,
						'product_id' => $product_id,
						'properties_id' => $this->input->post('prop_name')
						);

					if($this->model_products->add_belong_to($input_belong))
					{
						$this->session->set_flashdata(
						    'alert', 
						    array(
						        'msg'=>'Data berhasil ditambahkan.',
						        'type'=>'alert-success'
						    )
						);
					} else 
					{
						$this->session->set_flashdata(
						    'alert', 
						    array(
						        'msg'=>'Gagal memasangkan produk dengan kategori.',
						        'type'=>'alert-success'
						    )
						);
					}

				} else 
				{
					$this->session->set_flashdata(
						    'alert', 
						    array(
						        'msg'=>'Gagal menambahkan produk.',
						        'type'=>'alert-success'
						    )
						);
				}
			} else 
			{
				$this->session->set_flashdata(
						    'alert', 
						    array(
						        'msg'=>'Gagal menambahkan produk, <strong>Kode produk sudah terdaftar di database.</strong>',
						        'type'=>'alert-danger'
						    )
						);
			}

		} else // edit produk
		{
			$product_id = $this->input->post('product_id');

			if($this->input->post('bel_id') != $this->input->post('last_bel_id')) // bel_id dirubah 
			{
				if($row_found <= 0)
				{
					if($this->model_products->update_product2($product_id, $input_product))
					{
						// for table belong_to
						$cat_id = filter_var($this->input->post('cat_name'), FILTER_SANITIZE_NUMBER_INT);

						$input_belong = array(
							'categori_id' => $cat_id,
							'bel_id' => $max_bel_id,
							'product_id' => $product_id,
							'properties_id' => $this->input->post('prop_name')
							);

						if($this->model_products->update_belongto($id, $input_belong))
						{
							$this->session->set_flashdata(
							    'alert', 
							    array(
							        'msg'=>'Data berhasil ditambahkan.',
							        'type'=>'alert-success'
							    )
							);
						} else 
						{
							$this->session->set_flashdata(
							    'alert', 
							    array(
							        'msg'=>'Gagal memasangkan produk dengan kategori.',
							        'type'=>'alert-success'
							    )
							);
						}

					} else 
					{
						$this->session->set_flashdata(
							    'alert', 
							    array(
							        'msg'=>'Gagal menambahkan produk.',
							        'type'=>'alert-success'
							    )
							);
					}
				} else 
				{
					$this->session->set_flashdata(
							    'alert', 
							    array(
							        'msg'=>'Gagal menambahkan produk, <strong>Kode produk sudah terdaftar di database.</strong>',
							        'type'=>'alert-danger'
							    )
							);
				}
			} else // bel_id tidak dirubah
			{
				if($this->model_products->update_product2($product_id, $input_product))
				{
					// for table belong_to
					$cat_id = filter_var($this->input->post('cat_name'), FILTER_SANITIZE_NUMBER_INT);

					$input_belong = array(
						'categori_id' => $cat_id,
						'bel_id' => $max_bel_id,
						'product_id' => $product_id,
						'properties_id' => $this->input->post('prop_name')
						);

					if($this->model_products->update_belongto($id, $input_belong))
					{
						$this->session->set_flashdata(
						    'alert', 
						    array(
						        'msg'=>'Data berhasil ditambahkan.',
						        'type'=>'alert-success'
						    )
						);
					} else 
					{
						$this->session->set_flashdata(
						    'alert', 
						    array(
						        'msg'=>'Gagal memasangkan produk dengan kategori.',
						        'type'=>'alert-success'
						    )
						);
					}

				} else 
				{
					$this->session->set_flashdata(
						    'alert', 
						    array(
						        'msg'=>'Gagal menambahkan produk.',
						        'type'=>'alert-success'
						    )
						);
				}
			}	
		}

		redirect('production/products');
	}

	function get_properties_cat()
	{
		$cat_id = $this->input->post('cat_id');

		if($cat_id != NULL)
		{
			$properties_cat = $this->model_products->get_propertie($cat_id)->result();

			$response = array(
				'success'=>true,
				'data'=>$properties_cat
				);
		} else 
		{
			$response = array(
				'success'=>false,
				'data'=>[]
				);
		}

		$this->output
	      ->set_content_type('application/json', 'utf-8')
	      ->set_output(json_encode($response, JSON_PRETTY_PRINT))
	      ->_display();
	    exit;
	}

	function test()
	{
		$vocal = array('a','i','u','e','o');
		
		$records = $this->db->get('products');

		if($records->num_rows() > 0)
		{
			foreach ($records->result() as $record) 
			{
				echo str_replace($vocal, "", $record->prod_name). "<br>";
			}
		}
	}

}

/* End of file Products.php
/* Location: ./application/controllers/production/Products.php */
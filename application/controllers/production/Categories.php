<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

	private $tbl_categories = 'categori';
	private $tbl_properties = 'properties';

	function __construct()
	{
		parent::__construct();

		$this->load->model('production/model_categories');

		$priv = $this->session->userdata('privilage');
		
		if ($priv != 'Produksi') 
		{
			redirect(base_url('production/sign_in'));
		} 
	}

	public function index()
	{
		$data['categories'] = $this->model_categories->get_categories();
		$this->db
			->select("*")
			->from($this->tbl_properties)
			->join($this->tbl_categories, 'cat_id=categori_id');
		$data['properties'] = $this->db->get()->result();

		$header_data['title'] = "Produksi | Kategori";

		$this->load->view('production/app_header', $header_data);
		$this->load->view('production/categories', $data);
		$this->load->view('production/app_footer');
	}

	function form($cat_id = NULL)
	{
		$data = NULL;

		if($cat_id != NULL)
		{
			$data['category'] = $this->model_categories->get_category($cat_id)->row();
			$this->db
				->select("*")
				->from($this->tbl_properties)
				->join($this->tbl_categories, 'cat_id=categori_id')
				->where('categori_id',$cat_id);
			$data['properties'] = $this->db->get()->result();
		}

		$header_data['title'] = "Produksi | Kategori";

		$this->load->view('production/app_header', $header_data);
		$this->load->view('production/form_cat', $data);
		$this->load->view('production/app_footer');
	}

	function modify()
	{
		$cat_id = $this->input->post('cat_id');
		$input_cat = array(
			'cat_name' => ucwords(strtolower($this->input->post('cat_name')))
			);

		if($cat_id == NULL)
		{
			if($this->model_categories->add_cat($input_cat))
			{
				$this->db->select_max('cat_id');

				$last_cat_id = ($this->db->get($this->tbl_categories)->row()->cat_id);

				// $this->utility->print_log($this->input->post('propertie'));

				if($this->input->post('propertie'))
				{
					$properties = array_filter($this->input->post('propertie'));
					$this->utility->print_log($properties);

					foreach ($properties as $propertie) 
					{
						$this->db->insert(
							$this->tbl_properties, 
							array('categori_id'=>$last_cat_id, 'name'=>$propertie)
						);
					}
				}

				$this->utility->set_flash(utility::SUCCESS, "Data berhasil disimpan");

			} else 
			{
				$this->session->set_flashdata(
					'alert', 
					array(
						'msg'=>'Gagal menambahkan kategori',
						'type'=>'alert-danger'
					)
				);
			}
		} else 
		{
			if($this->model_categories->update_cat($cat_id, $input_cat))
			{
				if($this->model_categories->del_properties_cat($cat_id)) 
				{
					if($this->input->post('propertie'))
					{
						$properties = array_filter($this->input->post('propertie'));
						foreach ($properties as $propertie) 
						{
							$this->db->insert(
								$this->tbl_properties, 
								array('categori_id'=>$cat_id, 'name'=>$propertie)
							);
						}
					}

					$this->session->set_flashdata(
						'alert', 
						array(
							'msg'=>'Kategori <strong>'.$cat_name.'</strong> berhasil dirubah.',
							'type'=>'alert-success'
						)
					);
				}
			} else 
			{
				$this->session->set_flashdata(
						'alert', 
						array(
							'msg'=>'Gagal update kategori',
							'type'=>'alert-danger'
						)
					);
			}
		}
		redirect(base_url('production/categories'));
		
	}

	function del_cat(){
		$cat_id = $this->input->post('cat_id');
		$cat_name = $this->input->post('cat_name');

		// del_cat also del properties base on $cat_id
		if($this->model_categories->del_cat($cat_id))
		{
			$this->session->set_flashdata(
				'alert', 
				array(
					'msg'=>'Kategori <strong>'.$cat_name.'</strong> berhasil dihapus.',
					'type'=>'alert-success'
				)
			);
		} else 
		{
			$this->session->set_flashdata(
				'alert', 
				array(
					'msg'=>'Terjadi error, kategori gagal dihapus',
					'type'=>'alert-success'
				)
			);
		}
	}

	function properties()
	{
		$data['properties'] = $this->model_categories->categories_properties();
		$data['categories'] = $this->model_categories->get_categories();
		
		$header_data['title'] = "Produksi | Properti";

		$this->load->view('production/app_header', $header_data);
		$this->load->view('production/properties', $data);
		$this->load->view('production/app_footer');
	}

	/*function add_cat()
	{
		$cat_name = $this->input->post('cat_name');
		$cat_name = ucwords(strtolower($cat_name));
		$input_cat = array(
			'cat_name' => $cat_name
			);
		if($this->model_categories->add_cat($input_cat))
		{
			$this->db->select_max('cat_id');
			$last_cat_id = ($this->db->get($this->tbl_categories)->row()->cat_id);

			if($this->input->post('propertie'))
			{
				$properties = array_filter($this->input->post('propertie'));
				foreach ($properties as $propertie) 
				{
					$this->db->insert(
						$this->tbl_properties, 
						array('categories_id'=>$last_cat_id, 'name'=>$propertie)
					);
				}

				$this->session->set_flashdata(
					'alert', 
					array(
						'msg'=>'Kategori <strong>'.$cat_name.'</strong> berhasil ditambahkan.',
						'type'=>'alert-success'
					)
				);
			}

		} else 
		{
			$this->session->set_flashdata(
				'alert', 
				array(
					'msg'=>'Gagal menambahkan kategori.',
					'type'=>'alert-danger'
				)
			);
		}
		
		redirect(base_url('production/categories'));
		
	}

	function edit($cat_id){
		$data['category'] = $this->model_categories->get_category($cat_id)->row();
		$this->db
			->select("*")
			->from($this->tbl_properties)
			->join($this->tbl_categories, 'cat_id=categori_id')
			->where('categori_id',$cat_id);
		$data['properties'] = $this->db->get()->result();

		$this->load->view('production/app_header');
		$this->load->view('production/edit_cat', $data);
		$this->load->view('production/app_footer');
	}

	function do_edit(){
		$cat_id = $this->input->post('cat_id');
		$cat_name = $this->input->post('cat_name');
		$cat_name = ucwords(strtolower($cat_name));

		$update_cat = array(
			'cat_name' => $cat_name
			);

		$this->model_categories->update_cat($cat_id, $update_cat);
		$this->session->set_flashdata('alert_msg', 'Kategori berhasil diupdate.');
		redirect(base_url('production/categories'));
	}*/

	

}

/* End of file categories.php */
/* Location: ./application/controllers/production/categories.php */
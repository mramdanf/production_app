<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_Income extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('production/m_products_income');

		$priv = $this->session->userdata('privilage');
		if ($priv != 'Produksi') 
		{
			redirect(base_url('production/sign_in'));
		}
	}

	public function index()
	{
		$to_day = date("Y-m-d");
		$data['records'] = $this->m_products_income->sold_by_date($to_day);
		$data['date'] = $this->app_helper->dateSimpleForm($to_day);

		$header_data['title'] = "Produksi | Prod. Terjual";

		$this->load->view('production/app_header', $header_data);
		$this->load->view('production/products_income', $data);
		$this->load->view('production/app_footer');
	}

	function search()
	{
		$key_date = $this->input->get('key_date');
		$is_today = $this->input->get('is_today');

		if($is_today)
		{
			$search_filter = date("Y-m-d");
			$records = $this->m_products_income->sold_by_date($search_filter);

			$search_filter = $this->app_helper->dateSimpleForm($search_filter);

		} else if($key_date)
		{
			$search_filter = $key_date;
			$search_filter = $this->app_helper->dateDbForm($search_filter);
			$records = $this->m_products_income->sold_by_date($search_filter);

			// kemabikan lagi search filter ke bentuk simpleFormdate
			$search_filter = $key_date;
		}

		$data['records'] = $records;
		$data['date'] = $search_filter;

		$this->load->view('production/app_header');
		$this->load->view('production/products_income', $data);
		$this->load->view('production/app_footer');

	}

}

/* End of file Products_Income.php */
/* Location: ./application/controllers/production/Products_Income.php */
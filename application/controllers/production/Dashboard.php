<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/*
	Attentions:
		1. Jangan bingung, data pada tabel order_detail bisa (ga tau kenapa) dipopulate
		   dengan ajax ataupun dengan mengakses fungsi get_order_details.
	*/

	function __construct()
	{
		parent::__construct();
		$this->load->model('production/model_dashboard');

		$priv = $this->session->userdata('privilage');
		if ($priv != 'Produksi') 
		{
			redirect(base_url('production/sign_in'));
		}

		
	}

	public function index()
	{
		$data['waiting_orders'] = $this->model_dashboard->waiting_orders();
		$data['order_details'] = $this->model_dashboard->order_details(null);
		$data['order_id'] = '';

		$header_data['title'] = "Produksi | Dashboard";

		$this->load->view('production/app_header', $header_data);
		$this->load->view('production/dashboard', $data);
		$this->load->view('production/app_footer');
	}

	function get_order_details($order_id)
	{
		$order_id = str_replace("_", "/", $order_id);
		$data['waiting_orders'] = $this->model_dashboard->waiting_orders();
		$data['order_details'] = $this->model_dashboard->order_details($order_id);
		
		if ($data['order_details'] != null) 
		{
			$data['order_id'] = $order_id;

		} else {
			
			$data['order_id'] = '';
		}

		$this->utility->print_log($order_id);
		

		$header_data['title'] = "Produksi | Dashboard";

		$this->load->view('production/app_header', $header_data);
		$this->load->view('production/dashboard', $data);
		$this->load->view('production/app_footer');
	}

	// used on user click btn_edit in order_details table
	function order_detail_id()
	{
		$order_detail_id = $this->input->post('order_detail_id');
		// $bel_id = 'DENISH00001';
		$db = $this->model_dashboard->order_detail_id($order_detail_id);

		$response = array();
		foreach ($db->result() as $row) 
		{
			$tmp = array(
				'bel_id' => $row->bel_id,
				'order_detail_id' => $row->order_detail_id,
				'detail_qty' => $row->detail_qty,
				'pending' => $row->pending,
				'prod_name' => $row->prod_name,
				'prod_id' => $row->prod_id
				);
			array_push($response, $tmp);
		}

		echo json_encode($response);
	}

	function json_order_details()
	{
		$order_id = $this->input->post('order_id');

		$data['order_details'] = $this->model_dashboard->order_details($order_id);

		$response['data'] = array();
		$no = 1;
		foreach ($data['order_details'] as $row) 
		{
			$tmp = array();
			$tmp[] = $no++;
			$tmp[] = $row->bel_id;
			$tmp[] = $row->prod_name;
			$tmp[] = $row->detail_qty;
			$tmp[] = $row->pending;
			$tmp[] = $row->notes;
			
		
			if($row->decor)
			{
				$url = site_url('end_cust/decoration/index').'/'.$row->order_detail_id.'/0';

				$tmp[] = '<a href="'.$url.'" class="btn btn-primary">
                               Decor
                         </a>';
			}else
			{
				$tmp[] = '';
			}

			$tmp[] = '<a id="'.$row->order_detail_id.'" class="btn btn-success btn_edit">
                         Update
                       </a>';

			array_push($response['data'], $tmp);
			
		}

		echo json_encode($response);
	}

	// for refresh function in orders table
	function json_orders()
	{
		$data['waiting_orders'] = $this->model_dashboard->waiting_orders();

		$response['data'] = array();
		$no = 1;
		foreach ($data['waiting_orders'] as $row) 
		{
			$tmp = array();
			$tmp[] = $no++;
			$tmp[] = $row->order_id;
			if ($row->order_name == null)
			{
				$tmp[] = '-';
			} else {
				$tmp[] = $row->order_name;
			}
			$tmp[] = $this->app_helper->dateTimeSimpleForm($row->order_date);
			$tmp[] = $row->order_type;

			if ($row->order_deliv_date == '0000-00-00 00:00:00' || $row->order_deliv_date == NULL) 
				$tmp[] = "-";
			else 
				$tmp[] = $this->app_helper->dateTimeSimpleForm($row->order_deliv_date);

			$val_order_id = str_replace("/", "_", $row->order_id);

			$url = base_url('production/dashboard/get_order_details/'.$val_order_id);

			$tmp[] = '<a href="'.$url.'" class="btn btn-success btn_view" title="Lihat Detail Produk" 	style="color: white;">
                        <span class="glyphicon glyphicon-eye-open"></span> view
                    </a>';

			array_push($response['data'], $tmp);
			
		}

		echo json_encode($response);
	}

	function update_pending()
	{
		$order_detail_id = $this->input->post('order_detail_id');
		$prod_id = $this->input->post('prod_id');
		$order_id = $this->input->post('order_id');
		$value_added = $this->input->post('value_added');

		/*log_message('error', 'order_detail_id: '.$order_detail_id);
		log_message('error', 'prod_id: '.$prod_id);
		log_message('error', 'order_id: '.$order_id);
		log_message('error', 'value_added: '.$value_added);*/

		$this->model_dashboard->update_pending($order_detail_id, $prod_id, $order_id, $value_added);

		redirect(base_url('production/dashboard/get_order_details/'.str_replace("/", "_", $order_id)));

	}

	function sign_out()
	{
		$this->session->unset_userdata('privilage');
		redirect(base_url('production/sign_in'));
	}

	function go_signin()
	{
		$this->load->view('production/signin');
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/production/Dashboard.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign_in extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('production/model_dashboard');
	}

	public function index()
	{
		$priv = $this->session->userdata('privilage');
		
		if ($priv != 'Produksi') {
			die($this->load->view('production/signin', '', TRUE));
		} else {
			redirect(base_url('production/dashboard'));
		}
	}

	function check_user(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$data = array(
			'username' => $username,
			'password' => $password
			);

		$res = $this->model_dashboard->signin($data);
		if ($res == 'user_found') {
			$this->session->set_userdata('privilage', 'Produksi');
			redirect(base_url('production/dashboard'));
		} else {
			$this->session->set_flashdata('alert_msg', 'Login gagal. username atau password salah.');
			redirect(base_url('production/sign_in'));
		}
	}

}

/* End of file sign_in.php */
/* Location: ./application/controllers/production/sign_in.php */
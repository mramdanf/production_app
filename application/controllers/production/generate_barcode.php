<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generate_barcode extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('Zend');
	}

	public function index()
	{
		$this->load->view('production/barcode_view');
		// echo phpinfo();
	}

	function do_generate($kode)
	{
		
		$this->zend->load('Zend/Barcode');
		Zend_Barcode::render('code128', 'image', array('text'=>$kode), array());
	}

}

/* End of file generate_barcode.php */
/* Location: ./application/controllers/production/generate_barcode.php */
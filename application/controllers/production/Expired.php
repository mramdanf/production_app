<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expired extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('production/m_expired');
	}

	public function index()
	{
		
		$data['expired_prods'] = $this->m_expired->get_expireds()->result();
		
		$header_data['title'] = "Produksi | Expired Prod.";

		$this->load->view('production/app_header', $header_data);

		$this->load->view('production/expired', $data);
		$this->load->view('production/app_footer');
		
	}

	function form($id = NULL)
	{
		$this->load->model('end_cust/model_dashboard');
		$data['prod_name'] = $this->model_dashboard->get_prod_name('');

		if($id != NULL)
		{
			$data['expired'] = $this->m_expired->get_expired($id)->row();
			$data['expired']->exp_date = $this->app_helper->dateSimpleForm($data['expired']->exp_date);
		}

		$header_data['title'] = "Produksi | Expired Prod.";

		$this->load->view('production/app_header', $header_data);
		$this->load->view('production/expired_form', $data);
		$this->load->view('production/app_footer');
	}

	function modify()
	{
		$id = $this->input->post('id');
		$data = $this->input->post();

		unset($data['prod_name']);
		
		$data["last_update"] = $this->app_helper->dateTimeDbForm();
		$data['exp_date'] = $this->app_helper->dateDbForm($this->input->post("exp_date"));

		if($id != NULL)
		{
			if($this->m_expired->update_expired($data))
			{
				$this->app_helper->setFlash("success",'Data expired berhasil diubah.');
			} else 
			{
				$this->app_helper->setFlash("failed",'Terjadi error, data expired gagal diubah.');
			}

		} else 
		{
			unset($data['id']);

			if($this->m_expired->add_expired($data))
			{
				$this->app_helper->setFlash("success",'Data expired berhasil ditambahkan.');
			} else 
			{
				$this->app_helper->setFlash("failed",'Terjadi error, data expired gagal ditambahkan.');
			}
		}

		redirect('production/expired');
	}

	function delete()
	{
		$id = $this->input->post('id');
		if($this->m_expired->del_expired($id))
		{
			$this->app_helper->setFlash("success", "Data berhasil dihapus.");
		} else 
		{
			$this->app_helper->setFlash("failed", "Terjadi error, data gagal dihapus.");
		}

		redirect('production/expired');
	}


}

/* End of file Expired.php */
/* Location: ./application/controllers/production/Expired.php */
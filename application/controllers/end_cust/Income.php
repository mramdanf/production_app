<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Income extends CI_Controller {

	private $array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
	private $months_id = array(1=>"Januari","Februari","Maret","April","Mei", "Juni","Juli","Agustus","September", "Oktober", "November","Desember");

	function __construct()
	{
		parent::__construct();
		$this->load->model('end_cust/m_income');
	}

	public function index()
	{
		$data['orders'] = $this->m_income->get_orders();
		$data['total_income'] = 0;
		$data['income_filter'] = NULL;
		$data['income_shift_satu'] = 0;
		$data['income_shift_dua'] = 0;
		$data['shift'] = NULL;
		$data['months_id'] = $this->months_id;
		$data['active_shift'] = $this->m_income->active_shift();

		$header_data['title'] = 'Kasir | Pendapatan';

		$this->load->view('end_cust/app_header', $header_data);
		$this->load->view('end_cust/income', $data);
		$this->load->view('end_cust/app_footer');
	}

	/*
		used-by: view when click btn closing
		desc: close last order (that haven't close)
		@params: 0: will not print struct; 1: will print struct
	*/
	function closing($print = 0)
	{
		if($this->m_income->total_unclose() > 0)
		{
			$order_ids = $this->m_income->last_unclose_order()->result_array();

			if($this->m_income->close_last_order())
			{
				$this->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>'Closing berhasil.',
				        'type'=>'alert-success'
				    )
				);

				if ($print) 
				{
					$this->print_closing_struct($order_ids);
				}

			} else 
			{
				$this->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>'Closing gagal.',
				        'type'=>'alert-danger'
				    )
				);
			}
		} else 
		{
			$this->load->model('end_cust/model_dashboard');
			$curr_shift = "";
			$last_shift = $this->model_dashboard->last_shift();

			// ketika data order masih kosong penentuan shift berdasarkan jam selainnya
			// penentuan shift berdasrkan last shift
			if(count($last_shift) == 0) 
			{
				date_default_timezone_set("Asia/Jakarta");
				if(time() <= strtotime("13:00:00") && time() >= strtotime("07:00:00"))
				{
					$curr_shift = 0;

				} else 
				{
					$curr_shift = 1;
				}
			} else 
			{
				$curr_shift = ($last_shift[0]->shift==0) ? 1:0;
			}

			if($this->m_income->no_order_inshift($curr_shift))
			{
				$this->session->set_flashdata(
				    'alert', 
				    array(
				        'msg'=>'Closing berhasil.',
				        'type'=>'alert-success'
				    )
				);
			}
				
		}
		redirect('end_cust/income');
	}

	/*
		used-by: view, on btn closing click
		desc: will print last unclosed struct
		@params: list of order_ids to be printed
	*/
	function print_closing_struct($order_ids = NULL)
	{
		// convert multidimentional array to ordinary array
		$order_ids = array_map('current', $order_ids);
		
		try 
		{
			$this->load->library('Struct_Print');
			$this->struct_print->multiple_struct($order_ids);

		} catch (Exception $e) 
		{
			log_message("error", "Error: Could not print. Message ".$e->getMessage());
			$this->struct_print->close_after_exception();
		}
		
	}

	/*
		used-by: view, when click: btn search
		desc: search by: day (date,mount,year) - month (month,year)
			with or without shift
	*/
	function adv_search()
	{
		// user search by month and year, in the outher word if user select month and
		// year all search key except month and year will be ignored
		if($this->input->get('month') && $this->input->get('year'))
		{
			$month = $this->input->get('month');
			$year = $this->input->get('year');

			// income filter
			$income_filter = $this->months_id[$month] ." ". $year;

			// total income
			$total_income = $this->m_income->income_month_year($month, $year);

			// income shift satu
			$income_shift_satu = $this->m_income->income_shift(0, "", $month, $year);

			// income shift dua
			$income_shift_dua = $this->m_income->income_shift(1, "", $month, $year);

			// user consider also search by shift
			$shift = NULL;

			if($this->input->get('shift') != NULL && $this->input->get('shift') != "")
			{
				$input_shift = $this->input->get('shift');

				$orders = $this->m_income->orders_date_year($month, $year, $input_shift);
				
				$shift = ($input_shift==0)?"Shift Satu":"Shift Dua";

				// concate income_filter with shift
				$income_filter = $this->months_id[$month] ." ". $year ." - ". $shift;

			} else 
			{
				$orders = $this->m_income->orders_date_year($month, $year);
			}

		} else if($this->input->get('key_date')) // user search by date/day
		{
			$key_date = $this->app_helper->dateDbForm($this->input->get('key_date'));

			// income filter
			$date_income = $this->app_helper->dateSimpleForm($key_date);
			$date_income = $this->array_hari[date("N", strtotime($date_income))] .", ". $date_income;
			$income_filter = $date_income;

			// total income
			$total_income = $this->m_income->income_by_date($key_date);

			// income shift satu
			$income_shift_satu = $this->m_income->income_shift(0, $key_date);

			// income shift dua
			$income_shift_dua = $this->m_income->income_shift(1, $key_date);

			// user consider also search by shift
			$shift = NULL;

			if($this->input->get('shift') != NULL && $this->input->get('shift') != "")
			{
				$input_shift = $this->input->get('shift');

				$orders = $this->m_income->get_orders_date_shift($input_shift, $key_date);
				
				$shift = ($input_shift==0)?"Shift Satu":"Shift Dua";

				// concat income filter with shift data
				$income_filter = $date_income ." - ". $shift;

			} else 
			{
				$orders = $this->m_income->get_orders_by_date($key_date);
			}
		} else if($this->input->get('to_day')) // user search for to_day
		{
			$now_day = date("Y-m-d");

			// income search filter
			$income_filter = $this->app_helper->dateSimpleForm($now_day);
			$income_filter	= $this->array_hari[date("N")] .", ". $income_filter;

			// user also consider search by shift
			if($this->input->get('shift') != NULL && $this->input->get('shift') != "")
			{
				$input_shift = $this->input->get('shift');

				$total_income = $this->m_income->income_shift($input_shift, $now_day);

				$orders = $this->m_income->get_orders_date_shift($input_shift, $now_day);

				$income_shift_satu = $this->m_income->income_shift(0, $now_day);
				$income_shift_dua = $this->m_income->income_shift(1, $now_day);

				$shift = ($input_shift==0)?"Shift Satu":"Shift Dua";

				$income_filter = $income_filter .' - '. $shift;

			} else 
			{
				$total_income = $this->m_income->income_by_date($now_day);

				$orders = $this->m_income->get_orders_by_date($now_day)      ;

				$income_shift_satu = $this->m_income->income_shift(0, $now_day);
				$income_shift_dua = $this->m_income->income_shift(1, $now_day);

				$shift = NULL;
			}
		} else
		{
			$total_income = 0;
			$income_filter = NULL;
			$income_shift_satu = 0;
			$income_shift_dua = 0;
			// supaya tidak error jadi untuk orders dibuat seperti ini
			$orders = $this->m_income->get_orders_by_date(NULL);
			$shift = NULL;
		}

		$data['total_income'] = $total_income;		
		$data['income_filter'] = $income_filter;
		$data['income_shift_satu'] = $income_shift_satu;
		$data['income_shift_dua'] = $income_shift_dua;
		$data['months_id'] = $this->months_id;
		$data['orders'] = $orders;
		$data['shift'] = $shift;
		$data['active_shift'] = $this->m_income->active_shift();
		$header_data['title'] = 'Kasir | Pendapatan';

		$this->load->view('end_cust/app_header', $header_data);
		$this->load->view('end_cust/income', $data);
		$this->load->view('end_cust/app_footer');
	}

	function test()
	{
		$this->utility->print_log($this->m_income->total_unclose());
	}

}

/* End of file Income.php */
/* Location: ./application/controllers/end_cust/Income.php */
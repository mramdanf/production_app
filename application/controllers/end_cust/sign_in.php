<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign_in extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('end_cust/model_dashboard');
	}

	public function index()
	{
		$priv = $this->session->userdata('privilage');
		if ($priv != 'End Customer') {
			$this->load->view('end_cust/login');
		} else {
			redirect(base_url('end_cust/dashboard'));
		}
	}

	function check_user(){
		$data = array(
			'user' => $this->input->post('username'),
			'pass' => $this->input->post('password')
			);

		$res_auth = $this->model_dashboard->search_user($data);

		if ($res_auth['status'] == 'user_found') {
			$this->session->set_userdata('privilage', 'End Customer');
			$this->session->set_userdata('first_name', $res_auth['first_name']);

			redirect(base_url('end_cust/dashboard'));
		} else {
			$this->session->set_flashdata('alert_msg', 'Login failed. username atau password salah.');
			// $this->load->view('end_cust/login');
			redirect(base_url('end_cust/sign_in'));
		}
	}

	

}

/* End of file sign_in.php */
/* Location: ./application/controllers/end_cust/sign_in.php */
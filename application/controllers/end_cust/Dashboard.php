<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	/*
		Attentions:
			1. detail_price in table order_detail, just contain (product_price * detail_qty)
			2. discount pada order_detail belum merupakan discount murni
			3. numbering format for pricing, do in controller
			4. counting or logical operation should do in model (if relevant)
			5. all word sub_total mean qty * prod_price without any addition to it
			6. all word product mean, heaven product like cake, bread etc
			7. on table orders field order_amount currently contain sum of order_detail
				sub_total + ongkir
			8. kelunasan tidak di simpan di db melainkan dihitung manual, yaitu jika uang
				bayar < grand_total -> belum lunas, selainnya sudah lunas

		Query notes:
			1. query get_order_detail by order_id paling bagus ada di model_dashboard

	*/

	private $base_api_url = 'http://localhost/heavenshard/api_production/dashboard/';
	private $dir_end_cust = 'end_cust/';
	private $ip_printer = "10.69.69.3";
	private $url_lcd = "http://10.69.69.2:3090/";
	const CAKE_ID = "2";
	private $tag_order_id = "order_id";

	function __construct()
	{
		parent::__construct();

		$this->load->model($this->dir_end_cust.'model_dashboard');

		$priv = $this->session->userdata('privilage');

		if ($priv != 'End Customer') 
		{
			redirect(base_url('end_cust/sign_in'));
		}

	}

	public function index()
	{
		$order_id = $this->session->userdata('order_id');
		$data['bel_id'] = $this->model_dashboard->get_bel_id('');
		$data['prod_name'] = $this->model_dashboard->get_prod_name('');
		$data['order_id'] = $order_id;
		$data['detail_price'] = $this->model_dashboard->sum_price($order_id);
		$data['order_details'] = $this->model_dashboard->data_order_details($order_id);
		$data['discount'] = $this->model_dashboard->get_active_discount();
		$data['order'] = $this->model_dashboard->get_order_by_id($order_id)->row();
		$data['edit_order'] = $this->model_dashboard->is_active_editorder();

		// when page in edit order mode $data['order'] should contain array
		if(count($data['order']) > 0)
		{
			// so, i need to convert datetime format of order_deliv_date
			$order_deliv_date = $data['order']->order_deliv_date;
			if($order_deliv_date)
			{
				$order_deliv_date = $this->utility->dateTimeSimpleForm($order_deliv_date);
				$data['order']->order_deliv_date = $order_deliv_date;
			}

			// when edit order mode $data['detail_price'] sould be + ongkir (if ongkir exist)
			$data['detail_price'] += $data['order']->ongkir;

		} 

		// numbering format
		foreach ($data['order_details'] as $row) 
		{
			// number_format money simbol
    		$row->harga = number_format($row->harga);
    		$row->total_decor = number_format($row->total_decor);
    		$row->discount = number_format(0 - $row->discount);
    		$row->sub_total = number_format($row->sub_total);
		}
		
		$header_data['title'] = 'Kasir | Dashboard';

		$this->load->view('end_cust/app_header', $header_data);
		$this->load->view('end_cust/dashboard', $data);
		$this->load->view('end_cust/app_footer');
	}

	function del_order_details()
	{

		$order_detail_id = $this->input->post('order_detail_id');
		$bel_id = $this->input->post('bel_id');
		$last_qty = $this->input->post('last_qty');
		$order_id = $this->session->userdata($this->tag_order_id);

		if ($this->model_dashboard->count_order_details() == 'last_row') 
		{
			$this->model_dashboard->del_cur_orders($order_id);

			$this->clear_session_editorder();
		}
		$this->model_dashboard->del_order_details($order_detail_id);
		$this->model_dashboard->update_stocks($bel_id, $last_qty, 0);

		$this->session->set_flashdata('alert_msg', 'Data berhasil dihapus');
		
	}

	// fungsi ini bisa handle ketika pakai barcode maupun tidak
	function add_order_details()
	{
		// generate order id
		// blok kodingan ini ingin menghasilkan HVSRD/ORDER/20160604/max_id
		$curr_date = date('Ymd'); // 20160604
		$max_id = $this->model_dashboard->get_max_orderid();

		$max_id = substr($max_id, 21) + 1;
		$order_id = 'HVSRD/ORDER/'.$curr_date.'/'.$max_id;

		// detail qty
		$detail_qty = $this->input->post('detail_qty');
		if ($detail_qty == NULL) 
		{
			$detail_qty = 1;
		}

		// belong to id
		$bel_id = $this->input->post('bel_id');

		// cek apakah produk yang diinput telah ada di current order
		// jika ya ambil detail qtynya dan tambahkan dengan qty yang diinputkan
		$curr_qty = $this->model_dashboard->check_prod_order($order_id, $bel_id);
		if ($curr_qty > 0) 
		{
			$detail_qty += $curr_qty;
			$curr_stock = $this->model_dashboard->substract_stocks($bel_id, $curr_qty, $detail_qty);

		}else
		{
			$curr_stock = $this->model_dashboard->substract_stocks($bel_id, 0, $detail_qty);
		}

		// pending and status
		$stock_awal = $curr_stock + ($detail_qty);
		$is_cake = $this->model_dashboard->prod_name_by_id($bel_id)->row()->cat_id;

		if($is_cake == Dashboard::CAKE_ID) // all cake order will be pending
		{
			$pending = $detail_qty;
			$status = App_helper::LBL_ON_PROGRESS;

		} else if ($stock_awal >= 0) // 0 dan angka pluss
		{
			$pending = $stock_awal - $detail_qty;
			if ($pending >= 0) 
			{
				$pending = 0;
				$status = App_helper::LBL_COMPLETE;

			} else 
			{
				$pending = $pending * (-1);
				$status = App_helper::LBL_ON_PROGRESS;
			}
		} else 
		{
			$pending = (0 - $detail_qty)  * (-1);
			$status = App_helper::LBL_ON_PROGRESS;
		}

		// detail price
		$price = $this->model_dashboard->get_price($bel_id);
		$detail_price = $price * $detail_qty;
		
		// discount
		$discount = $this->input->post('discount');

		$input_order_details = array(
			'order_id' => $order_id,
			'bel_id' => $bel_id,
			'detail_price' => $detail_price,
			'detail_qty' => $detail_qty,
			'status' => $status,
			'pending' => $pending,
			'discount' => $discount,
			'notes' => $this->input->post('notes')
		);

		if ($this->session->userdata($this->tag_order_id) == NULL) 
		{
			$this->session->set_userdata($this->tag_order_id, $order_id);
		}

		// jika produk pernah diinsert sebelumnya pada current order
		// maka actionnya update
		if ($curr_qty > 0) 
		{
			$this->model_dashboard->update_prod_order($input_order_details, $order_id, $bel_id);

		}else
		{
			$this->model_dashboard->add_order_details($input_order_details);
		}

		// data untuk ke lcd
		$cur_prod_name = $this->model_dashboard->prod_name_by_id($bel_id)->row()->prod_name;
		// qty * harga produk = $detail_price
		$total_harga = $this->model_dashboard->sum_price($order_id);
		// $this->display_lcd($cur_prod_name, (string)$detail_price, (string)$total_harga);

		$this->session->set_flashdata('alert_msg', 'Produk <strong>'.$this->input->post('product_name').'</strong> berhasil ditambahkan.');
		
		redirect(base_url('end_cust/dashboard'));

		
	}

	function update_order_details()
	{
		$order_detail_id = $this->input->post('order_detail_id');
		$bel_id = $this->input->post('bel_id');
		$detail_qty = $this->input->post('detail_qty');
		$price = $this->model_dashboard->get_price($bel_id);

		$detail_price = $price * $detail_qty;

		$last_qty = $this->input->post('last_qty');

		// checking wether there is a discount
		$discount = $this->input->post('discount');
		if($discount > 0)
		{
			$detail_price = $detail_price - ($detail_price * ($discount/100));
		}

		$data = array(
			'bel_id' => $bel_id,
			'detail_price' => $detail_price,
			'notes' => $this->input->post('notes'),
			'discount' => $discount,
			'detail_qty' => $detail_qty
			);

		$this->model_dashboard->update_order_details($order_detail_id, $data);
		$this->model_dashboard->update_stocks($bel_id, $last_qty, $detail_qty);
		$this->session->set_flashdata('alert_msg', 'Produk berhasi diupdate.');
		redirect(base_url('end_cust/dashboard'));
	}

	function prod_name_by_id()
	{
		$bel_id = $this->input->post('bel_id');
		// $bel_id = 'DENISH00001';
		$db = $this->model_dashboard->prod_name_by_id($bel_id);

		$response = array();
		foreach ($db->result() as $row) 
		{
			$tmp = array(
				'prod_name' => $row->prod_name,
				'cat_name' => $row->cat_name
				);
			array_push($response, $tmp);
		}

		// [{"prod_name":"Danish 1","cat_name":"Danish"}]
		echo(json_encode($response));	
	}

	function del_cur_orders()
	{
		$order_id = $this->input->post('var_order_id');
		$this->model_dashboard->suit_stock($order_id);
		$this->model_dashboard->del_cur_orders($order_id);

		$this->clear_session_editorder();

		$this->session->set_flashdata('alert_msg', 'Order <strong>'.$order_id.'</strong> berhasil dihapus.');

		// $this->clear_lcd();
	}

	/*
		used: for testing purpose
	*/
	function print_pdf_order()
	{

		$order_id = $this->input->post('order_id');
		$order_name = $this->input->post('order_name');
		$order_add = $this->input->post('order_add');
		$order_phone = $this->input->post('order_phone');
		$order_date = $this->input->post('order_date');
		$order_tax = $this->input->post('order_tax');
		$order_type = $this->input->post('is_pickup');
		$ongkir = $this->removeComaFromNumber($this->input->post('ongkir'));
		$order_recipient = $this->input->post('order_recipient');
		$order_phone_recipient = $this->input->post('order_phone_recipient');
		$order_amount = $this->removeComaFromNumber($this->input->post('order_amount'));
		$order_paid = $this->removeComaFromNumber($this->input->post('order_paid'));
		$order_paid_return = $this->removeComaFromNumber($this->input->post('order_paid_return'));
		$is_waiting = $this->model_dashboard->waiting_check($order_id);

		// ketika input order_deliv_date kosong maka isi dengan NULL
		$order_deliv_date = NULL;
		if($this->input->post('order_deliv_date') != "" 
			&& $this->input->post('order_deliv_date') != NULL)
		{
			$order_deliv_date 
				= $this->app_helper->dateTimeDbForm($this->input->post('order_deliv_date'));
		}

		// penentuan shift
		$curr_shift = "";
		$last_shift = $this->model_dashboard->last_shift();

		// ketika data order masih kosong penentuan shift berdasarkan jam, selainnya
		// penentuan shift berdasrkan last shift
		if(count($last_shift) == 0) 
		{
			date_default_timezone_set("Asia/Jakarta");
			if(time() <= strtotime("13:00:00") && time() >= strtotime("07:00:00"))
			{
				$curr_shift = 0;

			} else 
			{
				$curr_shift = 1;
			}
		} else 
		{
			$curr_shift = ($last_shift[0]->shift==0) ? 1:0;
		}

		$input_orders = array(
			'order_id' => $order_id,
			'order_name' => $order_name,
			'order_add' => $order_add,
			'order_phone' => $order_phone,
			'order_deliv_date' => $order_deliv_date,
			'order_tax' => $order_tax,
			'order_type' => $order_type,
			'order_amount' => $order_amount,
			'order_paid' => $order_paid,
			'order_paid_return' => $order_paid_return,
			'shift' => $curr_shift,
			'ongkir' => $ongkir,
			'order_recipient' => $order_recipient,
			'order_phone_recipient' => $order_phone_recipient,
			'status' => $is_waiting
			);

		// pilih aksi, insert atau update
		if($this->session->userdata('is_update'))
		{
			unset($input_orders['order_id']);
			$this->model_dashboard->update_order($input_orders, $order_id);
			$this->session->unset_userdata('is_update');

		} else
		{
			$this->model_dashboard->process_order($input_orders);
		}

		// clear session contain is_update, order_id
		$this->clear_session_editorder();

		redirect('end_cust/dashboard');

		// creating invoice
		/*$data['order_id'] = $order_id;
		$data['order_details'] = $this->model_dashboard->data_order_details($order_id);
		$dateTime = new DateTime("now", new DateTimeZone('Asia/Jakarta'));
		$add_date = $dateTime->format("d/m/Y H:i:s");
		$data['order_date'] = $add_date;
		$data['detail_price'] = $this->input->post('order_amount');
		$data['order_paid'] = $order_paid;
		$data['first_name'] = ($curr_shift==0) ? "Shift Pagi":"Shift Siang";
		$data['order_paid_return'] = $order_paid_return;
		$data['order_type'] = $order_type;
		$data['order_name'] = $order_name;
		$data['order_deliv_date'] = $order_date;
		$data['order_phone'] = $order_phone;
		$data['order_add'] = $order_add;

		$html=$this->load->view('end_cust/invoice_pdf', $data, true); 

		$this->load->library('m_pdf');
		$pdf = $this->m_pdf->load();
		$pdf->WriteHTML($html);
		$pdf->Output();*/
	}

	/*
		used: when order is complete, and print struct
	*/
	function print_struct()
	{
		$order_id = $this->input->post('order_id');
		$order_name = $this->input->post('order_name');
		$order_add = $this->input->post('order_add');
		$order_phone = $this->input->post('order_phone');
		$order_date = $this->input->post('order_date');
		$order_tax = $this->input->post('order_tax');
		$order_type = $this->input->post('is_pickup');
		$ongkir = $this->removeComaFromNumber($this->input->post('ongkir'));
		$order_recipient = $this->input->post('order_recipient');
		$order_phone_recipient = $this->input->post('order_phone_recipient');
		$order_amount = $this->removeComaFromNumber($this->input->post('order_amount'));
		$order_paid = $this->removeComaFromNumber($this->input->post('order_paid'));
		$order_paid_return = $this->removeComaFromNumber($this->input->post('order_paid_return'));
		$is_waiting = $this->model_dashboard->waiting_check($order_id);

		// ketika input order_deliv_date kosong maka isi dengan NULL
		$order_deliv_date = NULL;
		if($this->input->post('order_deliv_date') != "" 
			&& $this->input->post('order_deliv_date') != NULL)
		{
			$order_deliv_date 
				= $this->app_helper->dateTimeDbForm($this->input->post('order_deliv_date'));
		}

		// penentuan shift
		$curr_shift = "";
		$last_shift = $this->model_dashboard->last_shift();

		// ketika data order masih kosong penentuan shift berdasarkan jam, selainnya
		// penentuan shift berdasrkan last shift
		if(count($last_shift) == 0) 
		{
			date_default_timezone_set("Asia/Jakarta");
			if(time() <= strtotime("13:00:00") && time() >= strtotime("07:00:00"))
			{
				$curr_shift = 0;

			} else 
			{
				$curr_shift = 1;
			}
		} else 
		{
			$curr_shift = ($last_shift[0]->shift==0) ? 1:0;
		}

		$input_orders = array(
			'order_id' => $order_id,
			'order_name' => $order_name,
			'order_add' => $order_add,
			'order_phone' => $order_phone,
			'order_deliv_date' => $order_deliv_date,
			'order_tax' => $order_tax,
			'order_type' => $order_type,
			'order_amount' => $order_amount,
			'order_paid' => $order_paid,
			'order_paid_return' => $order_paid_return,
			'shift' => $curr_shift,
			'ongkir' => $ongkir,
			'order_recipient' => $order_recipient,
			'order_phone_recipient' => $order_phone_recipient,
			'status' => $is_waiting
			);

		// pilih aksi, insert atau update
		if($this->session->userdata('is_update'))
		{
			unset($input_orders['order_id']);
			$q_result = $this->model_dashboard->update_order($input_orders, $order_id);

		} else
		{
			$q_result = $this->model_dashboard->process_order($input_orders);
		}

		if($q_result)
		{
			try 
			{
				$this->load->library('Struct_Print');
				$this->struct_print->single_struct($order_id);

			} catch (Exception $e) 
			{
				log_message("error", "Error: Could not print. Message ".$e->getMessage());
				$this->struct_print->close_after_exception();
			}
		}

		// clear session contain is_update, order_id
		$this->clear_session_editorder();

		redirect('end_cust/dashboard');


	}

	/*
		used: for data products in products page
	*/
	function products()
	{
		$this->load->model('production/model_products');
		$data['products'] = $this->model_products->data_products();
		$data['categories'] = $this->model_products->get_categories();

		$header_data['title'] = 'Kasir | Products';

		$this->load->view('end_cust/app_header', $header_data);
		$this->load->view('end_cust/products', $data);
		$this->load->view('end_cust/app_footer');
		
	}

	/*
		used: fungsi ketika ingin mengedit order (void)
	*/
	function display_order()
	{
		$key_order_id = $this->input->post('key_order_id');
		$key_order_id = preg_replace('/\s+/', '', $key_order_id);

		$can_edit_order = $this->model_dashboard->is_active_editorder();

		if($key_order_id != NULL && $can_edit_order)
		{
			$this->session->set_userdata($this->tag_order_id, $key_order_id);
			$this->session->set_userdata('is_update', "update");

		} else 
		{
			$this->clear_session_editorder();
		}
		
		redirect('end_cust/dashboard');
	}

	/*
		used: for clear lcd after process order or delete order
	*/
	function clear_lcd()
	{
		$url = $this->url_lcd . "clear";

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER,
		        array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_POST, true);
		$json_response = curl_exec($curl);
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);
		$response = json_decode($json_response, true);
	}

	/*
		used: for display lcd when add product to order detail
	*/
	function display_lcd($prod_name='', $detail_price='', $total_harga='')
	{
		$data = array(
			  'clear'      => 'true',
			  'item'    => $prod_name,
			  'price'       => $detail_price,
			  'total' => $total_harga,
			);
		$url = $this->url_lcd;    
		$content = json_encode($data);

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER,
		        array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

		$json_response = curl_exec($curl);

		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);
		$response = json_decode($json_response, true);
	}

	function signout()
	{
		$this->session->unset_userdata('privilage');
		$this->session->unset_userdata('first_name');
		$this->session->unset_userdata('order_id');
		redirect(base_url('end_cust/sign_in'));
	}

	// get data by bel_id
	// return: json data 
	// [{"bel_id":"1122","prod_name":"Decor Cream","cat_name":"Cake"}]
	// params: bel_id
	function auto_prod_id()
	{
		if (isset($_GET['key'])) 
		{
			$key = $_GET['key'];
			$this->model_dashboard->auto_prod_id('1122');			
		}
	}

	// get data by bel_id
	// return: json data 
	// [{"bel_id":"1122","prod_name":"Decor Cream","cat_name":"Cake"}]
	// params: prod_name
	function auto_prod_name()
	{
		if (isset($_GET['term'])) 
		{
			$key = $_GET['term'];
			$this->model_dashboard->auto_prod_name($key);
		}
	}

	/* =========== helper function =============== */
	function removeComaFromNumber($data)
	{
		if(preg_match("/^[0-9,]+$/", $data)) {
			$data = str_replace(',','',$data);
		}

		return $data;
	}

	function clear_session_editorder()
	{
		$this->session->unset_userdata('is_update');
		$this->session->unset_userdata($this->tag_order_id);
	}
}


/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
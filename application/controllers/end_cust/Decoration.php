<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Decoration extends CI_Controller {

	private $title = 'Kasir | Decoration';

	function __construct()
	{
		parent::__construct();
		$this->load->model('end_cust/m_decoration');
	}

	/**
	 * @param	int   $mode   page mode, 1: editable, !1: uneditable
	 */
	public function index($order_detail_id = '', $mode = 1)
	{
		$data['title'] = $this->title;

		$this->load->model('end_cust/model_dashboard');
		
		$data['bel_id'] = $this->model_dashboard->get_bel_id('');
		$data['prod_name'] = $this->model_dashboard->get_prod_name('');
		$data['order_detail_id'] = $order_detail_id;
		$data['order_detail'] = $this->m_decoration->order_detail($order_detail_id)->row();
		$data['decors'] = $this->m_decoration->decors($order_detail_id);
		$data['sum_decor_subtotal'] = $this->m_decoration->sum_decor_subtotal($order_detail_id);
		$data['mode'] = $mode;

		// numbering format
		foreach ($data['decors']->result() as $row) 
		{
			$row->harga = number_format($row->harga);
			$row->sub_total = number_format($row->sub_total);
		}

		
		$this->load->view('end_cust/app_header', $data);
		$this->load->view('end_cust/decoration');
		$this->load->view('end_cust/app_footer');

	}

	function modify()
	{
		$id = $this->input->post('id');
		$data = $this->input->post();

		if($this->m_decoration->modify($data))
		{
			$this->session->set_flashdata(
			    'alert', 
			    array(
			        'msg'=>'Data berhasil disimpan',
			        'type'=>'alert-success'
			    )
			);

		} else
		{
			$this->session->set_flashdata(
			    'alert', 
			    array(
			        'msg'=>'Terjadi error, data gagal disimpan',
			        'type'=>'alert-danger'
			    )
			);
		}

		redirect('end_cust/decoration/index/'.$data['order_detail_id']);
	}

	function delete()
	{
		$id = $this->input->post('id');
		$odi = $this->input->post('odi');

		if($this->m_decoration->delete($id, $odi))
		{
			$this->session->set_flashdata(
			    'alert', 
			    array(
			        'msg'=>'Data berhasil hapus',
			        'type'=>'alert-success'
			    )
			);

		} else
		{
			$this->session->set_flashdata(
			    'alert', 
			    array(
			        'msg'=>'Terjadi error, data gagal hapus',
			        'type'=>'alert-danger'
			    )
			);
		}
	}

	function cancel_decor()
	{
		$odi = $this->input->post('order_detail_id');

		if($this->m_decoration->cancel_decor($odi))
		{

			echo "true";

		} else 
		{
			$this->session->set_flashdata(
			    'alert', 
			    array(
			        'msg'=>'Terjadi error, silahkan coba kembali',
			        'type'=>'alert-danger'
			    )
			);

			echo "false";
		}
	}

}

/* End of file Decoration.php */
/* Location: ./application/controllers/end_cust/Decoration.php */
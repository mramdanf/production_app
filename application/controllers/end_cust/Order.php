<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('end_cust/model_order');
		$this->load->model('end_cust/m_income');

		date_default_timezone_set("Asia/Jakarta");

		$priv = $this->session->userdata('privilage');
		if ($priv != 'End Customer') 
		{
			redirect(base_url('end_cust/sign_in'));
		}
	}

	public function index()
	{
		// fill with null to prevent error in view
		$data['order'] = NULL;
		$data['order_details'] = NULL;
		$data['order_id'] = '';
		$data['key_date'] = NULL;

		$header_data['title'] = 'Kasir | Order';

		$this->load->view('end_cust/app_header', $header_data);
		$this->load->view('end_cust/end_cust_order', $data);
		$this->load->view('end_cust/app_footer');
	}

	/*
		used: when view button click  on table orders
	*/
	function get_order_details()
	{
		$this->load->model('end_cust/model_dashboard');

		$order_id = $this->input->post('order_id');
		
		$data['order_details'] =$this->model_dashboard->data_order_details($order_id);

		$response['data'] = array();
		$no = 1;
		foreach ($data['order_details'] as $row) 
		{
			$tmp = array();
			$tmp[] = $no++;
			$tmp[] = $row->bel_id;
			$tmp[] = $row->prod_name;
			$tmp[] = $row->name;
			
			$tmp[] = $row->pending;

			$type = 'label label-primary'; 
			if ($row->status == App_helper::LBL_ON_PROGRESS) { $type = 'label label-danger'; }
			$tmp[] = '<span class="'.$type.'">'.$row->status.'</span>';
			
			
			$tmp[] = number_format($row->harga);
			$tmp[] = $row->detail_qty;
			$tmp[] = number_format($row->total_decor);
			$tmp[] = number_format(0 - $row->discount);
			$tmp[] = number_format($row->sub_total);
			$tmp[] = $row->notes;

			if($row->total_decor > 0)
			{
				$url = site_url('end_cust/decoration/index').'/'.$row->order_detail_id.'/0';

				$tmp[] = '<a href="'.$url.'" class="btn btn-primary">
                               Decor
                         </a>';
			}else
			{
				$tmp[] = '';
			}

			array_push($response['data'], $tmp);
			
		}

		echo json_encode($response);
	}

	/*
		used: by jquery to populate orders table
	*/
	function json_orders()
	{
		$data['order'] = $this->model_order->get_all_orders();

		$response['data'] = array();
		$no = 1;
		foreach ($data['order'] as $row) 
		{
			$tmp = array();

			// nomor tabel
			$tmp[] = $no++;

			// order_id
			$tmp[] = $row->order_id;

			// order_date
			$tmp[] = $this->app_helper->dateTimeSimpleForm($row->order_date);

			// ini order date tapi dalam bentuk tanggal saja, dihidden. digunakan
			// untuk search by date di end_cust_order page
			$tmp[] = $this->app_helper->dateSimpleForm($row->order_date);

			// shift untuk keperluan search juga. dihidden
			$tmp[] = $row->shift;

			// order_amount
			$tmp[] = number_format($row->order_amount);

			// tagihan
			if($row->order_paid < $row->order_amount)
			{
				$tmp[] = number_format($row->order_amount - $row->order_paid);
			} else 
			{
				$tmp[] = NULL;
			}

			// order_paid
			$tmp[] = number_format($row->order_paid);

			// order_paid
			$tmp[] = number_format($row->order_paid_return);

			// status order
			$type = 'label label-primary'; 
			if ($row->status == App_helper::LBL_ON_PROGRESS) { $type = 'label label-danger'; }
			$tmp[] = '<span class="'.$type.'">'.$row->status.'</span>';

			// kelunasan
			if($row->order_paid < $row->order_amount)
			{
				$tmp[] = '<span class="label label-danger">BELUM LUNAS</span>';
			} else 
			{
				$tmp[] = '<span class="label label-primary">LUNAS</span>';;
			}

			/*========== delivery info ================*/
			$tmp[] = $row->order_type;
			if ($row->order_deliv_date == '0000-00-00 00:00:00' || $row->order_deliv_date == NULL)
			{
				$tmp[] = NULL;

			} else 
			{
				$tmp[] = $this->app_helper->dateTimeSimpleForm($row->order_deliv_date);
			}
			$tmp[] = number_format($row->ongkir);
			$tmp[] = $row->order_name;
			$tmp[] = $row->order_phone;
			$tmp[] = $row->order_recipient;
			$tmp[] = $row->order_phone_recipient;
			$tmp[] = $row->order_add;

			if($row->order_paid < $row->order_amount)
			{
				$tmp[] = '<a href="javascript:void(null);" title="Lihat Detail Order" class="btn btn-success btn_view" style="color: white;" id="'.$row->order_id.'">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a> <a href="javascript:void(null);" title="Edit Pembayaran" class="btn btn-warning btn_edit" style="color: white;" id="'.$row->order_id.'">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>';
			} else 
			{
				$tmp[] = '<a href="javascript:void(null);" title="Lihat Detail Order" class="btn btn-success btn_view" style="color: white;" id="'.$row->order_id.'">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>';
			}

			array_push($response['data'], $tmp);
			
		}

		echo json_encode($response);
	}

	/*
		used: when customer complete the paid
	*/
	function update_paid()
	{
		$order_id = $this->input->post('order_id');
		$order_target = $this->model_order->get_order_by_id($order_id)->row();
		$last_paid = $order_target->order_paid;
		$data['order_paid'] 
			= $last_paid + $this->removeComaFromNumber($this->input->post('order_paid'));
		$order_amount = $order_target->order_amount;
		// $lunas = 0;
		if($data['order_paid'] > $order_amount) 
		{
			$data['order_paid_return'] 
				= $data['order_paid'] - $order_amount;
			// $lunas = 1;

		} else 
		{
			$data['order_paid_return'] = 0;
		}
		
		if($this->model_order->pelunasan($data, $order_id))
		{
			$this->session->set_flashdata(
			    'alert', 
			    array(
			        'msg'=>'Data berhasil disimpan.',
			        'type'=>'alert-success'
			    )
			);

			// ketika order sudah lunas maka print struct
			/*if($lunas)
			{
				
				
			}*/
			try 
			{
				$this->load->library('Struct_Print');
				$this->struct_print->single_struct($order_id);

			} catch (Exception $e) 
			{
				log_message("error", "Error: Could not print. Message ".$e->getMessage());
				$this->struct_print->close_after_exception();
			}

		} else 
		{
			$this->session->set_flashdata(
			    'alert', 
			    array(
			        'msg'=>'Terjadi error data gagal disimpan.',
			        'type'=>'alert-danger'
			    )
			);
		}

		redirect('end_cust/order');
	}

	/*
		used: when print multiple struct
	*/
	function print_multiple_struct()
	{		
		$order_ids = $this->input->post('order_ids');
		$orders = $this->m_income->get_orders();
		try 
		{
			$this->load->library('Struct_Print');
			$this->struct_print->multiple_struct($order_ids, 0);
			redirect('end_cust/order');

		} catch (Exception $e) 
		{
			log_message("error", "Error: Could not print. Message ".$e->getMessage());
			$this->struct_print->close_after_exception();
		}
	}


	/* =========== helper function =============== */
	function removeComaFromNumber($data)
	{
		if(preg_match("/^[0-9,]+$/", $data)) {
			$data = str_replace(',','',$data);
		}

		return $data;
	}
}

/* End of file Order.php */
/* Location: ./application/controllers/end_cust/Order.php */
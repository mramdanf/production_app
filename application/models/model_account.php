<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_account extends CI_Model {

	public $user_table = 'user_table';

	function __construct(){
		parent::__construct();
	}

	function check_username_registered($username){
		$this->db->where('username', $username);
		$query = $this->db->get($this->user_table);

		if ($query->num_rows() > 0) {
			return true; // username registered
		} else return false;
	}

	function change_pass($username, $data){
		$this->db->where('username', $username);
		$this->db->update($this->user_table, $data);
	}
	

}

/* End of file model_account.php */
/* Location: ./application/models/model_account.php */
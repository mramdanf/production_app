<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_decoration extends CI_Model {

	private $decor_table = 'decoration';

	function __construct()
	{
		parent::__construct();

		$this->load->model('end_cust/model_dashboard');
	}

	function order_detail($order_detail_id = NULL)
	{
		$query = "SELECT od.*, pd.prod_name, pt.name
					FROM order_details AS od
					INNER JOIN belong_to AS bt
					ON od.bel_id = bt.bel_id
					INNER JOIN products pd
					ON bt.product_id = pd.prod_id
					JOIN properties pt
					ON bt.properties_id = pt.id
					WHERE od.order_detail_id = '".$order_detail_id."'";

		return $this->db->query($query);
	}

	function decors($order_detail_id = NULL)
	{
		$sql = "SELECT d.*, p.prod_name, p.harga, pt.name\n"
			    . "FROM decoration d \n"
			    . "INNER JOIN belong_to bt \n"
			    . "	ON d.decor_id = bt.bel_id\n"
			    . "INNER JOIN products p \n"
			    . "	ON bt.product_id = p.prod_id\n"
			    . "INNER JOIN properties pt \n"
			    . "	ON bt.properties_id = pt.id\n"
			    . "WHERE d.order_detail_id = '".$order_detail_id."'";

		$recs = $this->db->query($sql);

		return $recs;
	}

	function modify($data = NULL)
	{
		$id = $data['id'];
		unset($data['id']);

		$prod_price = $this->model_dashboard->get_price($data['decor_id']);
		$decor_subtotal = $data['qty'] * $prod_price;
		$odi = $data['order_detail_id'];
		$data['sub_total'] = $decor_subtotal;

		if($id == NULL)
		{
			unset($data['last_qty']);
			if($this->db->insert($this->decor_table, $data))
			{
				// substract decoration stock
				$this->substract_decor_stock($data['decor_id'], $data['qty']);

				// after insert to decoration, also insert to total_decor
				// in order_detail
				if($decor_subtotal > 0)
				{
					return $this->add_sub_total($decor_subtotal, $odi);
				}
			}
		
		} else
		{
			// return back updated decoration stock, idea: add stock 
			// with last qty than substrac with new qty
			$this->add_decor_stock($data['decor_id'], $data['last_qty']);
			$this->substract_decor_stock($data['decor_id'], $data['qty']);	

			unset($data['last_qty']);

			// when subtotal of decoration change, also change the total_decor
			// in order_detail table
			if($this->db->update($this->decor_table, $data, array("id"=>$id)))
			{
				$new_od_data['total_decor'] = $this->sum_decor_subtotal($odi);

				// update order_detail with new_od_subtotal
				return $this->db->update('order_details', $new_od_data, array("order_detail_id"=>$odi));
			}
		}
	}

	function delete($id = "")
	{
		// when user delete one item of decoration, total_decor in order_detail
		// will bei substrac too.
		$decor = $this->decor($id)->row(); // get deleted decoration
		$prod_price = $this->model_dashboard->get_price($decor->decor_id);
		$substract = $decor->qty * $prod_price;

		if($this->substract_od_subtotal($substract, $decor->order_detail_id))
		{
			// add or return back stock of decoration
			$this->add_decor_stock($decor->decor_id, $decor->qty);

			return $this->db->delete($this->decor_table, array("id"=>$id));
		}

	}

	function cancel_decor($odi)
	{
		// when user cencel adding decoration to product, total_decor in order_detail
		// will be set to 0, ofcourse.
		$new_od_data['total_decor'] = 0;

		if($this->db->update('order_details', $new_od_data, array("order_detail_id"=>$odi)))
		{
			// return back stock foreach deleted decoration items
			$deleted_decors = $this->db->get_where($this->decor_table, array("order_detail_id"=>$odi));

			if ($deleted_decors->num_rows() > 0) 
			{
				foreach ($deleted_decors->result() as $row) 
				{
					$this->add_decor_stock($row->decor_id, $row->qty);
				}
			}

			return $this->db->delete($this->decor_table, array("order_detail_id"=>$odi)); 
		}

		
	}


	function add_sub_total($addition = 0, $odi = "")
	{
		$sql = "UPDATE order_details 
				SET order_details.total_decor = order_details.total_decor + ". $addition ." 
				WHERE order_details.order_detail_id = '".$odi."'";

		return $this->db->query($sql);
	}

	function decor($id = "")
	{
		return $this->db->get_where('decoration', array("id"=>$id));
	}

	function substract_od_subtotal($substract = 0, $odi = "")
	{
		$sql = "UPDATE order_details 
				SET order_details.total_decor = order_details.total_decor - ". $substract ." 
				WHERE order_details.order_detail_id = '".$odi."'";

		return $this->db->query($sql);
	}

	function sum_decor_subtotal($odi = "")
	{
		$rec = $this->db
				->select_sum("sub_total", "sum_sub_total")
				->where("order_detail_id", $odi)
				->get("decoration");

		if($rec->num_rows() > 0)
		{
			return $rec->row()->sum_sub_total;

		} else
		{
			return 0;
		}		
	}

	/*
		used-by: inner func @modify
		desc: substract_decor_stock when user bougth decorations
	*/
	function substract_decor_stock($bel_id, $substractions)
	{
		// get prod_id by $bel_id
		$this->load->model('end_cust/model_dashboard');
		$prod_id = $this->model_dashboard->get_prod_id($bel_id);

		$do_subs_stock = "UPDATE products 
				SET products.stocks = (products.stocks - ".$substractions.") 
				WHERE products.prod_id = ".$prod_id;

		return $this->db->query($do_subs_stock);
	}

	/*
		used-by: inner func @delete, @cancel_decor
	*/
	function add_decor_stock($bel_id, $additions)
	{
		// get prod_id by $bel_id
		$this->load->model('end_cust/model_dashboard');
		$prod_id = $this->model_dashboard->get_prod_id($bel_id);

		$do_add_stock = "UPDATE products 
				SET products.stocks = (products.stocks + ".$additions.") 
				WHERE products.prod_id = ".$prod_id;
		
		return $this->db->query($do_add_stock);
	}





}

/* End of file m_decoration.php */
/* Location: ./application/models/end_cust/m_decoration.php */
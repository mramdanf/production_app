<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_income extends CI_Model {

	private $orders_table = "orders";
	private $unclose = NULL;

	/*
		used-by: Struct_Print lib (multiple_sturct func)
	*/
	function get_orders()
	{
		$this->db->order_by('order_id', 'DESC');
		$query = $this->db->get($this->orders_table);
	    
	    return $this->counting_wrapper($query);
	}

	/*
		used-by: Struct_Print lib (single_struct func)
	*/
	function get_order($order_id = NULL)
	{
		$record = $this->db->get_where($this->orders_table, array("order_id"=>$order_id));

		return $this->counting_wrapper($record);
	}

	function get_prod_by_order($order_id)
	{
		$sql = "SELECT order_details.order_id, belong_to.bel_id, products.prod_name, order_details.detail_qty, order_details.detail_price, order_details.insert_date, order_details.total_decor, products.harga, properties.name, order_details.discount, order_details.order_detail_id\n"
		    . "FROM order_details\n"
		    . "JOIN belong_to\n"
		    . "ON order_details.bel_id = belong_to.bel_id\n"
		    . "JOIN products\n"
		    . "ON products.prod_id = belong_to.product_id\n"
		    . "INNER JOIN categori\n"
			. "ON belong_to.categori_id = categori.cat_id\n"
			. "INNER JOIN properties\n"
			. "ON belong_to.properties_id = properties.id\n"
		    . "WHERE order_details.order_id = '".$order_id."'";

		$query = $this->db->query($sql);

		return $query->result();
	}

	function total_unclose()
	{
		$this->db->select_sum('order_amount');
	    $this->db->from($this->orders_table);
	    $this->db->where('close_time', $this->unclose);
	    $query = $this->db->get();

	    if($query->num_rows() > 0)
	    {
	    	if($query->row()->order_amount)
	    	{
	    		return $query->row()->order_amount;
	    	} else 
	    	{
	    		return 0;
	    	}
	    } else
	    {
	    	return 0;
	    }
	}

	function close_last_order()
	{
		date_default_timezone_set("Asia/Jakarta");
		$data["close_time"] = date("Y-m-d H:m:s");
		return $this->db->update($this->orders_table, $data, array("close_time"=>NULL));
	}

	// jika dalam satu shift tidak ada order sama sekali maka insert 1 order kosong
	function no_order_inshift($shift)
	{
		date_default_timezone_set("Asia/Jakarta");
		$data["close_time"] = date("Y-m-d H:m:s");
		$data['shift'] = $shift;
		return $this->db->insert($this->orders_table, $data);
	}

	function income_by_date($date = "")
	{
		$sql = "SELECT orders.*
				  FROM orders
				 WHERE DATE(orders.order_date) = '".$date."'";

		$record = $this->db->query($sql);

		$total = 0;
		
		foreach ($record->result() as $result) 
		{
			if($result->order_paid >= $result->order_amount)
			{
				$total += $result->order_amount;
			} else 
			{
				$total += $result->order_paid;
			}
		}

		return $total;

	}

	function get_orders_by_date($date)
	{
		$sql = "SELECT orders.*
				  FROM orders
				 WHERE DATE(orders.order_date) = '".$date."' ORDER BY orders.order_date DESC";

		$records = $this->db->query($sql);

	    return $this->counting_wrapper($records);
	}

	function income_shift($shift=-1, $date="", $month=0, $year=0)
	{

		if($date != "")
		{
			$sql = "SELECT orders.*
				  FROM orders
				 WHERE DATE(orders.order_date) = '".$date."' AND shift = '".$shift."'";
		}
		
		// condition for search income by month of year
		if($month != 0 && $year != 0)
		{
			$sql = "SELECT orders.*
				  FROM orders
				 WHERE MONTH(orders.order_date) = '".$month."' AND YEAR(orders.order_date) = '".$year."' AND shift = '".$shift."'";
		}

		$record = $this->db->query($sql);

		$total = 0;
		
		foreach ($record->result() as $result) 
		{
			if($result->order_paid >= $result->order_amount)
			{
				$total += $result->order_amount;
			} else 
			{
				$total += $result->order_paid;
			}
		}

		return $total;
	}

	function get_orders_date_shift($shift, $date)
	{
		$sql = "SELECT orders.*
				  FROM orders
				 WHERE DATE(orders.order_date) = '".$date."' 
				 AND shift = '".$shift."' ORDER BY orders.order_date DESC";

		$records = $this->db->query($sql);

	    return $this->counting_wrapper($records);
	}

	function income_month_year($month, $year)
	{
		$sql = "SELECT orders.*
				  FROM orders
				 WHERE MONTH(orders.order_date) = '".$month."'
				 AND YEAR(orders.order_date) = '".$year."'";

		$record = $this->db->query($sql);

		$total = 0;
		
		foreach ($record->result() as $result) 
		{
			if($result->order_paid >= $result->order_amount)
			{
				$total += $result->order_amount;
			} else 
			{
				$total += $result->order_paid;
			}
		}

		return $total;
	}

	function orders_date_year($month, $year, $shift=-1)
	{
		$sql = "SELECT orders.*
				  FROM orders
				 WHERE MONTH(orders.order_date) = '".$month."'
				 AND YEAR(orders.order_date) = '".$year."'";

		if($shift != -1)
		{
			$sql = "SELECT orders.*
				  FROM orders
				 WHERE MONTH(orders.order_date) = '".$month."'
				 AND YEAR(orders.order_date) = '".$year."' AND shift = '".$shift."'";
		}

		$records = $this->db->query($sql);

	    return $this->counting_wrapper($records);
	}

	function last_unclose_order()
	{
		$this->db->select('order_id');
		$query = $this->db->get_where($this->orders_table,array('close_time'=>$this->unclose));

		return $query;
	    /*$return = array();

	    foreach ($query->result() as $order)
	    {
	        $order->subs = $this->get_prod_by_order($order->order_id);

	        // Perhitungan discount, jadi discount langsung di convert ke
	        // angka bukan persentase lagi
	        foreach ($order->subs as $sub) 
	        {
	        	if($sub->discount != 0 && $sub->discount != NULL)
	        	{
	        		$total_shop = ($sub->harga*$sub->detail_qty) + $sub->total_decor;

	        		$sub->discount = $total_shop*($sub->discount/100);
	        	}
	        }
	        array_push($return, $order);
	    }

	    return $return;*/
	}

	/*
		counting some important order, products and decor resume
		@param: CI query objcet $records need to be ->result() to
											to get data from it

		@return: array of order, products, and decors with some
		order remuse
	*/
	function counting_wrapper($records = NULL)
	{
		$total_prod_qty =  $dirty_total_decor = 0;
	    $dirty_total_prod = $total_decor_qty = $total_disc = 0;

	    $return = array();

	    // loop through orders
	    foreach ($records->result() as $order)
	    {
	        $order->subs = $this->get_prod_by_order($order->order_id);

	        // loop through order_details
	        foreach ($order->subs as $sub) 
	        {
	        	$total_prod_qty += $sub->detail_qty;
	        	$dirty_total_prod += $sub->detail_price;

	        	// Perhitungan discount
	        	if($sub->discount != 0 && $sub->discount != NULL)
	        	{
	        		$total_shop = ($sub->harga * $sub->detail_qty) + $sub->total_decor;

	        		$disc_type = substr($sub->discount, strpos($sub->discount, "-") + 1);
					$sub->discount = strtok($sub->discount, "-");

					// convert to nominal if discount form was percents. note: if
					// discount in percents form, formula for calculating discount is
					// taken from $total_shop
					if(!$disc_type)
					{
						$sub->discount = $total_shop * ($sub->discount/100);				
					} else
					{
						$sub->discount = $sub->discount * $sub->detail_qty;	
					}

	        		$sub->discount = 0 - $sub->discount;
	        		$total_disc += $sub->discount;

	        		$sub->discount = number_format($sub->discount);
	        	}

	        	// subtotal of each product in order_details
	        	$sub->prod_subtotal = $sub->harga * $sub->detail_qty;

	        	$sub->harga = number_format($sub->harga);
	        	$sub->prod_subtotal = number_format($sub->prod_subtotal);
	        	$sub->total_decor = number_format($sub->total_decor);

	        	// adding decoration to sub (order_details)
	        	$this->load->model('end_cust/m_decoration');
	        	$sub->decors = $this->m_decoration->decors($sub->order_detail_id)->result();

	        	// loop through decorations data
	        	foreach ($sub->decors as $decor) 
	        	{
	        		$total_decor_qty += $decor->qty;
	        		$dirty_total_decor += $decor->sub_total;

	        		$decor->harga = number_format($decor->harga);
	        		$decor->sub_total = number_format($decor->sub_total);
	        	}    

	        }

	        // for placing in panel-heading
	        $order->order_date = $this->utility->dateTimeSimpleForm($order->order_date);

	        // adding order resume
	        // total qty of products
	        $order->total_prod_qty = $total_prod_qty;

	        // total of products sub_total 
	        $order->dirty_total_prod = $dirty_total_prod;

	        // total of decor qty
	        $order->total_decor_qty = $total_decor_qty;

	        $order->total_disc = $total_disc;

	        // total of decor sub_total
	        $order->dirty_total_decor = $dirty_total_decor;

	        // grand_total for qty
	        $order->grand_total_qty = $total_prod_qty + $total_decor_qty;

	        // grand_total for price
	        $order->grand_total = $dirty_total_prod + $dirty_total_decor + $total_disc + $order->ongkir;       

	        
	        $order->ongkir = number_format($order->ongkir);

	        $order->dirty_total_prod = number_format($order->dirty_total_prod);
	        $order->dirty_total_decor = number_format($order->dirty_total_decor);
	        $order->total_disc = number_format($order->total_disc * -1);
	        $order->grand_total = number_format($order->grand_total);
	        $order->order_paid = number_format($order->order_paid);
	        $order->order_paid_return = number_format($order->order_paid_return);

	        array_push($return, $order);

	        // set to zero again :)
	        $total_prod_qty = 0;
        	$dirty_total_prod = 0;
        	$total_decor_qty = 0;
    		$dirty_total_decor = 0;
    		$total_disc = 0;
	    }

	    return $return;
	}

	function active_shift()
	{
		$this->db->select('shift');
	    $this->db->from($this->orders_table);
	    $this->db->where('close_time', $this->unclose);
	    $query = $this->db->get();
	    
	    $ret_val = 0;
	    if ($query->num_rows() > 0) 
	    {
	    	$row = $query->first_row();
	    	$ret_val = ($row->shift==0)?'Shift Satu':'Shift Dua';
	    }
	    return $ret_val;
	}

}

/* End of file m_income.php */
/* Location: ./application/models/end_cust/m_income.php */
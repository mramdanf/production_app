<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_order extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	// order with status = menunggu
	function get_order()
	{

		$query = "SELECT orders.order_id, orders.order_name, orders.order_date, orders.order_type, 
					order_details.status
				FROM orders
				INNER JOIN order_details
				ON order_details.order_id = orders.order_id
				WHERE order_details.status = 'Menunggu'";

		$res = $this->db->query($query);

		return $res->result();
	}

	// all order detail that stok of product < 0
	function get_order_details($order_id)
	{

		$query = "SELECT products.stocks, products.prod_name, belong_to.bel_id
					FROM products
					INNER JOIN belong_to
					ON products.prod_id = belong_to.product_id
					WHERE products.prod_id IN (
					SELECT products.prod_id
					FROM order_details
					INNER JOIN belong_to
					ON order_details.bel_id = belong_to.bel_id
					INNER JOIN products
					ON belong_to.product_id = products.prod_id
					WHERE order_details.order_id = '".$order_id."')
					AND products.stocks < 0";

		$res = $this->db->query($query);

		return $res->result();

	}

	function get_all_orders()
	{
		$query = "SELECT *
				FROM orders ORDER BY order_date DESC";

		$res = $this->db->query($query);

		return $res->result();
	}

	/*function order_by_id($order_id){
		$query = "SELECT products.prod_id, belong_to.bel_id, 
					products.prod_name, categori.cat_name, properties.`name`, order_details.detail_qty, 
					order_details.status, order_details.pending, 
					order_details.notes, products.harga, 
					order_details.total_decor, order_details.order_detail_id, order_details.discount, order_details.detail_price
										
					FROM order_details
					INNER JOIN belong_to
						ON order_details.bel_id = belong_to.bel_id
					INNER JOIN products
						ON belong_to.product_id = products.prod_id
					INNER JOIN categori
						ON belong_to.categori_id = categori.cat_id
					INNER JOIN properties
						ON belong_to.properties_id = properties.id
					WHERE order_details.order_id = '".$order_id."' 
					ORDER BY order_details.order_detail_id DESC";

		$res = $this->db->query($query);

		foreach ($res->result() as $row) 
		{
			$this->load->model('end_cust/m_decoration');

			$row->total_decor = $this->m_decoration->sum_decor_subtotal($row->order_detail_id);

			$total_shop = ($row->harga*$row->detail_qty) + $row->total_decor;

    		$row->discount = $total_shop*($row->discount/100);


		}

		return $res->result();
	}*/

	function pelunasan($data,$order_id)
	{
		return $this->db->update(
			"orders",
			$data,
			array("order_id"=>$order_id)
		);
	}

	function get_order_by_id($order_id)
	{
		return $this->db->get_where('orders',array('order_id'=>$order_id));
	}

	function get_order_to_print($order_id = NULL)
	{
		$this->db->order_by('order_id', 'DESC');
		$query = $this->db->get_where('orders', array('order_id'=>$order_id));
	    $return = array();

	    foreach ($query->result() as $order)
	    {
	        $order->subs = $this->get_prod_by_order($order->order_id);

	        // Perhitungan discount, jadi discount langsung di convert ke
	        // angka bukan persentase lagi
	        foreach ($order->subs as $sub) 
	        {
	        	if($sub->discount != 0 && $sub->discount != NULL)
	        	{
	        		$total_shop = ($sub->harga*$sub->detail_qty) + $sub->total_decor;

	        		$sub->discount = $total_shop*($sub->discount/100);
	        	}
	        }
	        array_push($return, $order);
	    }

	    return $return;
	}

	function get_prod_by_order($order_id)
	{
		$sql = "SELECT order_details.order_id, belong_to.bel_id, products.prod_name, order_details.detail_qty, order_details.detail_price, order_details.insert_date, order_details.total_decor, products.harga, properties.name, order_details.discount\n"
		    . "FROM order_details\n"
		    . "JOIN belong_to\n"
		    . "ON order_details.bel_id = belong_to.bel_id\n"
		    . "JOIN products\n"
		    . "ON products.prod_id = belong_to.product_id\n"
		    . "INNER JOIN categori\n"
			. "ON belong_to.categori_id = categori.cat_id\n"
			. "INNER JOIN properties\n"
			. "ON belong_to.properties_id = properties.id\n"
		    . "WHERE order_details.order_id = '".$order_id."'";

		$query = $this->db->query($sql);

		return $query->result();
	}

	function check_decor($order_detail_id)
	{
		$rec = $this->db->get_where('decoration', array("order_detail_id"=>$order_detail_id));
		if($rec->num_rows() > 0)
		{
			return 1;

		}else 
		{
			return 0;
		}
	}

}

/* End of file model_order.php */
/* Location: ./application/models/end_cust/model_order.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dashboard extends CI_Model {

	private $table_user = 'user_table';
	private $table_order_details = 'order_details';
	private $table_orders = 'orders';
	private $table_belong_to = 'belong_to';
	private $table_products = 'products';
	private $table_categori = 'categori';
	private $fitur_table = 'fitur';

	function __construct()
	{
		parent::__construct();
	}

	function add_order_details($data)
	{
		$this->db->insert($this->table_order_details, $data);
	}

	function get_max_orderid()
	{
		$this->db->select_max('id', 'max_id');
		$query = $this->db->get($this->table_orders);

		$max_id = 0;
		
		if ($query->num_rows() > 0) 
		{
			foreach ($query->result() as $row) 
			{
				$max_order_id = $this->db->get_where($this->table_orders, array("id"=>$row->max_id));
				$max_order_id = $max_order_id->row()->order_id;
			}
		}

		return $max_order_id;
	}

	function get_price($bel_id = "")
	{
		$tmp = "SELECT products.harga AS price
				FROM belong_to
				INNER JOIN products
				ON belong_to.product_id = products.prod_id
				WHERE products.prod_id = (SELECT belong_to.product_id FROM belong_to WHERE belong_to.bel_id = '".$bel_id."')";
		$query = $this->db->query($tmp);

		$price = 0;
		if ($query->num_rows() > 0) 
		{
			foreach ($query->result() as $row) 
			{
				$price = $row->price;
			}
		}

		return $price;
	}

	function search_user($login_data)
	{
		$user = $login_data['user'];
		$pass = $login_data['pass'];
		$pass = md5($pass);

		$this->db->where('username', $user);
		$this->db->where('password', $pass);
		$this->db->where('privilage', 'End Customer');

		$query = $this->db->get($this->table_user);

		$res = '';
		$first_name = '';
		if ($query->num_rows() > 0) 
		{
			$res = 'user_found';
			foreach ($query->result() as $row) 
			{
				$first_name = $row->first_name;
			}
		} else 
		{
			$res = 'user_not_found';
		}

		$data = array('status' => $res, 'first_name' => $first_name);
		return $data;
	}

	function auto_prod_id($key)
	{
		$this->db->select('belong_to.bel_id, products.prod_name, products.prod_id, categori.cat_id, categori.cat_name');
		$this->db->from('belong_to');
		$this->db->join('categori', 'belong_to.categori_id = categori.cat_id');
		$this->db->join('products', 'belong_to.product_id = products.prod_id');
		$this->db->like('belong_to.bel_id', $key);
		
		$query = $this->db->get();

	    if($query->num_rows() > 0)
	    {
	      foreach ($query->result_array() as $row)
	      {
	        $new_row['bel_id']=htmlentities(stripslashes($row['bel_id']));
	        $new_row['prod_name']=htmlentities(stripslashes($row['prod_name']));
	        $new_row['cat_name']=htmlentities(stripslashes($row['cat_name']));
	        $row_set[] = $new_row; //build an array
	      }
	      echo json_encode($row_set); //format the array into json data
	    }
	}

	function auto_prod_name($key)
	{
		$this->db->select('belong_to.bel_id, products.prod_name, products.prod_id, categori.cat_id, categori.cat_name');
		$this->db->from('belong_to');
		$this->db->join('categori', 'belong_to.categori_id = categori.cat_id');
		$this->db->join('products', 'belong_to.product_id = products.prod_id');
		$this->db->like('products.prod_name', $key);

		$query = $this->db->get();

	    if($query->num_rows() > 0)
	    {
	      foreach ($query->result_array() as $row){

	      	$new_row['prod_name']=htmlentities(stripslashes($row['prod_name']));
	        $new_row['bel_id']=htmlentities(stripslashes($row['bel_id']));
	        $new_row['cat_name']=htmlentities(stripslashes($row['cat_name']));
	        $row_set[] = $new_row; //build an array
	      }
	      echo json_encode($row_set); //format the array into json data
	    }
	}

	// get prod name by belong id
	function prod_name_by_id($bel_id)
	{
		$this->db->select('belong_to.bel_id, products.prod_name, products.prod_id, categori.cat_id, categori.cat_name');
		$this->db->from('belong_to');
		$this->db->join('categori', 'belong_to.categori_id = categori.cat_id');
		$this->db->join('products', 'belong_to.product_id = products.prod_id');
		$this->db->where('belong_to.bel_id', $bel_id);

		return $this->db->get();
	}

	// bel id for combobox
	function get_bel_id($order_id = '')
	{

		if ($order_id == '') {
			$query = "SELECT bt.bel_id, p.prod_name, pt.name
					FROM belong_to bt
					INNER JOIN products p
						ON bt.product_id = p.prod_id
					INNER JOIN properties pt
						ON bt.properties_id = pt.id
					ORDER BY bt.bel_id ASC";
		} else {
			$query = "SELECT belong_to.bel_id
				FROM order_details
				RIGHT JOIN belong_to
				ON belong_to.bel_id = order_details.bel_id
                GROUP BY belong_to.bel_id
				HAVING belong_to.bel_id NOT IN (SELECT bel_id 
				FROM order_details WHERE order_details.order_id = '".$order_id."')
				ORDER BY belong_to.bel_id ASC";
		}

		$res = $this->db->query($query);
		return $res->result();

	}

	function get_prod_name($order_id = '')
	{

		if ($order_id == '') {
			// $query = "SELECT belong_to.bel_id
			// 		FROM belong_to
			// 		ORDER BY belong_to.bel_id ASC";
			$query = "SELECT p.prod_name, bt.bel_id, pt.name\n"
				    . "FROM products AS p\n"
				    . "INNER JOIN belong_to bt\n"
				    . "ON p.prod_id = bt.product_id \n"
				    . "INNER JOIN properties pt\n"
				    . "ON bt.properties_id = pt.id \n"
					. "ORDER BY p.prod_name ASC";
		} else 
		{
			$query = "SELECT belong_to.bel_id, products.prod_name
				FROM order_details
				RIGHT JOIN belong_to
				ON belong_to.bel_id = order_details.bel_id
				RIGHT JOIN products
				ON products.prod_id = belong_to.product_id
                GROUP BY belong_to.bel_id
				HAVING belong_to.bel_id NOT IN (SELECT bel_id 
				FROM order_details WHERE order_details.order_id = '".$order_id."')
				ORDER BY belong_to.bel_id ASC";
		}

		$res = $this->db->query($query);
		return $res->result();

	}

	function del_order_details($order_detail_id)
	{
		// im do this because, if the decoration is no exist
		// it will not produce error
		$this->load->model('end_cust/m_decoration');
		$this->m_decoration->cancel_decor($order_detail_id);
		
		return $this->db->delete($this->table_order_details, array("order_detail_id"=>$order_detail_id));
		
	}

	function sum_price($order_id)
	{
		$query = "SELECT od.*, pd.harga
					FROM order_details AS od
					INNER JOIN belong_to AS bt
					ON od.bel_id = bt.bel_id
					INNER JOIN products pd
					ON bt.product_id = pd.prod_id
					JOIN properties pt
					ON bt.properties_id = pt.id
					WHERE od.order_id = '".$order_id."' ORDER BY od.order_detail_id DESC";

		$res = $this->db->query($query);

		$total_all = 0;

		foreach ($res->result() as $row) 
		{
    		$total_shop = ($row->harga * $row->detail_qty) + $row->total_decor;

			if ($row->discount) 
			{
				$disc_type = substr($row->discount, strpos($row->discount, "-") + 1);
				$row->discount = strtok($row->discount, "-");

				// convert to nominal if discount form was percents. note: if
				// discount in percents form, formula for calculating discount is
				// taken from $total_shop
				if(!$disc_type)
				{
					$row->discount = $total_shop * ($row->discount/100);				
				
				} else 
				{
					$row->discount = $row->discount * $row->detail_qty;
				}
			}

			$row->sub_total = $total_shop - $row->discount;

    		$total_all += $row->sub_total;    		
		}

		return $total_all;
	}

	function data_order_details($order_id)
	{
		$query = "SELECT od.order_detail_id, od.order_id, bt.bel_id, pd.harga, od.detail_price, od.detail_qty, pd.prod_name, pd.stocks, od.status, od.pending, pt.name, od.notes, od.total_decor, od.discount, od.notes, od.total_decor, bt.categori_id
					FROM order_details AS od
					INNER JOIN belong_to AS bt
					ON od.bel_id = bt.bel_id
					INNER JOIN products pd
					ON bt.product_id = pd.prod_id
					JOIN properties pt
					ON bt.properties_id = pt.id
					WHERE od.order_id = '".$order_id."' ORDER BY od.order_detail_id DESC";

		$res = $this->db->query($query);

		foreach ($res->result() as $row) 
		{
			$total_shop = ($row->harga * $row->detail_qty) + $row->total_decor;

			if ($row->discount) 
			{
				// im add $tmp_disc to record for order_detail edit purpose 
				// in end_cust dashboard view
				$row->tmp_disc = $row->discount;

				$disc_type = substr($row->discount, strpos($row->discount, "-") + 1);
				$row->discount = strtok($row->discount, "-");

				// convert to nominal if discount form was percents. note: if
				// discount in percents form, formula for calculating discount is
				// taken from $total_shop. And if discount was nominal kalikan dulu
				// dengan detail_qty, krn discount nominal belong ke tiap produk
				if(!$disc_type)
				{
					$row->discount = $total_shop * ($row->discount/100);				
				
				} else 
				{
					$row->discount = $row->discount * $row->detail_qty;
				}

				
			}

			$row->sub_total = $total_shop - $row->discount;
		}

		return $res->result();
	}

	function count_order_details()
	{
		$query = $this->db->get($this->table_order_details);

		$res = 'not_last';
		if ($query->num_rows() == 1) 
		{
			$res = 'last_row';
		}

		return $res;
	}

	function update_order_details($order_detail_id, $data)
	{
		$this->db->where('order_detail_id', $order_detail_id);
		$this->db->update($this->table_order_details, $data);
	}

	function del_cur_orders($order_id)
	{
		if($this->db->delete($this->table_order_details, array("order_id"=>$order_id)))
		{
			$ret_val = $this->db->delete($this->table_orders, array("order_id"=>$order_id));

		} else 
		{
			$ret_val = false;
		}

		return $ret_val;

	}

	function process_order($data)
	{
		return $this->db->insert($this->table_orders, $data);
	}

	function update_order($data, $order_id)
	{
		return $this->db->update($this->table_orders, $data, array("order_id"=>$order_id));
	}

	function substract_stocks($bel_id, $last_qty, $curr_qty)
	{
		$prod_id = $this->get_prod_id($bel_id);

		$query = "UPDATE products 
			SET products.stocks = (products.stocks + ".$last_qty.") - ".$curr_qty." 
			WHERE products.prod_id = ".$prod_id;

		$this->db->query($query);

		$qu_get_stok = "SELECT products.stocks
						FROM belong_to
						INNER JOIN products
						ON belong_to.product_id = products.prod_id
						WHERE belong_to.bel_id = '".$bel_id."'";
		$res = $this->db->query($qu_get_stok);

		$curr_stock = 0;
		if ($res->num_rows() > 0) 
		{
			foreach ($res->result() as $row) 
			{
				$curr_stock = $row->stocks;
			}
		}

		return $curr_stock;
	}

	function update_stocks($bel_id, $last_qty, $curr_qty)
	{
		$prod_id = $this->get_prod_id($bel_id);

		// update stock
		$query = "UPDATE products 
			SET products.stocks = (products.stocks + ".$last_qty.") - ".$curr_qty." 
			WHERE products.prod_id = ".$prod_id;

		$this->db->query($query);

		// update status
		$is_minus = "SELECT products.stocks
					FROM products WHERE prod_id = ".$prod_id;

		$res = $this->db->query($is_minus);
		$pending = 0;
		$status = '';

		if ($res->num_rows() > 0) 
		{
			foreach ($res->result() as $row) 
			{

				$curr_stock = $row->stocks;
				$stock_awal = $curr_stock + ($curr_qty);

				$is_cake = $this->prod_name_by_id($bel_id)->row()->cat_id;

				if($is_cake == Dashboard::CAKE_ID) // all cake order will be pending
				{
					$pending = $curr_qty;
					$status = App_helper::LBL_ON_PROGRESS;

				} else if ($stock_awal >= 0) 
				{
					$pending = $stock_awal - $curr_qty;
					if ($pending >= 0) {
						$pending = 0;
						$status = App_helper::LBL_COMPLETE;
					} else {
						// $pending = 0 - $detail_qty;
						$pending = $pending * (-1);

						$status = App_helper::LBL_ON_PROGRESS;
					}
				} else 
				{
					$pending = (0 - $curr_qty)  * (-1);
					$status = App_helper::LBL_ON_PROGRESS;
				}

			}
		}

		$update_status = "UPDATE order_details SET status = '".$status."', pending = ".$pending." WHERE bel_id = '".$bel_id."'";
		$this->db->query($update_status);
	}

	// return back the stock after deleting order
	function suit_stock($order_id)
	{
		$res_order_details = $this->data_order_details($order_id);

		foreach ($res_order_details as $row) 
		{
			$this->update_stocks($row->bel_id, $row->detail_qty, 0);
		}
	}

	function get_prod_id($bel_id)
	{
		$query = "SELECT products.prod_id
				FROM belong_to
				INNER JOIN products
				ON products.prod_id = belong_to.product_id
				WHERE belong_to.bel_id = '".$bel_id."'";

		$res = $this->db->query($query);
		$prod_id = 0;
		if ($res->num_rows() > 0) 
		{
			foreach ($res->result() as $row) 
			{
				$prod_id = $row->prod_id;
			}
		}

		return $prod_id;
	}

	// check if order contain waiting status
	function waiting_check($order_id)
	{
		$this->db->where('order_id', $order_id);
		$res = $this->db->get('order_details');

		$is_waiting = App_helper::LBL_COMPLETE;
		foreach ($res->result() as $row) {
			if ($row->status == App_helper::LBL_ON_PROGRESS){
				$is_waiting = $row->status;
			}
		}

		return $is_waiting;
	}

	function check_prod_order($order_id, $bel_id)
	{
		$sql = "SELECT od.detail_qty\n"
		    . "FROM order_details od\n"
		    . "WHERE od.order_id = '".$order_id."'\n"
		    . "AND od.bel_id = '".$bel_id."'";

		$query = $this->db->query($sql);
		$ret_val = 0;
		if ($query->num_rows() > 0) 
		{
			$ret_val = $query->row()->detail_qty;
		}

		return $ret_val;
	}

	function update_prod_order($input_order_details, $order_id, $bel_id)
	{
		$this->db->update(
			$this->table_order_details,
			$input_order_details,
			array(
				'order_id' => $order_id,
				'bel_id' => $bel_id
				)
			);
	}

	function last_shift()
	{
		$this->db->order_by('id', 'DESC');
		$this->db->where('close_time !=', NULL);
		return $this->db->get('orders')->result();
	}

	function get_active_discount()
	{
		$records = $this->db->get_where('discount',array("is_active"=>1));

		if($records->num_rows() > 0)
		{	
			$option = "";

			$select = "<label>Discount</label>
						<select name='discount' class='form-control'>
						<option></option>";

			foreach ($records->result() as $record) 
			{
				$value = $record->jumlah . '-' . $record->jenis;
				$text = ($record->jenis) ? number_format($record->jumlah) : $record->jumlah . ' %';

				$option .= "<option value='{$value}'>{$text}</option>";
			}
			
			$select .= $option . "</select>";

			return $select;			
			
		} else 
		{
			return NULL;
		}
	}

	function get_order_by_id($order_id = "")
	{
		return $this->db->get_where($this->table_orders, array("order_id"=>$order_id));
	}

	/*
		get active fitur edit order
		@return: 1: edit_order active, 0: edit_order in-active
	*/
	function is_active_editorder()
	{
		return $this->db->get_where($this->fitur_table, array("id"=>1))->row()->is_active;
	}

}

/* End of file model_dashboard.php */
/* Location: ./application/models/end_cust/model_dashboard.php */
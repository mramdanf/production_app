<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_products_income extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function summary_all_income()
	{
		$sql = "SELECT 
					od.bel_id, p.prod_name, 
					SUM(od.detail_qty) AS sum_qty, 
					p.harga,
					od.insert_date, 
					(SUM(od.detail_qty)*p.harga) AS sum_harga, 
					SUM(od.additional_price) AS sum_decor,
					SUM( ( (p.harga*od.detail_qty) + od.additional_price ) * (od.discount/100) ) AS sum_discount,
					(SUM(od.detail_qty)*p.harga) + SUM(od.additional_price) - SUM( ( (p.harga*od.detail_qty) + od.additional_price ) * (od.discount/100) ) AS sum_subtotal

				FROM 
					order_details od
				JOIN belong_to bt
					ON od.bel_id = bt.bel_id
				JOIN products p
					ON bt.product_id = p.prod_id
				GROUP BY od.bel_id";

		$records = $this->db->query($sql);

		return $records;
	}

	function sold_by_date($date)
	{
		$sql = "SELECT \n"
		    . "	od.bel_id, p.prod_name, od.insert_date, SUM(od.detail_qty) AS sum_qty\n"
		    . "\n"
		    . "	FROM \n"
		    . "	order_details od\n"
		    . "	JOIN belong_to bt\n"
		    . "	ON od.bel_id = bt.bel_id\n"
		    . "	JOIN products p\n"
		    . "	ON bt.product_id = p.prod_id\n"
		    . " WHERE date(od.insert_date) = '".$date."'\n"
		    . " GROUP BY od.bel_id";

		return $this->db->query($sql);
	}

}

/* End of file m_products_income.php */
/* Location: ./application/models/production/m_products_income.php */
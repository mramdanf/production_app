<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_prodvis extends CI_Model 
{
	private $categories_tbl = 'categori';

	function __construct()
	{
		parent::__construct();
	}

	/*
		used-by: index() func in Products_Visual controller
		@return: $ret_data['products'] contain list of products with exel like 
				visualitaion but still in array form and will parse in Products_Visual
				$ret_data['categories'] contain array of categories
	*/
	function product_collections($categorie_id)
	{
		// Query for get prod_name base on $categori_id
		$sql = "SELECT belong_to.properties_id, products.prod_name, properties.name, belong_to.bel_id
				FROM belong_to
				INNER JOIN properties ON belong_to.properties_id = properties.id
				INNER JOIN products ON belong_to.product_id = products.prod_id
				INNER JOIN categori ON belong_to.categori_id = categori.cat_id
				WHERE belong_to.categori_id = '".$categorie_id."'";

		$records = $this->db->query($sql)->result();

		// convert to indexed array, to easy when removing duplicate prod_namae
		$tmp = array();
		foreach ($records as $key => $value) 
		{
			$tmp[$key][] = $value->prod_name;
		}

		// remove duplicate prod_name
		$tmp = array_map("unserialize", array_unique(array_map("serialize", $tmp)));

		// adding bel_id to each properties ($tmp) base on each class
		$prods = $this->products_and_propertie($categorie_id);
		
		$i=1;
		foreach ($tmp as $key => $value) 
		{
			$class = $tmp[$key][1] = $i++;

			$prod_arr = array();
			foreach ($prods as $prod_key => $prod_value) 
			{
				if ($prod_value["class"] == $class) 
				{
					$prod_arr[$prod_key]["bel_id"] = $prod_value["bel_id"];				
					$prod_arr[$prod_key]["type"] = $prod_value["type"];				
				}
			}

			$tmp[$key][2] = $prod_arr;
		}

		// $ret_data['products'] contain producst for each properties
		$ret_data['products'] = $tmp;

		// $ret_data['properties'] is for header table in products_visual view
		$ret_data['properties'] = $this->properties_by_categorie($categorie_id);


		return $ret_data;
	}

	/*============== HELPER FUNCTIONS ==================*/
	function products_and_propertie($categori_id)
	{
		$sql = "SELECT belong_to.bel_id
				FROM belong_to
				INNER JOIN properties ON belong_to.properties_id = properties.id
				INNER JOIN products ON belong_to.product_id = products.prod_id
				INNER JOIN categori ON properties.categori_id = categori.cat_id
				WHERE properties.categori_id = '".$categori_id."'";

		$records = $this->db->query($sql)->result_array();

		// set class from bel_id (remove fist and last character)
		// set type (D20, 20x40,..etc) from bel_id (last character)
		foreach ($records as $key => $value) 
		{
			$records[$key]["class"] = substr($value["bel_id"], 1, -1);
			$records[$key]["type"] = substr($value["bel_id"], -1);
		}

		// Short $records base on bel_id desc
		usort($records, function($a, $b) {
		    return $a['bel_id'] - $b['bel_id'];
		});

		return $records;
	}

	/*
		desc: return property (STD, MINI...etc) base on $categori_id
	*/
	function properties_by_categorie($categori_id)
	{
		$sql = "SELECT categori.cat_name, properties.name, properties.id
				FROM properties
				INNER JOIN categori ON properties.categori_id = categori.cat_id
				WHERE properties.categori_id = '".$categori_id."'";

		return $this->db->query($sql)->result();
	}
}

/* End of file m_prodvis.php */
/* Location: ./application/models/production/m_prodvis.php */
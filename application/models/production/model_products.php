<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_products extends CI_Model {

	private $products_table = 'products';
	private $properties_table = 'properties';
	private $belongto_table = 'belong_to';

	function __construct()	{
		parent::__construct();
	}

	function data_products(){
		$query = "SELECT products.*, belong_to.bel_id, categori.cat_name, properties.name as p_name
				FROM products
				JOIN belong_to
				ON products.prod_id = belong_to.product_id
				JOIN categori
				ON categori.cat_id = belong_to.categori_id
				LEFT JOIN properties
				ON properties.id = belong_to.properties_id 
				ORDER BY products.last_update DESC";

		$res = $this->db->query($query);

		return $res->result();
	}

	function del_product($prod_id, $bel_id){
		$this->db->where('prod_id', $prod_id);
		$this->db->delete('products');

		$this->db->where('bel_id', $bel_id);
		$this->db->delete('belong_to');
	}

	function get_categories(){
		$res = $this->db->get('categori');
		return $res->result();
	}

	function max_prod_id(){
		$this->db->select_max('prod_id', 'max_prod_id');
		$res = $this->db->get('products');

		foreach ($res->result() as $row) {
			$ret_val = $row->max_prod_id;
		}

		return $ret_val;
	}

	function max_bel_id(){

		$query = "SELECT MAX(belong_to.bel_id) as max_bel_id FROM belong_to";
		
		$res = $this->db->query($query);

		$ret_val = 'no_data';
		foreach ($res->result() as $row) {
			$ret_val = $row->max_bel_id;
		}

		return $ret_val;
	}

	function add_product($input_product){
		return $this->db->insert('products', $input_product);
	}

	function add_belong_to($input_belong){
		return $this->db->insert('belong_to', $input_belong);
	}

	function prod_details($bel_id){
		$sql = "SELECT belong_to.bel_id, products.prod_name, categori.cat_name, belong_to.product_id,\n"
			    . "products.stocks, products.harga, products.description, belong_to.categori_id, belong_to.product_id, belong_to.bel_id, belong_to.id, belong_to.properties_id\n"
			    . "FROM products\n"
			    . "INNER JOIN belong_to\n"
			    . "ON belong_to.product_id = products.prod_id\n"
			    . "INNER JOIN categori\n"
			    . "ON belong_to.categori_id = categori.cat_id\n"
			    . "WHERE belong_to.bel_id = '".$bel_id."'";

		$query = $this->db->query($sql);

		return $query;
	}

	function update_product($prod_id, $bel_id, $update_product, $update_belongto){
		$this->db->where('prod_id', $prod_id);
		$this->db->update('products', $update_product);

		$this->db->where('bel_id', $bel_id);
		$this->db->update('belong_to', $update_belongto);
	}

	function img_prod(){
		$sql = "SELECT products.*, belong_to.bel_id
			FROM products
			INNER JOIN belong_to
			ON products.prod_id = belong_to.product_id
			WHERE products.img_hot_list != ''";

		$query = $this->db->query($sql);

		return $query->result();
	}

	function img_detail($prod_id){
		$this->db->where('prod_id', $prod_id);
		$query = $this->db->get($this->products_table);

		return $query->result();
	}

	function prod_img($prod_id, $data_update){
		$this->db->where('prod_id', $prod_id);
		$this->db->update($this->products_table, $data_update);
	}

	function del_prod_img($prod_id, $data_update){
		$this->db->where('prod_id', $prod_id);
		$this->db->update($this->products_table, $data_update);
	}

	function add_image($prod_id, $data_update){
		$this->db->where('prod_id', $prod_id);
		$this->db->update($this->products_table, $data_update);
	}

	function prod_barcode($prod_id){
		
		$sql = "SELECT products.barcode FROM products WHERE products.prod_id = ".$prod_id;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function prod_barcode2($prod_id){
		
		$sql = "SELECT products.barcode FROM products WHERE products.prod_id = ".$prod_id;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function get_propertie($cat_id = NULL)
	{
		$ret_val = NULL;
		
		if($cat_id == NULL)
		{
			$ret_val = $this->db->get($this->properties_table);
		} else 
		{
			$ret_val = $this->db->get_where($this->properties_table, array('categori_id'=>$cat_id));
		}

		return $ret_val;
		
	}

	function update_product2($prod_id, $data)
	{
		return $this->db->update($this->products_table, $data, array('prod_id'=>$prod_id));
	}

	function update_belongto($id, $input_belong)
	{
		return $this->db->update($this->belongto_table, $input_belong, array('id'=>$id));
	}

}

/* End of file model_products.php */
/* Location: ./application/models/production/model_products.php */
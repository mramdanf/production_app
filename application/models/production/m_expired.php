<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_expired extends CI_Model {

	private $expired_table = "expired";

	function __construct()
	{
		parent::__construct();
	}

	function get_expireds()
	{
		$this->db
			->select('products.prod_name, expired.expired_product, expired.exp_date, expired.belong_to_id, expired.id, properties.name')
			->from($this->expired_table)
			->join('belong_to', 'belong_to.bel_id = expired.belong_to_id')
			->join('products', 'products.prod_id = belong_to.product_id')
			->join('properties', 'belong_to.properties_id = properties.id')
			->order_by('expired.last_update', 'DESC');

		return $this->db->get();

	}

	function get_expired($id = NULL)
	{
		return $this->db->get_where($this->expired_table, array("id"=>$id));
	}

	function add_expired($data)
	{
		// cari apakah produk sudah pernah diinput pada tanggal yang
		// diingginkan jika sudah jangan diinput lagi
		$last_exp = $this->get_exp_by_date($data);

		if($last_exp->num_rows() > 0) 
		{
			$last_exp = $last_exp->result_array()[0];

			if($this->del_expired($last_exp['id']))
			{
				$ret_val = $this->add_expired($data);
			}

		} else 
		{
			$ret_val = $this->db->insert($this->expired_table, $data);
		}

		return $ret_val;
	}

	function del_expired($id)
	{
		return $this->db->delete($this->expired_table, array("id"=>$id));
	}

	function get_exp_by_date($data)
	{
		return $this->db->get_where(
					$this->expired_table, 
					array("belong_to_id"=>$data['belong_to_id'], "exp_date"=>$data['exp_date'])
				);
	}

	function update_expired($data)
	{
		// antisipasi ketika user merubah belong_to_id dan
		// exp_date yang ternyata sebelumnya sudah didatabase
		$last_exp = $this->get_exp_by_date($data);

		// ketika data sudah ada di database, maka hapus data
		// kemudian reinsert
		if($last_exp->num_rows() > 0) 
		{
			log_message('error', 'duplicate data');

			if($this->del_expired($data['id']))
			{
				log_message('error', 'duplicate data was deleted');

				unset($data['id']);

				$ret_val = $this->add_expired($data);
			}

		} else 
		{
			$ret_val = $this->db->update($this->expired_table, $data, array("id"=>$data['id']));
		}

		return $ret_val;
	}

	function readd_exp_prod($last_exp, $new_exp_prod)
	{
		$last_exp = $last_exp->result_array()[0];

		$last_exp['expired_product'] += $new_exp_prod;

		return $this->db->update($this->expired_table, $last_exp, array("id"=>$last_exp['id']));
	}

}

/* End of file m_expired.php */
/* Location: ./application/models/production/m_expired.php */
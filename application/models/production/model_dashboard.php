<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dashboard extends CI_Model {

	private $table_user = 'user_table';

	function __construct(){
		parent::__construct();
	}

	function signin($data)
	{
		$this->db->where('username', $data['username']);
		$this->db->where('password', md5($data['password']));

		$res = $this->db->get($this->table_user);

		$return_val = '';
		if ($res->num_rows() > 0) {
			$return_val = 'user_found';
		} else $return_val = 'user_not_found';

		return $return_val;
	}

	// because its for production so status is waiting
	function get_all_orders()
	{
		$query = "SELECT orders.order_id, orders.order_name, orders.order_date, orders.order_type, 
					orders.status, orders.*
				FROM orders
				INNER JOIN order_details
				ON order_details.order_id = orders.order_id
				GROUP BY orders.order_id
				HAVING orders.status = '".App_helper::LBL_ON_PROGRESS."' ";

		$res = $this->db->query($query);

		return $res->result();
	}

	// select order_id dari tabel orders yang statusnya on progress
	function get_minus_stock()
	{
		$query = "SELECT order_details.bel_id, products.prod_name, order_details.pending
					FROM order_details
					INNER JOIN orders
					ON orders.order_id = order_details.order_id
					INNER JOIN belong_to
					ON belong_to.bel_id = order_details.bel_id
					INNER JOIN products
					ON products.prod_id = belong_to.product_id
					WHERE orders.status = '".App_helper::LBL_ON_PROGRESS."' AND order_details.pending > 0";

		$res = $this->db->query($query);

		return $res->result();
	}

	function get_prod_byid($bel_id)
	{
		$query = "SELECT belong_to.bel_id, belong_to.product_id, (products.stocks*(-1)) as stocks, 
					products.prod_name
					FROM belong_to
					INNER JOIN products
					ON belong_to.product_id = products.prod_id
					WHERE belong_to.bel_id = '".$bel_id."'";

		return $this->db->query($query);
	}

	function update_stock($prod_id, $sum_maded)
	{
		$query = "UPDATE products SET products.stocks = products.stocks + ".$sum_maded." WHERE products.prod_id = '".$prod_id."'";

		$this->db->query($query);
	}

	function update_pending($order_detail_id, $prod_id, $order_id, $new_pending)
	{

		//add stock by new pending
		/*$query = "UPDATE products SET stocks = stocks + ".$new_pending." WHERE prod_id = ".$prod_id;
		$this->db->query($query);*/
		
		// update pending value
		$query = "UPDATE order_details
					SET order_details.pending = order_details.pending - ".$new_pending."
					WHERE order_details.order_detail_id = ".$order_detail_id;
		$this->db->query($query);

		// update status tabel order details
		$get_pending = "SELECT pending FROM order_details WHERE order_detail_id = ".$order_detail_id;
		$res = $this->db->query($get_pending);

		foreach ($res->result() as $row) 
		{
			$curr_pending = $row->pending;
			if ($row->pending > 0) 
			{
				$status = App_helper::LBL_ON_PROGRESS;

			} else 
			{
				$status = App_helper::LBL_COMPLETE;
			}
		}

		$update_status = "UPDATE order_details SET order_details.status = '".$status."' WHERE order_details.order_detail_id = " .$order_detail_id;
		$this->db->query($update_status);

		// cek semua status dari order detail dan jika semua statusnya langsung maka update status di
		// tabel orders
		
		// pengecekan apakah pada $order_id di tabel order detail itu masi
		// ada yang statusnya on progress ?
		$query = "SELECT order_details.status
				FROM order_details
				WHERE order_details.order_id = '".$order_id."'
				AND order_details.status = '".App_helper::LBL_ON_PROGRESS."'";

		$res = $this->db->query($query);
		if ($res->num_rows() > 0) 
		{
			// masih ada yang statusnya on progress
			$status_orders = App_helper::LBL_ON_PROGRESS;

		} else 
		{
			$status_orders = App_helper::LBL_COMPLETE;
		}
		  

		$query = "UPDATE orders SET orders.status = '".$status_orders."' 
				WHERE orders.order_id = '".$order_id."'";
		$this->db->query($query);
	}

	// get order detail id for update
	function get_target_update($bel_id)
	{

		$query = "SELECT order_details.order_detail_id
					FROM order_details
					WHERE order_details.status = '".App_helper::LBL_ON_PROGRESS."' AND order_details.bel_id = '".$bel_id."' AND order_details.order_detail_id = (SELECT MIN(order_details.order_detail_id) FROM order_details WHERE order_details.status = '".App_helper::LBL_ON_PROGRESS."')";

		$res = $this->db->query($query);
		$ret_val = '';
		foreach ($res->result() as $row) {
			$ret_val = $row->order_detail_id;
		}

		return $ret_val;
	}

	function prod_id_bybelid($bel_id)
	{
		$query = "SELECT products.prod_id
				FROM products
				INNER JOIN belong_to
				ON products.prod_id = belong_to.product_id
				WHERE belong_to.bel_id = '".$bel_id."'";

		$res = $this->db->query($query);
		foreach ($res->result() as $row) {
			$ret_val = $row->prod_id;
		}

		return $ret_val;
	}

	// select order id tabel orders dengan status = on progress dan ordernya paling kecil
	function get_order_id()
	{
		$query = "SELECT MIN(orders.order_id) as min_order
				FROM orders
				GROUP BY orders.status
				HAVING orders.status = '".App_helper::LBL_ON_PROGRESS."'";

		$res = $this->db->query($query);
		$ret_val = 'no_data';
		if ($res->num_rows() > 0) {
			foreach ($res->result() as $row) {
				$ret_val = $row->min_order;
			}
		}

		return $ret_val;
	}

	function waiting_orders()
	{
		$sql = "SELECT *
				FROM orders
				WHERE orders.`status` = '".App_helper::LBL_ON_PROGRESS."' ORDER BY orders.order_date DESC";
		$query = $this->db->query($sql);

		return $query->result();
	}

	function order_details($order_id)
	{
		// get order_detail by order id
		$sql = "SELECT order_details.order_detail_id, order_details.bel_id, 
				order_details.detail_qty, order_details.pending, products.prod_name, order_details.notes 
				FROM order_details
				INNER JOIN belong_to
				ON order_details.bel_id = belong_to.bel_id
				INNER JOIN products
				ON products.prod_id = belong_to.product_id
				WHERE order_details.order_id = '".$order_id."'
				AND order_details.pending > 0";

		$records = $this->db->query($sql)->result();

		foreach ($records as $record) 
		{
			$record->decor = 0;
			if($this->check_decor($record->order_detail_id) > 0)
			{
				$record->decor = 1;
			}
		}
		return $records;
	}

	function order_detail_id($order_detail_id)
	{
		// get order detail by order detail id
		$sql = "SELECT order_details.order_detail_id, order_details.bel_id, order_details.order_id, 
				order_details.detail_qty, order_details.pending, products.prod_name, products.prod_id 
				FROM order_details
				INNER JOIN belong_to
				ON order_details.bel_id = belong_to.bel_id
				INNER JOIN products
				ON products.prod_id = belong_to.product_id
				WHERE order_details.order_detail_id = '".$order_detail_id."'";

		return $this->db->query($sql);
	}

	function check_decor($order_detail_id)
	{
		$rec = $this->db->get_where('decoration',array("order_detail_id"=>$order_detail_id));
		if($rec->num_rows() > 0)
		{
			return 1;

		}else 
		{
			return 0;
		}
	}


}

/* End of file model_dashboard.php */
/* Location: ./application/models/production/model_dashboard.php */
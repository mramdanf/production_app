<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_categories extends CI_Model {

	private $categories_table = 'categori';
	private $properties_table = 'properties';

	function __construct(){
		parent::__construct();
	}

	function get_categories(){
		$this->db->order_by('cat_id', 'DESC');
		$query = $this->db->get($this->categories_table);
		return $query->result();
	}

	function add_cat($input_cat){
		return $this->db->insert($this->categories_table, $input_cat);
	}

	function del_cat($cat_id)
	{		
		if($this->db->delete($this->categories_table, array("cat_id"=>$cat_id)))
		{
			return $this->db->delete('properties',array("categori_id"=>$cat_id));
		}
	}

	function get_category($cat_id){
		$this->db->where('cat_id', $cat_id);
		$query = $this->db->get($this->categories_table);
		return $query;
	}

	function update_cat($cat_id, $update_cat){
		$this->db->where('cat_id', $cat_id);
		return $this->db->update($this->categories_table, $update_cat);
	}

	function del_properties_cat($cat_id)
	{
		return $this->db->delete($this->properties_table, array('categori_id'=>$cat_id));
	}

	function categories_properties()
	{
		$sql = "SELECT c.cat_name, pr.name\n"
		    . "FROM belong_to bt \n"
		    . "INNER JOIN categori c\n"
		    . "	ON bt.categori_id = c.cat_id\n"
		    . "INNER JOIN properties pr \n"
		    . "	ON bt.properties_id = pr.id\n"
		    . "GROUP BY bt.properties_id";

		return $this->db->query($sql);
	}

}

/* End of file model_categoreis.php */
/* Location: ./application/models/production/model_categoreis.php */
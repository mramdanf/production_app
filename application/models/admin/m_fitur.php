<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_fitur extends CI_Model {

	private $fitur_table = "fitur";

	function __construct()
	{
		parent::__construct();
	}

	function get_fiturs()
	{
		return $this->db->get($this->fitur_table);
	}

	function activate($id)
	{
		$data = array(
			"is_active"=>1
			);
		return $this->db->update($this->fitur_table, $data, array("id"=>$id));
	}

	function inactivate($id)
	{
		$data = array(
			"is_active"=>0
			);
		return $this->db->update($this->fitur_table, $data, array("id"=>$id));
	}

}

/* End of file m_fitur.php */
/* Location: ./application/models/admin/m_fitur.php */
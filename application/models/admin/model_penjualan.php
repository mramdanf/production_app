<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_penjualan extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	function penjualan(){
		// query untuk di tabel data penjualan
		$sql = "SELECT order_details.order_id, belong_to.bel_id, products.prod_name, order_details.detail_qty, order_details.detail_price, order_details.insert_date\n"
		    . "FROM order_details\n"
		    . "INNER JOIN belong_to\n"
		    . "ON order_details.bel_id = belong_to.bel_id\n"
		    . "INNER JOIN products\n"
		    . "ON products.prod_id = belong_to.product_id";

		$query = $this->db->query($sql);

		return $query->result();
	}

}

/* End of file model_penjualan.php */
/* Location: ./application/models/admin/model_penjualan.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_discount extends CI_Model {

	private $discount_table = "discount";

	function __construct()
	{
		parent::__construct();
	}

	function get_discounts()
	{
		$discounts = $this->db->get($this->discount_table);

		foreach ($discounts->result() as $discount) 
		{
			// give a user friendly label when is_active contain active or in active
			if($discount->is_active)
			{
				$discount->active = "<span class='label label-primary'>AKTIF</span>";
			
			} else 
			{
				$discount->active = "<span class='label label-danger'>TIDAK AKTIF</span>";
			}

			// change jenis 0:persen; 1:nominal
			$discount->jenis = ($discount->jenis)?'Nominal':'Persen';
		}

		return $discounts;
	}

	function get_discount($id = NULL)
	{
		return $this->db->get_where($this->discount_table, array("id"=>$id));
	}

	function update_discount($id, $data)
	{
		return $this->db->update($this->discount_table, $data, array("id"=>$id));
	}

	function do_activate($id)
	{
		$data['is_active'] = 1;
		return $this->db->update($this->discount_table, $data, array("id"=>$id));
	}

	function do_unactivate($id)
	{
		$data['is_active'] = 0;
		return $this->db->update($this->discount_table, $data, array("id"=>$id));
	}

}

/* End of file m_discount.php */
/* Location: ./application/models/admin/m_discount.php */
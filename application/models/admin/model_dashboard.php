<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dashboard extends CI_Model {

	private $user_table = 'user_table';

	function __cunstruct(){
		parent::__cunstruct();
		$this->load->library('database');
	}

	// nebeng fungsi buat check username dan password admin
	function check_admin($username, $password){
		$this->db->where('username', $username);
		$this->db->where('password', md5($password));

		$query = $this->db->get($this->user_table);

		if ($query->num_rows() > 0) {
			$ret_val = 'admin_found';
		} else {
			$ret_val = 'admin_not_found';
		}

		return $ret_val;
	}

	function users(){
		$this->db->where('privilage !=', 'admin');
		$this->db->order_by('user_table_id', 'DESC');
		$query = $this->db->get($this->user_table);

		return $query->result();
	}

	function del_user($user_table_id){
		$this->db->where('user_table_id', $user_table_id);
		$this->db->delete($this->user_table);
	}

	function add_user($input_user){
		$this->db->insert($this->user_table, $input_user);
	}

	function user_details($user_table_id){
		$this->db->where('user_table_id', $user_table_id);
		$query = $this->db->get($this->user_table);

		return $query->result();
	}

	function do_edit($update_user, $user_table_id){
		$this->db->where('user_table_id', $user_table_id);
		$this->db->update($this->user_table, $update_user);
	}

	// check_existing_username on insert query
	function check_existing_username($username){
		$this->db->where('username', $username);
		$query = $this->db->get($this->user_table);

		if ($query->num_rows() > 0) {
			$ret_val = true; 
		} else $ret_val = false; // username existing

		return $ret_val;
	}

	// check_existing_username on update query
	function check_update_username($username, $user_table_id){
		$this->db->where('username', $username);
		$this->db->where('user_table_id !=', $user_table_id);
		$query = $this->db->get($this->user_table);

		if ($query->num_rows() > 0) {
			$ret_val = true; 
		} else $ret_val = false; // username existing

		return $ret_val;
	}

	function reset_pass($user_table_id, $data){
		$this->db->where('user_table_id', $user_table_id);
		$this->db->update($this->user_table, $data);
	}

}

/* End of file user_model.php */
/* Location: ./application/models/admin/user_model.php */
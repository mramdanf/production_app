<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __construct(){
		parent::__construct();
	}
}

/**
* 
*/
class Endcust_Controller extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();

		$priv = $this->session->userdata('privilage');
		if ($priv != 'End Customer') {
			// redirect(base_url('end_cust/dashboard/go_signin'));
			$this->load->view('end_cust/login');
		} else {
			redirect(base_url('end_cust/dashboard'));
		}
	}


}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
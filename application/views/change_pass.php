<!DOCTYPE html>
<html>
<head>
    <title>Heavenshard</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <link href="<?php echo base_url(); ?>/assets/css/jquery.dataTables.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>/assets/css/bootstrap.css" rel="stylesheet" />

    <script src="<?php echo base_url(); ?>/assets/js/jquery-1.12.3.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.js"></script>
</head>

<body>
    <div class="container">    
        <div id="loginbox" style="margin-top:100px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> 
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Change password</div>
                </div>     

                <div class="panel-body" >
                    <?php $alert_msg = $this->session->flashdata('alert_msg'); 
                        $alert_type = $this->session->flashdata('alert_type');
                    ?>
                    <?php if ($alert_msg != null) { ?>
                    <?php if($alert_type == null) $alert_type = 'success'; ?>
                    <div class="alert alert-<?php echo $alert_type; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        &times;</button>
                        <?php echo $alert_msg; ?>
                    </div>
                    <?php } ?>
                    <form action="<?php echo base_url(); ?>account/change_pass" method="post">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                            <input type="text" name="username" class="form-control" placeholder="Username" aria-describedby="basic-addon1" required id="username">
                        </div>
                        <div class="input-group" style="margin-top: 15px;">
                            <span class="input-group-addon" id="basic-addon1">
                                <i class="glyphicon glyphicon-lock"></i>
                            </span>
                            <input type="password" name="old_password" class="form-control" placeholder="Password Lama" aria-describedby="basic-addon1" required id="old_password">
                        </div>
                        <div class="input-group" style="margin-top: 15px;">
                            <span class="input-group-addon" id="basic-addon1">
                                <i class="glyphicon glyphicon-lock"></i>
                            </span>
                            <input type="password" name="new_password" class="form-control" placeholder="Password Baru" aria-describedby="basic-addon1" required id="new_password">
                        </div>
                        <div class="input-group" style="margin-top: 15px;">
                            <span class="input-group-addon" id="basic-addon1">
                                <i class="glyphicon glyphicon-repeat"></i>
                            </span>
                            <input type="password" name="confirm_pass" class="form-control" placeholder="Ketikan Ulang Password Baru" aria-describedby="basic-addon1" required id="confirm_pass">
                        </div>
                        <div class="form-group pull-right" style="margin-top: 20px;">
                            <button type="submit" class="btn btn-primary" id="do_reset">
                                <span class="glyphicon glyphicon-ok"></span> Change
                            </button>
                            <button type="reset" class="btn btn-danger">
                                <span class="glyphicon glyphicon-minus-sign"></span> Ulangi
                            </button>
                            <a href="<?php echo base_url(); ?>" class="btn btn-success">
                                <span class="glyphicon glyphicon-home"></span> Home
                            </a>
                        </div>
                    </form>
                </div>                     
            </div>  
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#do_reset').click(function(e){
            var jv_new_password = $('#new_password').val();
            var jv_old_password = $('#old_password').val();
            var jv_confirm_pass = $('#confirm_pass').val();

            if (jv_old_password == jv_new_password) {
                e.preventDefault();
                alert('Reset password gagal, password baru dan password lama tidak boleh sama.');
            }

            if (jv_confirm_pass != jv_new_password) {
                e.preventDefault();
                alert('Reset password gagal, password baru tidak cocok dengan konfirmasi password');
            }

            console.log('new: ' + jv_new_password + ' ');
            console.log('old: ' + jv_old_password + ' ');
            console.log('confirm: ' + jv_confirm_pass + ' ');
        });
    });
</script>


</body>
</html>

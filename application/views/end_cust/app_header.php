<!DOCTYPE html>
<html>
<head>
	<title><?=($title)?$title:'Heavenshard'?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<link href="<?php echo base_url(); ?>/assets/css/jquery.dataTables.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/css/bootstrap.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/css/jquery-ui.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/css/bootstrap-chosen.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">

	<script src="<?php echo base_url(); ?>/assets/js/jquery-1.12.3.min.js"></script>

	<style type="text/css">

		html {
		  position: relative;
		  min-height: 100%;
		}
		body {
		  /* Margin bottom by footer height */
		  margin-bottom: 60px;
		}
		.footer {
		  position: absolute;
		  bottom: 0;
		  width: 100%;
		  /* Set the fixed height of the footer here */
		  height: 60px;
		  background-color: #f5f5f5;
		}
		
	</style>
	
</head>

<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="container-fluid">
				
				<div class="navbar-header">
					<a class="navbar-brand" href="<?php echo base_url(); ?>end_cust/dashboard">Heavenshard</a>
		    	</div>
		    	
		    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    	<ul class="nav navbar-nav">
			    		<li><a href="<?php echo base_url(); ?>end_cust/order">Order</a></li>
			    		<li><a href="<?php echo base_url(); ?>end_cust/dashboard/products">Data Produk</a></li>
			    		<li><a href="<?php echo base_url(); ?>end_cust/income">Pendapatan</a></li>
			      	</ul>
			      	<div class="navbar-right">
	                    <a href="<?php echo base_url(); ?>end_cust/dashboard/signout" class="btn btn-danger navbar-btn">
	                        <span class="glyphicon glyphicon-log-out"></span> Sign out
	                    </a>
	                </div>
			    </div>

			</div>
		</div>
			
	</nav>

<style>
	.form-group select {
		max-width: 230px;
		margin-bottom: 20px;
	}
</style>
<div class="container">
	<div class="row">
		<ol class="breadcrumb">
          <li><a href="<?php echo base_url(); ?>end_cust/dashboard" style="text-decoration: none;">End Customer</a></li>
          <li class="active">Data Produk</li>
        </ol>
		<div class="panel panel-info">
			<div class="panel-heading">
				Produk
			</div>
			<div class="panel-body">
				<div class="form-group">
					<label>Cari Kategori</label>
					<select name="categories" class="form-control" placeholder="Cari Kategori">
						<option value="unselected"></option>
						<?php 
						foreach ($categories as $categorie) {
							echo "<option value='{$categorie->cat_name}'>{$categorie->cat_name}</option>";
						} ?>
					</select>
				</div>
				<div class="table-responsive">
					<table id="products_table" width="100%" class="table">
						<thead>
							<tr>
								<th>No</th>
								<th>Kode</th>
								<th>Nama</th>
								<th>Kategori</th>
								<th>Properti</th>
								<th>Harga</th>
								<th>Stok</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; foreach ($products as $row) { ?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $row->bel_id; ?></td>
								<td><?php echo $row->prod_name; ?></td>
								<td><?php echo $row->cat_name; ?></td>
								<td><?php echo $row->p_name; ?></td>
								<td><?php echo number_format($row->harga); ?></td>
								<td><?php echo $row->stocks; ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var prodTable = $('#products_table').DataTable();

		$("select[name='categories']").change(function() {
			if($(this).val() == "unselected") {
				prodTable
					 .search( '' )
					 .columns().search( '' )
					 .draw();
			} else {
				prodTable
			        .columns( 3 )
			        .search("^" + $(this).val() + "$", true, false, true)
			        .draw();
			} 
			
		});
	});
</script>
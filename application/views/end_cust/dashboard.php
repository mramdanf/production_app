<style>
	#form_do_order,
	.btn-row {
		margin-top: 20px;
	}
	.checkbox {
		margin-top: 20px;
	}
	.checkbox label {
		font-size: 12pt;
    	font-weight: bold;
	}
</style>
<?php error_reporting(0) ?>
<div class="container">

	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
			  <li><a href="<?php echo base_url(); ?>end_cust/dashboard" style="text-decoration: none;">End Customer</a></li>
			  <li class="active">Home</li>
			</ol>
		</div>
			
	</div>

	
	<div class="row">

		<?php if($edit_order) { ?>
		<div class="col-md-12">
			<div class="panel panel-info panel-void">
				<div class="panel-heading">
					Edit Order
				</div>
				<div class="panel-body">
					<form action="<?=site_url('end_cust/dashboard/display_order')?>" method="post" class="form-inline" id="form-edit-order">
						<div class="form-group">
							<label>No. Order</label>
							<?php 
								$is_udpate=""; if($this->session->userdata('is_update')) {
									$is_update = $this->session->userdata('order_id');
								} 
							 ?>
							<input type="text" name="key_order_id" class="form-control" value="<?=$is_update?>">
						</div>
						<div class="form-group">
							<input type="submit" value="Cari" class="btn btn-primary">
							<input type="submit" value="Batal" class="btn btn-danger">
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php } ?>

	</div>

	
	<div class="row">

		<!-- div add product -->
		<div class="col-md-12">

			<div class="panel panel-info" id="wrapper-add-product">
				<div class="panel-heading">
					Form Order
				</div>
				<div class="panel-body">

					<!-- form add_order_detail -->
					<form 
						action="<?=site_url('end_cust/dashboard/add_order_details')?>" 
						method="post" 
						id="form_add_order">

						<!-- hidden input for helper -->
						<input type="hidden" name="order_detail_id">
						<input type="hidden" name="last_qty">

						<div class="row form-group">
							
							<div class="col-md-4">

								<label>Kode Produk</label>
								<select 
									data-placeholder="- Kode produk -" 
									name="bel_id" 
									class="form-control">

									<option value="unselected"></option>
									<?php foreach ($bel_id as $row) { ?>
									<option value="<?=$row->bel_id?>"><?=$row->bel_id?></option>
									<?php } ?>
								</select>

							</div>

							<div class="col-md-4">

								<label>Nama Produk</label>
								<select 
									data-placeholder="- Nama Produk -" 
									name="no_name" 
									class="form-control">
									<option value="unselected"></option>
									<?php foreach ($prod_name as $row) { ?>
									<option value="<?=$row->bel_id?>"><?=$row->prod_name?> - <?=$row->name?></option>
									<?php } ?>
								</select>

							</div>

							<div class="col-md-4">

								<label>Quantity</label>
								<input 
									type="number" 
									name="detail_qty" 
									class="form-control" 
									id="detail_qty"
									required />

							</div>

						</div>

						<div class="row form-group">
							
							<div class="col-md-4">

								<label>Wording</label>
								<textarea 
									name="notes" 
									rows="4" 
									class="form-control"></textarea>

							</div>

							<div class="col-md-4">

								<?php if($discount) { ?>
								<?=$discount?>
								<?php  } ?>

							</div>

						</div>

						<div class="row form-group">

							<div class="col-md-6">
								<input type="submit" value="Simpan" class="btn btn-primary">
								<input type="reset" value="Reset" class="btn btn-danger">
							</div>

						</div>

					</form>

				</div>
			</div>

		</div>

	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info" id="wrapper-order">
				<div class="panel-heading">
					Order Details
				</div>
				<div class="panel-body">
					<div class="tabel-responsive">

						<!-- table order_detail -->
						<table id="order_details_table" width="100%" class="table">
		                    <thead>
		                        <tr>
		                            <th>No.</th>
		                            <th>Kd. Produk</th>
		                            <th>Nama Produk</th>
		                            <th>Hrg. Satuan</th>
		                            <th>Qty</th>
		                            <th>Total Decor</th>
		                            <th>Disc.</th>
		                            <th>Sub total</th>
		                            <th style="min-width: 100px;">Wording</th>
		                            <th>Status</th>
		                            <th>Pilihan</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                        <?php $i = 1; foreach ($order_details as $row) {?>
		                        <tr>
		                        	<input type="hidden" name="odi_td" value="<?=$row->order_detail_id?>">
		                            <input type="hidden" name="stock_td" value="<?=$row->stocks?>">
		                            <input type="hidden" name="disc_td" value="<?=$row->tmp_disc?>">
		                            <td><?=$i++?></td>
		                            <td><?=$row->bel_id?></td>
		                            <td><?=$row->prod_name?> - <?=$row->name?></td>
		                            <td><?=$row->harga?></td>
		                            <td><?=$row->detail_qty?></td>
		                            <td><?=$row->total_decor?></td>
		                            <td><?=$row->discount?></td>
		                            <td><?=$row->sub_total?></td>
		                            <td><?=$row->notes?></td>
		                            <td>
		                            	<?php $type = 'label label-primary'; if ($row->status == App_helper::LBL_ON_PROGRESS) { $type = 'label label-danger'; } ?>
		                            	<span class="<?php echo $type; ?>"><?php echo $row->status; ?></span>
		                            </td>
		                            <td>
		                                <a title="Edit data" class="btn btn-success btn_edit">
		                                	<span class="glyphicon glyphicon-pencil"></span>
		                                </a>
		                                <a class="btn btn-danger btn_delete" title="Hapus data">
		                                    <span class="glyphicon glyphicon-trash"></span>
		                                </a>

		                                <!-- decor just allowed for cake -->
		                                <?php if ($row->categori_id == "2") { ?>
		                                <a href="<?=site_url('end_cust/decoration/index')?><?='/'.$row->order_detail_id?>" class="btn btn-warning" title="Decorasi" style="color: white;">
		                                    <span class="glyphicon glyphicon-plus"></span>
		                                </a>
		                                <?php } ?>

		                            </td>
		                        </tr>
		                        <?php } ?>
		                    </tbody>
		                </table>

					</div>

					<!-- form process order -->
	        		<form action="<?php echo base_url(); ?>end_cust/dashboard/print_pdf_order" method="post" id="form_do_order">
	        			<div class="row">
	        				<div class="col-md-3">
	        					<div class="form-group">
									<label>Order No.</label>
									<input type="text" name="order_id" class="form-control" value="<?php echo $order_id; ?>" id="order_id" readonly="true">
									<input type="hidden" name="is_pickup" value="<?=($order->order_type)?$order->order_type:'Pickup'?>" id="is_pickup">
								</div>
	        				</div>	
	        				<div class="col-md-3">
								<div class="form-group">
									<label>Pajak (Rp)</label>
									<input type="text" name="order_tax" id="order_tax" class="form-control" readonly="true">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Total Harga (Rp)</label>
									<input type="text" name="order_amount" id="order_amount" class="form-control" readonly value="<?php echo $detail_price; ?>">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Uang bayar (Rp)</label>
									<input type="text" name="order_paid" id="order_paid" class="form-control" required="true" value="<?=($order->order_paid==0)?'':number_format($order->order_paid)?>">
								</div>
							</div>
	        			</div>
	        			<div class="row">
		        			<div class="col-md-3">
		        				<div class="form-group">
									<label>Uang kembali (Rp)</label>
									<input type="text" name="order_paid_return" id="order_paid_return" class="form-control" readonly="true">
								</div>
		        			</div>
		        			<div class="col-md-3">
		        				<div class="form-group">
									<label> Nama Pembeli</label>
									<input type="text" name="order_name" class="form-control deliv-field" id="order_name" readonly="true" value="<?=$order->order_name?>">
								</div>
		        			</div>
		        			<div class="col-md-3">
		        				<div class="form-group">
									<label>Tanggal pengiriman</label>
									<input type="text" name="order_deliv_date" class="form-control deliv-field" readonly="true" value="<?=$order->order_deliv_date?>">
								</div>
		        			</div>
		        			<div class="col-md-3">
		        				<div class="form-group">
									<label>Nomor Telepon Pengirim</label>
									<input type="text" name="order_phone" class="form-control deliv-field" readonly="true" value="<?=$order->order_phone?>">
								</div>
		        			</div>
	        			</div>
	        			<div class="row">
	        				<div class="col-md-3">
	        					<div class="form-group">
									<label>Alamat Pengiriman</label>
									<textarea name="order_add" class="form-control deliv-field" rows="4" readonly="true"><?=$order->order_add?></textarea>	
								</div>
	        				</div>
	        				<div class="col-md-3">
	        					<div class="form-group">
									<label>Nama Penerima</label>
									<input type="text" name="order_recipient" class="form-control deliv-field" readonly="true" value="<?=$order->order_recipient?>">	
								</div>
	        				</div>
	        				<div class="col-md-3">
	        					<div class="form-group">
									<label>Nomor Telepon Penerima</label>
									<input type="text" name="order_phone_recipient" class="form-control deliv-field" readonly="true" value="<?=$order->order_phone_recipient?>">	
								</div>
	        				</div>
	        				<div class="col-md-3">
	        					<div class="form-group">
									<label>Biaya Kirim</label>
									<input type="text" name="ongkir" class="form-control deliv-field auto-format" readonly="true" value="<?=($order->ongkir==0)?'':number_format($order->ongkir)?>">	
								</div>
	        				</div>
	        			</div>
	        			<div class="row">
	        				<div class="col-md-12">
	        					<div class="form-group">
									<a href="#delete_confirm" class="btn btn-primary" id="btn_proses" data-toggle="modal">
										<span class="glyphicon glyphicon-ok"></span> Proses
									</a>
									<a href="#delete_confirm" class="btn btn-danger" title="Hapus order" id="btn_del_all" data-toggle="modal">
										<span class="glyphicon glyphicon-trash	"></span> Hapus
									</a>
									<a href="javascript:void(null);" class="btn btn-success" id="btn-delivery">
										<span class="glyphicon glyphicon-home"></span> Delivery
									</a>
									<a href="javascript:void(null);" class="btn btn-success" id="btn-cencel-deliv" hidden>
										<span class="glyphicon glyphicon-minus-sign"></span> Batal
									</a>
								</div>
	        				</div>
	        			</div>
					</form>
	    		</div>
			</div>
		</div>
			
	</div>

</div>

<script type="text/javascript">
	var var_order_detail_id;
	var dropDownProdName = $("select[name='no_name']");
	var dropDownKdProd = $("select[name='bel_id']");

	$(document).ready(function(){
        var base_url = "<?=site_url('')?>";
        
        onLoadMode();

        /*================= ORDER DETAIL PROCESS ===================== */

        dropDownKdProd.chosen({

		    search_contains: true

		}).change(function(){
			var selected_bel_id = $('select option:selected').val();

			$('#bel_id_tmp').val(selected_bel_id);
			$('#detail_qty').val('');
			$("#dropdown_prod_name").val(selected_bel_id);
			$('#dropdown_prod_name').trigger("chosen:updated");

		});
		
		dropDownProdName.chosen({

		    search_contains: true

		}).change(function() {

			var selected_bel_id = $(this).val();

			dropDownKdProd.val(selected_bel_id);
			dropDownKdProd.trigger("chosen:updated");

		});
		

        $('#order_details_table').DataTable({
        	scrollX: true
        });

        $('#form_add_order').submit(function(e){

        	if (dropDownKdProd.val() == "unselected") {
        		e.preventDefault();
        		alert('Mohon untuk memilih Kode Produk dan Nama Produk');
        	}

        	var inputQty = $("input[name='detail_qty']");

        	if (inputQty.val() == 0 || inputQty.val() == '') {
        		e.preventDefault();
        		alert('Qty tidak boleh kosong atau nol.');
        	}
        });

        // set dropdown prodname and product code to default
	    $("#form_add_order").bind("reset", function() {

			dropDownKdProd.val('unselected');
	        dropDownKdProd.trigger("chosen:updated");

	        dropDownProdName.val('unselected');
	        dropDownProdName.trigger("chosen:updated");
		});
		
		$('.btn_delete').click(function() {

			if(!confirm("Anda yakin ?")) {
				return;
			}

			var parentTr = $(this).closest('tr');

			var lastQty = $('td:eq(4)', parentTr).text();
			var belId = $('td:eq(1)', parentTr).text();
			var oderDetailId = parentTr.find("input[name='odi_td']").val();

			$.ajax({
				type: 'POST',
				url: base_url+'end_cust/dashboard/del_order_details',
				data: {order_detail_id: oderDetailId, last_qty: lastQty, bel_id: belId},
				success: function(){
					window.location.replace(base_url+'end_cust/dashboard');
				}
			});
		});

		$('.btn_edit').click(function(){

			var parentTr = $(this).closest('tr');
			var discountTd = $("input[name='discount_td']");

			$('#form_add_order').attr('action', base_url+'end_cust/dashboard/update_order_details');

			$("input[name='order_detail_id']").val(parentTr.find("input[name='odi_td']").val());

			$("input[name='last_qty']").val($('td:eq(4)', parentTr).text());

			$("input[name='detail_qty']").val($('td:eq(4)', parentTr).text());

			$("textarea[name='notes']").val($('td:eq(8)', parentTr).text());

			dropDownKdProd.val($('td:eq(1)', parentTr).text());
			dropDownKdProd.trigger("chosen:updated");

			dropDownProdName.val($('td:eq(1)', parentTr).text());
			dropDownProdName.trigger("chosen:updated");

			$("select[name='discount']").val(parentTr.find("input[name='disc_td']").val());

		});




		/*================= ORDER PROCESS ===================== */

		$('#btn_del_all').click(function() {

			if ($('#order_id').val() == '') {

				alert('Tidak ada order untuk dihapus.');
				return false;

			} else {
				
				if(!confirm("Anda yakin ?")) {
					return;
				}

				$.post(
					base_url+'end_cust/dashboard/del_cur_orders', 
					{var_order_id: $('#order_id').val()},
					function(){
						window.location.replace(base_url+'end_cust/dashboard');
					}
				);
			}
		});       


		$('#btn_proses').click(function() {

		    if(!confirm("Anda yakin ?")){
		    	return;
		    }

		    $('#form_do_order').trigger('submit');

			
		});

		$( "#order_paid" ).keyup(function(evt) {

		  if ($('#order_amount').val() == '' || $('#order_amount').val() == 0) {

		  	alert('Total harga tidak boleh kosong.');
		  	$(this).val('');

		  } else {

		  	  var n = parseInt($(this).val().replace(/\D/g,''),10);

		  	  if (n > 0) {
				  var price = parseInt($('#order_amount').val().replace(/\D/g,''),10);
				  var paid_money = parseInt($('#order_paid').val().replace(/\D/g,''),10);
				  if((paid_money - price) <=0 ) {
				  	$('#order_paid_return').val("");
				  } else {
				  	$('#order_paid_return').val((paid_money - price).toLocaleString());
				  }

				   $(this).val(n.toLocaleString());
				  
		  	  }
		  }

		});

		var orderTotal = $("input[name='order_amount']");
		var orderTotalVal = parseInt(orderTotal.val().replace(/\D/g,''),10);
		$("input[name='ongkir']").keyup(function(event) {
			if($(this).val() != '' && $(this).val() != 0) {
				var ongkir = parseInt($(this).val().replace(/\D/g,''),10);
				if(ongkir > 0) {
					orderTotal.val((orderTotalVal+ongkir).toLocaleString());
				}
			} else {
				orderTotal.val(orderTotalVal.toLocaleString());
			}
		});

		// give thoushand separator on keyup
		$(".auto-format").keyup(function() {
			var currVal = $(this).val();
			$(this).val(thousandSeparator(currVal));
		});

		var nn = parseInt($('#order_amount').val().replace(/\D/g,''),10);
		if (nn>0) {
			$('#order_amount').val(nn.toLocaleString());
		} else {
			$('#order_amount').val('');
		}

		// untuk validasi input, yang mana hanya angka yang di izinkan untuk diinput
		$(".auto-format").keydown(function (e) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	             // Allow: Ctrl+A, Command+A
	            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
	             // Allow: home, end, left, right, down, up
	            (e.keyCode >= 35 && e.keyCode <= 40)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        // Ensure that it is a number and stop the keypress
	        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
	    });

	    $("input[value='Batal']").click(function() {

	    	$("input[name='key_order_id']").val("");
	    });

	    $("input[name='order_deliv_date']").datetimepicker({
		    dateFormat: 'dd M yy',
		    timeFormat: 'HH:mm:ss'
		});


		$('#btn-delivery').click(function(){
			
			$('#btn-cencel-deliv').show();

            $(".deliv-field").attr('readonly', false);

	        $('#btn-delivery').hide();
	        $('#is_pickup').val('Delivery');


		});

		$('#btn-cencel-deliv').click(function(){
			
			$('#btn-cencel-deliv').hide();

			$(".deliv-field").attr('readonly', true);

	        $('#btn-delivery').show();
	        $('#is_pickup').val('Pickup');

	        $('#order_name').val('');
	        $('#order_date').val('');
	        $('#order_phone').val('');
	        $('#order_add').val('');
		});
		
	});


	/*================= HELPER FUNCTIONS ===================== */

	function thousandSeparator(data) {
		data = parseInt(data.replace(/\D/g,''),10);
		if (data > 0) {
			return data.toLocaleString();
		}
	}

	function onLoadMode() {
		$('#btn-cencel-deliv').hide();
        $('#btn-delivery').show();
        $('#btn_update').hide();
        $('#btn_cencel_update').hide();

        // set focus ke dropdown dropdown_prod_name ketika pertama load page
        dropDownProdName.chosen().trigger('chosen:activate');

        // scroll down sehingga mudah ketika input produk
        $('html, body').animate({
	        scrollTop: $("#wrapper-add-product").offset().top
	    }, 500);

        // ketika proses edit order, kembalian disesuaikan
        countOrderPaidReturn();
	}

	function countOrderPaidReturn() {
		var n = parseInt($('#order_paid').val().replace(/\D/g,''),10);
		if (n > 0) {
			var price = parseInt($('#order_amount').val().replace(/\D/g,''),10);
			var paid_money = parseInt($('#order_paid').val().replace(/\D/g,''),10);

			if((paid_money - price) <=0 ) {

				$('#order_paid_return').val("");
				
			} else {
				$('#order_paid_return').val((paid_money - price).toLocaleString());
			}

			$('#order_paid').val(n.toLocaleString());

		}
	}
</script>


<script>
	/*shortcut key fiture*/

	jQuery(document).ready(function() {
		
		document.addEventListener('keyup', doc_keyUp, false);

		
		function doc_keyUp(e) {
			// ctrl+shift+a
		    if (e.ctrlKey && e.keyCode == 65 && e.shiftKey) {
		        $("input[name='order_paid']").focus();
		        $('html, body').animate({
			        scrollTop: $("#wrapper-order").offset().top
			    }, 500);
		    }

		    // ctrl+shift+s
		    if (e.ctrlKey && e.keyCode == 83 && e.shiftKey) {
		        $("#btn_proses").trigger('click');
		    }
		}
	});

	
</script>

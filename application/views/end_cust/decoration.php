<style>
	.bottom_gap {
		margin-bottom: 20px;
	}
	#sum_decor {
		font-size: 12pt;
		font-weight: bold;
	}
</style>
<div class="container">

	<div class="row">
		<div class="col-md-12">
			<?php $alert = $this->session->flashdata('alert'); ?>
			<?php if ($alert != null) { ?>
			<div class="alert <?=$alert['type']?> alert-dismissable" role="alert">
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
			    <?php echo $alert['msg']; ?>
			</div>
			<?php } ?>
		</div>
	</div>

	<?php if($mode) { ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info" id="wrapper_form_decor">
				<div class="panel-heading">
					Form Decor
				</div>
				<div class="panel-body">
					
					<form action="<?=site_url('end_cust/decoration/modify')?>" method="post">

						<!-- hidden input for helper -->
						<input type="hidden" name="id">
						<input type="hidden" name="order_detail_id" value="<?=$order_detail->order_detail_id?>">
						<input type="hidden" name="last_qty">
						
						<div class="row form-group">
							
							<div class="col-md-4">
								<label>No. Order</label>
								<input type="text" class="form-control" value="<?=$order_detail->order_id?>" readonly>
							</div>

							<div class="col-md-4">
								<label>Nama Produk</label>
								<input type="text" class="form-control" value="<?=$order_detail->prod_name?> <?=$order_detail->name?>" readonly>
							</div>

						</div>

						<div class="row form-group">
							
							<div class="col-md-4">
								<label>Kode Produk Decor</label>
								<select 
									data-placeholder="- Kode produk -" 
									name="decor_id" 
									class="form-control">

									<option value="unselected"></option>
									<?php foreach ($bel_id as $row) { ?>
									<option value="<?=$row->bel_id?>"><?=$row->bel_id?></option>
									<?php } ?>
								</select>
							</div>

							<div class="col-md-4">
								<label>Nama Produk Decor</label>
								<select 
									data-placeholder="- Nama Produk -" 
									id="prod_name" 
									class="form-control">
									<option value="unselected"></option>
									<?php foreach ($prod_name as $row) { ?>
									<option value="<?=$row->bel_id?>"><?=$row->prod_name?> - <?=$row->name?></option>
									<?php } ?>
								</select>
							</div>

							<div class="col-md-2">

								<label>Quantity</label>
								<input type="number" name="qty" class="form-control" required>
								
							</div>

						</div>

						<div class="row form-group">
							
							<div class="col-md-12">
								<input type="submit" value="Simpan" class="btn btn-primary">
								<a href="<?=site_url('end_cust/dashboard')?>" class="btn btn-warning">Kembali</a>
								<a id="cancel_decor" class="btn btn-danger">Hapus Decor</a>
							</div>

						</div>

					</form>

				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	
	<?php if(!$mode) { ?>
	<a style="margin-bottom: 20px;" href="javascript:history.back()" class="btn btn-danger">Kembali</a>

	<p style="font-size: 12pt;"><?=$order_detail->prod_name?> <?=$order_detail->name?></p>
	<?php } ?>

	<div class="row">
		<div class="col-md-12">

			<div class="panel panel-info">
				<div class="panel-heading">
					Data Decor
				</div>
				<div class="panel-body">

					<div class="row bottom_gap">
						<div class="col-md-4">
							<p id="sum_decor">Total Decor: <?=number_format($sum_decor_subtotal)?></p>
						</div>
					</div>

					<div class="table-responsive">
						<table class="table" id="decor_table">
							<thead>
								<tr>
									<th>No</th>
									<th>Kode Produk</th>
									<th>Nama Produk</th>
									<th>Properti</th>
									<th>Qty</th>
									<th>Hrg. Satuan</th>
									<th>Sub total</th>

									<?php if($mode) { ?>
									<th>Pilihan</th>
									<?php } ?>

								</tr>
							</thead>
							<tbody>
								<?php $i=1;
									foreach ($decors->result() as $decor) {
								?>
								<tr>
									<!-- hidden input inside table for helper -->
									<input type="hidden" name="id_td" value="<?=$decor->id?>">

									<td><?=$i++?></td>
									<td><?=$decor->decor_id?></td>
									<td><?=$decor->prod_name?></td>
									<td><?=$decor->name?></td>
									<td><?=$decor->qty?></td>
									<td><?=$decor->harga?></td>
									<td><?=$decor->sub_total?></td>

									<?php if($mode) { ?>
									<td><a title="Edit data" class="btn btn-success btn_edit">
		                                	<span class="glyphicon glyphicon-pencil"></span>
		                                </a>
		                                <a class="btn btn-danger btn_delete" title="Hapus data">
		                                    <span class="glyphicon glyphicon-trash"></span>
		                                </a></td>
		                            <?php } ?>
		                            
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>

</div>
<script>
	$(document).ready(function() {

		var dropDownKdProd = $("select[name='decor_id']");
		var dropDownProdName = $("#prod_name");

		dropDownProdName.chosen({

		    search_contains: true

		}).change(function() {

			var belId = $(this).val();

			dropDownKdProd.val(belId);
	        dropDownKdProd.trigger('chosen:updated');

			
		});

		dropDownKdProd.chosen({

		    search_contains: true

		}).change(function() {

			var belId = $(this).val();

			dropDownProdName.val(belId);
			dropDownProdName.trigger('chosen:updated');

		});

		dropDownProdName.chosen().trigger('chosen:activate');

		$("#cancel_decor").click(function() {

			if(!confirm("Anda yakin ?")) {
				return;
			}

			var orderDetailId = $("input[name='order_detail_id']").val();

			$.ajax({
				type: 'POST',
				url: baseUrl + 'end_cust/decoration/cancel_decor',
				data: {order_detail_id: orderDetailId},
				success: function(data){
					if(data)
						window.location.replace(baseUrl + 'end_cust/dashboard');
					else 
						window.location.replace(window.location.href);
				}

			});
		});



		/*======================= TABLE DECOR ====================== */

		var decorTable = $("#decor_table").DataTable();
		var baseUrl = "<?=site_url('')?>";

		decorTable.on('click', '.btn_edit', function() {
			
			var parentTr = $(this).closest('tr');

			$("input[name='id']").val(parentTr.find("input[name='id_td']").val());

			$("input[name='qty']").val($('td:eq(4)', parentTr).text());
			$("input[name='last_qty']").val($('td:eq(4)', parentTr).text());

			dropDownKdProd.val($('td:eq(1)', parentTr).text());
			dropDownKdProd.trigger('chosen:updated');

			dropDownProdName.val($('td:eq(1)', parentTr).text());
			dropDownProdName.trigger('chosen:updated');

		});

		decorTable.on('click', '.btn_delete', function() {
			
			if(!confirm("Anda yakin ?")) {
				return;
			}

			var parentTr = $(this).closest('tr');

			var dataId = parentTr.find("input[name='id_td']").val();
			var orderDetailId = $("input[name='order_detail_id']").val();

			$.ajax({
				type: 'POST',
				url: baseUrl + 'end_cust/decoration/delete',
				data: {id: dataId, odi: orderDetailId},
				success: function(){
					window.location.replace(window.location.href);
				}

			});

		});

		/*=================== ON LOAD VIEW SETTINGS =================*/
		<?php if($mode) { ?>
		$('html, body').animate({
	        scrollTop: $("#wrapper_form_decor").offset().top
	    }, 500);
		<?php } ?>

	});
</script>
<style>
	.my-row>:nth-child(2n+1) {  
	  clear:left;
	}
	.od-table td {
		padding: 5px;
	}
	ul li {
		list-style: none;
	 padding: 0;
	}

	.well p,
	.well ul li,
	.bold-text {
		font-size: 15pt;
	}
	.gap {
		border-top: solid 3px grey;
	}
	.bordering-bottom {
		border-bottom: solid 3px grey;
	}
	#form-resume {
		margin-bottom: 30px;
	}
	.wrapper-search-type {
		margin-bottom:  20px;
	}
	.number-form {
		text-align: right;
	}
</style>
<div class="container">
	<div class="row">
		<ol class="breadcrumb">
          <li><a href="<?php echo base_url(); ?>end_cust/dashboard" style="text-decoration: none;">End Customer</a></li>
          <li class="active">Pendapatan</li>
        </ol>
	</div>
	<div class="row">
		<?php $alert = $this->session->flashdata('alert'); ?>
		<?php if ($alert != null) { ?>
		<div class="alert <?=$alert['type']?> alert-dismissable" role="alert">
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
		    <?php echo $alert['msg']; ?>
		</div>
		<?php } ?>
		<?php if($active_shift) { ?>
		<div class="alert alert-warning alert-dismissable" role="alert">
		    Shift aktif saat ini adalah <strong><?=$active_shift?></strong>
		</div>
		<?php } ?>
		<div class="panel panel-info">
			<div class="panel-heading">
				Resume Pendapatan
			</div>
			<div class="panel-body">

				<div class="row wrapper-search-type">
					<div class="col-md-12">
						<label class="radio-inline">
		                    <input name="IsSmallBusiness" type="radio" value="0">
		                    Pendapatan Harian
		                </label>
		                <label class="radio-inline">
		                    <input name="IsSmallBusiness" type="radio" value="1">
		                    Pendapatan Bulanan
		                </label>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<form method="get" class="form-inline" id="form-resume" action="<?=site_url('end_cust/income/adv_search')?>">

							<!-- hidden input -->
							<input type="hidden" name="to_day">

							<div class="form-group" id="tgl_wrapper">
								<label>Tanggal</label>
								<input type="text" name="key_date" class="form-control">
							</div>

							<div class="form-group income_month">
								<label>Bulan</label>
								<select name="month" class="form-control">
									<option></option>
									<?php $i=1; foreach ($months_id as $month) { ?>
									<option value="<?=$i++?>"><?=$month?></option>
									<?php } ?>
								</select>
							</div>

							<div class="form-group income_month">
								<label>Tahun</label>
								<select name="year" class="form-control">
									<option></option>
									<option>2016</option>
								</select>
							</div>

							<div class="form-group" id="shift_wrapper">
								<label>Shift</label>
								<select name="shift" class="form-control">
									<option></option>
									<option value="0">Satu</option>
									<option value="1">Dua</option>
								</select>
							</div>

							<div class="form-group">
								<input type="submit" value="Cari" class="btn btn-primary">
								<a href="<?=site_url('end_cust/income')?>" class="btn btn-primary">Refresh</a>
								<input type="submit" value="Hari ini" class="btn btn-primary">
								<a href="#close-confirm" data-toggle="modal" class="btn btn-primary" id="btn-close">Closing</a>
							</div>
						</form>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<p class="bold-text">
							Filter: <?=$income_filter?>
						</p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<div class="well">
							<p>Total Pendapatan</p>
							<ul>
								<li><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Rp. <?=number_format($total_income)?></li>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="well">
							<p>Total Pendapatan Shift Satu</p>
							<ul>
								<li><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Rp. <?=number_format($income_shift_satu)?></li>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="well">
							<p>Total Pendapatan Shift Dua</p>
							<ul>
								<li><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Rp. <?=number_format($income_shift_dua)?></li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="row">
		<div class="panel panel-info">
			<div class="panel-heading">
				Order<?=($shift!="")?" - ".$shift:""?>
			</div>
			<div class="panel-body">
				<div class="row my-row">
					<?php
						if(count($orders) <=0)
							echo "<div class='col-md-4'><p>* Order kosong.</p></div>";
					?>
					<?php foreach ($orders as $order) { ?>
					<div class="col-xs-6 col-lg-6">
						<div class="panel panel-info">
							<div class="panel-heading">
								<?=$order->order_id?> &nbsp;&nbsp;&nbsp; <?=$order->order_date?>
							</div>
							<div class="panel-body">
								<table width="100%" class="od-table">
									<thead>
										<tr>
											<th>Produk</th>
											<th>Qty</th>
											<th>Harga</th>
											<th>Sub Total</th>
											<th>Decor</th>
											<th>Disc.</th>
										</tr>
									</thead>
									<tbody>

									<!-- Products loop -->
									<?php foreach ($order->subs as $sub) { ?>
										<tr>
											<td><?=$sub->prod_name?> - <?=$sub->name?></td>
											<td><?=$sub->detail_qty?></td>
											<td class="number-form"><?=$sub->harga?></td>
											<td class="number-form"><?=$sub->prod_subtotal?></td>
											<td class="number-form"><?=$sub->total_decor?></td>
											<td class="number-form"><?=$sub->discount?></td>
										</tr>
									<?php } ?>

										<!-- line devider -->
										<tr class="gap"><td colspan="6"></td></tr>

										<!-- Decors -->
										<?php 
											foreach ($order->subs as $sub) {
												if(count($sub->decors) > 0) {
										?>
										<tr>
											<td><?=$sub->prod_name?> - <?=$sub->name?></td>
										</tr>
												<?php foreach ($sub->decors as $decor) { ?>
										<tr>
											<td><?=$decor->prod_name?> - <?=$decor->name?></td>
											<td><?=$decor->qty?></td>
											<td class="number-form"><?=$decor->harga?></td>
											<td class="number-form"><?=$decor->sub_total?></td>
												<?php } ?>
										</tr>
																		
										<?php
												} 
											} 
										?>

										<?php if($order->total_decor_qty > 0) { ?>
										<!-- line devider -->
										<tr class="gap"><td colspan="6"></td></tr>
										<?php } ?>

										<!-- Order resume -->
										<tr>
											<td>Total Produk</td>
											<td colspan="4"><?=$order->total_prod_qty?></td>
											<td class="number-form"><?=$order->dirty_total_prod?></td>
										</tr>
										<tr>
											<td>Total Decor</td>
											<td colspan="4"><?=$order->total_decor_qty?></td>
											<td class="number-form"><?=$order->dirty_total_decor?></td>
										</tr>
										<tr>
											<td colspan="5">Total Disc.</td>
											<td class="number-form"><?=$order->total_disc?></td>
										</tr>
										<tr>
											<td colspan="5">Biaya Kirim</td>
											<td class="number-form"><?=$order->ongkir?></td>
										</tr>
										<tr>
											<td>Grand Total</td>
											<td colspan="4"><?=$order->grand_total_qty?></td>
											<td class="number-form"><?=$order->grand_total?></td>
										</tr>
										<tr>
											<td colspan="5">Tunai</td>
											<td class="number-form"><?=$order->order_paid?></td>
										</tr>
										<tr>
											<td colspan="5">Kembali</td>
											<td class="number-form"><?=$order->order_paid_return?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Closing Confirm Dialog -->
<div id="close-confirm" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Closing Confirmation</h4>
			</div>
			<div class="modal-body">
				Print struk untuk shift ini ?
			</div>
			<div class="modal-footer">
				<button onclick="closePrint()" type="button" class="btn btn-primary" data-dismiss="modal">Print</button>
				<button onclick="justClose()" type="button" class="btn btn-success" data-dismiss="modal">Hanya Close</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script>
	function closePrint() {
		window.open("<?=site_url('end_cust/income/closing/1')?>", "_self");
	}

	function justClose() {
		window.open("<?=site_url('end_cust/income/closing')?>", "_self");
	}
	jQuery(document).ready(function($) {
		var inputKeyDate = $("input[name='key_date']");

		/*======== hidding some thing =========== */
		$("#tgl_wrapper").hide();
		$(".income_month").hide();
		$("#shift_wrapper").hide();
		$("input[value='Cari']").hide();

		inputKeyDate.datetimepicker({
		    dateFormat: 'dd M yy',
		    showTimepicker: false
		});

		$("input[value='Hari ini']").click(function(event) {
			$("input[name='to_day']").val('to_day');
		});

		$("input[type='radio']").click(function(event) {
			
			$("#shift_wrapper").show();
			$("input[value='Cari']").show();

			if($(this).val() == 0) {

				$("#tgl_wrapper").show();
				$(".income_month").hide();

			} else if ($(this).val() == 1) {

				$(".income_month").show();
				$("#tgl_wrapper").hide();
			}
		});
	});
</script>
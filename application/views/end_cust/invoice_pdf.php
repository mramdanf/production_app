<!DOCTYPE html>
<html>
<head>
	<title>Invoice</title>
	<style type="text/css">
		.heder_tbl {
			background-color: #dcdcdc;
		}

		.body_tbl td {
			text-align: center;
		}

		.header_section {
			text-align: center;
			margin-bottom: 30px;
		} 
		 
	</style>
</head>
<body>
	<div class="header_section">
		<h2>Heavenshard</h2>
		<p>Jl. Kemang Utara IX No. 48-B, Duren Tiga, Pancoran Jakarta Selatan.</p>
	</div>
	<p>
		<?php echo $order_id; ?> <br>
		<?php echo $order_date ?> <br>
		Kasir: <?php echo $first_name ?>
	</p>
	<table width="100%">
        <thead>
            <tr class="heder_tbl">
                <th>No</th>
                <th>Kode Product</th>
                <th>Nama Product</th>
                <th>Harga Satuan</th>
                <th>Sub total</th>
                <th>Quantity</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 0; foreach ($order_details as $row) {?>
            <tr class="body_tbl">
                <td><?php echo(++$i) ?></td>
                <td><?php echo($row->bel_id); ?></td>
                <td><?php echo($row->prod_name); ?></td>
                <td><?php echo(number_format($row->harga)); ?></td>
                <td><?php echo(number_format($row->detail_price)); ?></td>
                <td><?php echo($row->detail_qty); ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <p>
    	Total (Rp): <?php echo $detail_price; ?> <br>
    	Tunai (Rp): <?php echo number_format($order_paid); ?> <br>
    	Kembalian (Rp): ?>
    </p>
    <hr>
    <?php if ($order_type != 'Pickup') { ?>
    <p>
	    Nama pembeli: <?php echo $order_name; ?> <br>
	    Tanggal pengiriman: <?php echo $order_deliv_date; ?><br>
	    Nomor telpon: <?php echo $order_phone; ?><br>
	    Alamat: <?php echo $order_add; ?><br>
    </p>
    <?php } ?>
</body>
</html>
<style>
    .row-form,
    .row-text-show {
        margin-bottom: 20px;
    }
    #text-show a {
        cursor: pointer;
        color: #3174c7;
        font-size: 12pt;
    }
    #search-form {
        margin-bottom: 20px;
    }
</style>
<link type="text/css" href="<?=site_url('assets/css/dataTables.checkboxes.css')?>" rel="stylesheet" />
<div class="container">
	<div class="row">
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url(); ?>end_cust/dashboard" style="text-decoration: none;">End Customer</a></li>
          <li class="active">Order</li>
        </ol>
        <?php $alert = $this->session->flashdata('alert'); ?>
        <?php if ($alert != null) { ?>
        <div class="alert <?=$alert['type']?> alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['msg']; ?>
        </div>
        <?php } ?>
        <div class="panel panel-info">
            <div class="panel-heading">
                Pelunasan
            </div>
            <div class="panel-body">
                <div class="row row-form">
                    <div class="col-md-12">
                        <form method="post" action="<?=site_url('end_cust/order/update_paid')?>" class="form-inline">
                            <div class="form-group">
                                <label>Tagihan (Rp)</label>
                                <input type="text" class="form-control" name="tagihan" readonly>
                            </div>
                            <div class="form-group">
                                <label>Uang Kembali (Rp)</label>
                                <input type="text" class="form-control" name="order_paid_return" readonly>
                            </div>
                            <div class="form-group">
                                <label>Bayar Pelunasan (Rp)</label>
                                <input type="text" class="form-control" name="order_paid">
                                <input type="hidden" class="form-control" name="order_id">
                            </div>

                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="<?=site_url('end_cust/order')?>" title="Batal Edit" class="btn btn-danger">Batal</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
		<div class="panel panel-info">
			<div class="panel-heading">
				Order
			</div>
			<div class="panel-body">
                <div class="row row-text-show">
                    <div class="col-md-12">
                        <p id="text-show"><a>Show/hide Delivery Information</a></p>
                    </div>
                </div>

                <!-- hidden form for list of selected row -->
                <form action="<?=site_url('end_cust/order/print_multiple_struct')?>" method="post" id="selected_list" hidden>
                </form>

                <div class="form-inline" id="search-form">
                    <div class="form-group">
                        <input type="text" name="key_date" placeholder="Cari Tanggal" class="form-control" value="<?=$key_date?>">
                    </div>
                    <div class="form-group">
                        <select name="shift" class="form-control">
                            <option value="unselected"></option>
                            <option value="0">Satu</option>
                            <option value="1">Dua</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="button" value="Cari" class="btn btn-success">
                        <a id="refresh-order" class="btn btn-primary">Refresh</a>
                        <a id="print_struct" class="btn btn-warning">Print Struk</a>
                    </div>
                </div>

                <div class="table-responsive">

                    <!-- tabel orders -->
                    <table id="order_table" class="table">
                        <thead>
                            <tr>
                                <th style="width: 30px;"></th>
                                <th style="width: 90px;">Order No.</th>
                                <th style="min-width: 150px;">Tgl Order</th>
                                <th style="min-width: 150px;">Tgl</th>
                                <th style="min-width: 150px;">Shift</th>
                                <th style="min-width: 90px;">Total</th>
                                <th style="min-width: 90px;">Tagihan</th>
                                <th style="min-width: 90px;">Bayar</th>
                                <th style="min-width: 90px;">Kembalian</th>
                                <th style="min-width: 90px;">Status Order</th>
                                <th style="min-width: 90px;">Kelunasan</th>
                                <th style="min-width: 90px;">Tipe Order</th>
                                <th style="min-width: 90px;">Tgl Delivery</th>
                                <th style="min-width: 90px;">Biaya Kirim</th>
                                <th style="min-width: 90px;">Pembeli</th>
                                <th style="min-width: 90px;">Tlp Pembeli</th>
                                <th style="min-width: 90px;">Penerima</th>
                                <th style="min-width: 90px;">Tlp Penerima</th>
                                <th style="min-width: 90px;">Alamat Kirim</th>
                                <th style="min-width: 90px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>
			</div>
		</div>
	</div>
	<div class="row">
        <input type="hidden" id="tmp_order_id">
		<div class="panel panel-info">
			<div class="panel-heading">
				Order Detail
			</div>
			<div class="panel-body">
				<div class="col-md-3">
					<div class="form-group">
						<label> Order No.</label>
						<input type="text" id="order_id" class="form-control" readonly>
					</div>
				</div>
				<table id="order_detail" width="100%" class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kd. Produk</th>
                            <th style="min-width: 120px;">Nama Produk</th>
                            <th>Properti</th>
                            <th>On Progress</th>
                            <th>Status</th>
                            <th>Hrg. Satuan</th>
                            <th>Qty</th>
                            <th>Total Decor</th>
                            <th>Disc.</th>
                            <th>Sub Total</th>
                            <th style="min-width: 90px;">Wording</th>
                            <th>Decor</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
			</div>
		</div>
	</div>
</div>

<script>
    var jv_order_id;

	$(document).ready(function(){

        var base_url = '<?=site_url('')?>';
        var curr_length=0, last_length=0;
        var counter=0;
        var is_complete="";
        var order_arr = [];
        var varOrderTable = $("#order_table");
        var varTagihan = $("input[name='tagihan']");
        var keyDate = $("input[name='key_date']");
        var ddShift = $("select[name='shift']");

        $("input[name='order_paid']").attr('readonly', true);

        var ordersTable = varOrderTable.DataTable( {
            ajax: base_url+'end_cust/order/json_orders',
            scrollX: true,
            columnDefs: [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    },
                    {
                        "targets": [ 3 ],
                        "visible": false
                    },
                    {
                        "targets": [ 4 ],
                        "visible": false
                    }
                ],
            select: {
                  'style': 'multi'
               }

        } );

        // auto refresh order table
        setInterval( function () {
            ordersTable.ajax.reload(null, false);
        }, 10000 );


        var ordersDetailsTable = $('#order_detail').DataTable( {
            ajax: {
                type: 'POST',
                url: base_url+'end_cust/order/get_order_details',
                data: function(d) {
                    d.order_id = $("#tmp_order_id").val()
                }

            },
            scrollX: true
        });

        // this condition was needed when user back from decoration page
        if($("#tmp_order_id").val() != "" && $("#tmp_order_id").val() != null) {
            // auto refresh order details
            ordersDetailsTable.ajax.reload(null, false);

            setInterval( function () {
                ordersDetailsTable.ajax.reload(null, false);
            }, 10000 );
        }


        varOrderTable.on("click", ".btn_view", function(event) {
            jv_order_id = $(this).attr('id');
            $('#tmp_order_id').val(jv_order_id);
            $('#order_id').val(jv_order_id);

            // auto refresh order details
            ordersDetailsTable.ajax.reload(null, false);

            setInterval( function () {
                ordersDetailsTable.ajax.reload(null, false);
            }, 10000 );

        });

        varOrderTable.on("click", ".btn_edit", function(event) {
            var parentTr = $(this).closest('tr');
            varTagihan.val($('td:eq(4)', parentTr).text());
            $("input[name='order_id']").val($('td:eq(1)', parentTr).text());
            $("input[name='order_paid']").attr('readonly', false);
        });

        $("input[name='order_paid']").keyup(function() {
            if($(this).val() != "" && $(this).val() != 0) {
                var dataTagihan = parseInt(varTagihan.val().replace(/\D/g,''),10);
                var dataNewPaid = parseInt($(this).val().replace(/\D/g,''),10);
                var dataPaidReturn = dataNewPaid-dataTagihan;
                if(dataPaidReturn > 0)
                    $("input[name='order_paid_return']").val(dataPaidReturn.toLocaleString());
                else
                    $("input[name='order_paid_return']").val("");

                $(this).val(dataNewPaid.toLocaleString());

            } else {
                $(this).val("");
                $("input[name='order_paid_return']").val("");
            }
        });

        // hide delivery information on first load
        toggleVis();

        $("#text-show a").click(function() {
            // toggle visibility of delivery informations
            toggleVis();
        });


        // search by date
        keyDate.datetimepicker({
            dateFormat: 'dd M yy',
            showTimepicker: false
        });

        $("input[value='Cari']").click(function() {
            ordersTable
                 .search( '' )
                 .columns().search( '' )
                 .draw();
                    
            ordersTable
                    .columns(3) // column 3 was hidden :)
                    .search("^" + keyDate.val() + "$", true, false, true)
                    .draw();

            if (ddShift.val() != "unselected") {

                ordersTable
                    .columns(4)
                    .search("^" + ddShift.val() + "$", true, false, true)
                    .draw();                
            }
            
        });

        $("#refresh-order").click(function() {
            ordersTable
                 .search( '' )
                 .columns().search( '' )
                 .draw();
            keyDate.val('');
            ddShift.val('unselected');

            // verify if there is checked row, if there trigger click
            // on checkboxes-select-all
            var found=0;
            $('#order_table').find('input[type="checkbox"]:checked').each(function () {
               found++;
            });
            if(found)
                $("th.checkboxes-select-all").trigger('click');
        });

        // printing struct
        $("#print_struct").click(function() {
            
            var listForm = $("#selected_list");

            var rows_selected = ordersTable.column(0).checkboxes.selected();
            
            var count=0;

            var rows = $("tr", $("#order_table"));

            // Iterate over all selected checkboxes
            $.each(rows_selected, function(index, rowId){
                count++;
                // Create a hidden element
                listForm.append(
                 $('<input>')
                    .attr('type', 'text')
                    .attr('name', 'order_ids[]')
                    .val(rows.eq(rowId).find('td:eq(1)').text())
                );
            });
            
            if(count)
                listForm.trigger('submit');
            else 
                alert("Anda belum memilih order untuk diprint.");
        });


        function toggleVis() {
            for (var i = 12; i <= 18; i++) {
                // Get the column API object
                var column = ordersTable.column( i );

                // Toggle the visibility
                column.visible( ! column.visible() );
            }
        }

    });
</script>
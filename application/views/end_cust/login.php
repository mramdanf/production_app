<!DOCTYPE html>
<html>
<head>
    <title>Heavenshard</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <link href="<?php echo base_url(); ?>/assets/css/jquery.dataTables.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>/assets/css/bootstrap.css" rel="stylesheet" />

    <script src="<?php echo base_url(); ?>/assets/js/jquery-1.12.3.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.js"></script>
</head>

<body>
    <div class="container">    
        <div id="loginbox" style="margin-top:100px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> 
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">Sign In Kasir dan Call Center</div>
                </div>     

                <div class="panel-body" >
                    <?php $alert_msg = $this->session->flashdata('alert_msg'); ?>
                    <?php if ($alert_msg != null) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        &times;</button>
                        <?php echo $alert_msg; ?>
                    </div>
                    <?php } ?>
                    <form action="<?php echo base_url(); ?>end_cust/sign_in/check_user" method="post" id="form_user">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                            <input type="text" name="username" class="form-control" placeholder="Username" aria-describedby="basic-addon1" required>
                        </div>
                        <div class="input-group" style="margin-top: 15px;">
                            <span class="input-group-addon" id="basic-addon1">
                                <i class="glyphicon glyphicon-lock"></i>
                            </span>
                            <input type="password" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" required>
                        </div>
                        <div class="form-group pull-right" style="margin-top: 20px;">
                            <button type="submit" form="form_user" class="btn btn-primary" id="save_user">
                                <span class="glyphicon glyphicon-log-in"></span> Sign in
                            </button>
                            <a href="<?php echo base_url(); ?>" class="btn btn-success">
                                <span class="glyphicon glyphicon-home"></span> Home
                            </a>
                        </div>
                    </form>
                </div>                     
            </div>  
        </div>
    </div>
</body>
</html>
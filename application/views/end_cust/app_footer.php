	<style type="text/css">

        footer {
            color: #666;
            background-color: #f5f5f5;
            padding: 17px 0 18px 0;
        }
    </style>
    
    <footer class="footer">
      <div class="container">
        <p class="text-muted">Copyright © 2016 Heavenshard. All rights reserved.</p>
      </div>
    </footer>

	
	<script src="<?php echo base_url(); ?>/assets/js/bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?=site_url('assets/js/dataTables.checkboxes.js')?>"></script>
	<script src="<?php echo base_url(); ?>/assets/js/jquery-ui.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/jquery-ui-timepicker-addon.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/chosen.jquery.js"></script>

	<script type="text/javascript">
    $(document).ready(function () {
        var get_url = window.location;
        var base_url = get_url .protocol + "//" + get_url.host + "/" + get_url.pathname.split('/')[1];

        $('#users_table').DataTable();


        //helper function
        function redirect_page(){
            window.location.replace(base_url);
        }

    });
</script>

</body>
</html>
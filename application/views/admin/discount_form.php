<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			Edit Discount
		</div>
		<div class="panel-body">
			<div class="row">
				<form action="<?php echo base_url(); ?>admin/discount/modify" method="post" class="form-inline">
					<div class="col-md-12">
						<div class="form-group">
							<label>Jumlah</label>
							<input type="text" name="jumlah" class="form-control" value="<?=$discount->jumlah?>">
							<input type="hidden" name="id" value="<?=$discount->id?>">
						</div>
						<div class="form-group">
							<select name="jenis" class="form-control">
							    <option></option>
								<option <?=($discount->jenis)?'':'selected'?> value="0">Persen</option>
								<option <?=($discount->jenis)?'selected':''?> value="1">Nominal</option>
							</select>
						</div>
						<div class="form-group">
							<input type="submit" value="Simpan" class="btn btn-primary">
							<a href="<?=site_url('admin/discount')?>" title="Batal Edit" class="btn btn-danger">Batal</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
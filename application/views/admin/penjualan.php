<div class="container" style="height: 100%;">
	<ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>admin/dashboard" style="text-decoration: none;">Admin</a></li>
      <li class="active">Penjualan</li>
    </ol>
	<div class="panel panel-default">
		<div class="panel-heading">
			Data Penjualan
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<a href="<?php echo base_url(); ?>admin/penjualan/report_penjualan" class="btn btn-success">
						<i class="fa fa-print fa-lg" aria-hidden="true"></i> Cetak Pdf
					</a>
				</div>
			</div>
			<div class="row" style="margin-top: 20px;">
				<div class="col-md-12">
					<table id="penjualan_table" width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Order No.</th>
								<th>Tgl Order</th>
								<th>Kode</th>
								<th>Nama</th>
								<th>Qty</th>
								<th>Sub Total (Rp)</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach ($penjualan as $row) { ?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $row->order_id; ?></td>
								<td><?php echo $row->insert_date; ?></td>
								<td><?php echo $row->bel_id; ?></td>
								<td><?php echo $row->prod_name; ?></td>
								<td><?php echo $row->detail_qty; ?></td>
								<td><?php echo number_format($row->detail_price); ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
				
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#penjualan_table').DataTable();
	});
</script>
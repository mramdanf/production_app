<!DOCTYPE html>
<html>
<head>
    <title>Report Penjualan</title>
    <style type="text/css">
         tbody tr td{
            text-align: center;
         }
         thead tr {
            background-color: #e7e7e7;
         }
    </style>
</head>
<body>
    <table width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Order No.</th>
                <th>Tgl Order</th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Qty</th>
                <th>Sub Total (Rp)</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach ($penjualan as $row) { ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $row->order_id; ?></td>
                <td><?php echo $row->insert_date; ?></td>
                <td><?php echo $row->bel_id; ?></td>
                <td><?php echo $row->prod_name; ?></td>
                <td><?php echo $row->detail_qty; ?></td>
                <td><?php echo number_format($row->detail_price); ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Heavenshard</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<link href="<?php echo base_url(); ?>/assets/css/jquery.dataTables.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/css/bootstrap.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">

	<script src="<?php echo base_url(); ?>/assets/js/jquery-1.12.3.min.js"></script>
	<style type="text/css">
		html {
		  position: relative;
		  min-height: 100%;
		}
		body {
		  /* Margin bottom by footer height */
		  margin-bottom: 60px;
		}
		.footer {
		  position: absolute;
		  bottom: 0;
		  width: 100%;
		  /* Set the fixed height of the footer here */
		  height: 60px;
		  background-color: #f5f5f5;
		}
	</style>
	
</head>

<body>
	<div class="navbar navbar-default">
		<div class="navbar-inner">
			<div class="container">
				<a class="navbar-brand" href="<?php echo base_url(); ?>admin/dashboard">Heavenshard</a>
				<ul class="nav navbar-nav">
		    		<li><a href="<?php echo base_url(); ?>admin/discount">Discount</a></li>
		    		<li><a href="<?php echo base_url(); ?>admin/penjualan">Penjualan</a></li>
		    		<li><a href="<?php echo base_url(); ?>admin/fitur">Fitur</a></li>
		      	</ul>
				<div class="navbar-right">
                    <a href="<?php echo base_url(); ?>admin/dashboard/sign_out" class="btn btn-danger navbar-btn">
                        <span class="glyphicon glyphicon-log-out"></span> Sign out
                    </a>
                </div> 
			</div>
		</div>
	</div>

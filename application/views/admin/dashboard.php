<div class="container" style="height: 100%;">
	<?php $alert_msg = $this->session->flashdata('alert_msg'); 
	      $alert_type = $this->session->flashdata('alert_type'); 
	?>
	<?php if ($alert_msg != null) { ?>
	<?php if ($alert_type == null) $alert_type = 'success'; ?>
	<div class="alert alert-<?php echo $alert_type; ?> alert-dissmisable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $alert_msg; ?>
	</div>
	<?php } ?>
	<ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>admin/dashboard" style="text-decoration: none;">Admin</a></li>
      <li class="active">Home</li>
    </ol>
	<div class="panel panel-default">
		<div class="panel-heading">
			Tambah User
		</div>
		<div class="panel-body">
			<div class="row">
				<form action="<?php echo base_url(); ?>admin/dashboard/add_user" method="post" id="form_user">
					<div class="col-md-4">
						<div class="form-group">
							<label>Nama Depan</label>
							<input type="text" name="first_name" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Nama Belakang</label>
							<input type="text" name="last_name" class="form-control" required>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Username</label>
							<input type="text" name="username" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Hak Akses</label>
							<select name="privilage" class="form-control" id="priv_user">
							    <option value="unselected">- Pilih hak akses -</option>
								<option value="End Customer">End customer (Kasir dan Call center)</option>
								<option value="Produksi">Produksi</option>
							</select>
						</div>
					</div>
					<div class="col-md-4" style="padding-top: 100px;">
						<div class="form-group">
							<button type="submit" form="form_user" class="btn btn-primary" id="save_user">
								<span class="glyphicon glyphicon-floppy-disk"></span> Simpan
							</button>
							<button type="reset" form="form_user" class="btn btn-danger">
								<span class="glyphicon glyphicon-repeat"></span> Reset
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			Data user (Default password adalah <strong>123</strong>)
		</div>
		<div class="panel-body">
			<table id="users_table" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Awal</th>
                        <th>Nama Akhir</th>
                        <th>Privilage</th>
                        <th>Username</th>
                        <th>Pilihan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0; foreach ($users as $row) {?>
                    <tr>
                        <td><?php echo(++$i) ?></td>
                        <td><?php echo($row->first_name); ?></td>
                        <td><?php echo($row->last_name); ?></td>
                        <td><?php echo($row->privilage); ?></td>
                        <td><?php echo($row->username); ?></td>
                        <td>
                            <a href="<?php echo base_url(); ?>admin/dashboard/edit_user/<?php echo $row->user_table_id; ?>" title="Edit data" class="btn btn-success btn_edit">
                            	<span class="glyphicon glyphicon-pencil" style="color: white;"></span>
                            </a>
                            <a href="#modal_confirm" class="btn btn-danger btn_delete" id="<?php echo $row->user_table_id; ?>_<?php echo $row->first_name; ?>" title="Hapus data" style="color: white;" role="button" data-toggle="modal">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                            <a href="<?php echo base_url(); ?>admin/dashboard/reset_pass/<?php echo $row->user_table_id; ?>" title="Reset password" class="btn btn-warning btn_edit">
                            	<span class="glyphicon glyphicon-repeat" style="color: white;"></span>
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_confirm" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-titel text-left">Konfirmasi</h4>
			</div>
			<div class="modal-body">
				<p class="text-left" id="confirm_msg"></p>
			</div>
			<div class="modal-footer">
				<a href="javascript:void(null);" class="btn btn-primary" id="do_delete">
					<span class="glyphicon glyphicon-trash"></span> Hapus
				</a>
				<a href="javascript:void(null);" class="btn btn-danger" data-dismiss="modal">
					<span class="glyphicon glyphicon-minus-sign"></span> Batal
				</a>
			</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	var jv_user_table_id;
	var jv_firts_name;

	$(document).ready(function(){
		var get_url = window.location;
        var base_url = get_url .protocol + "//" + get_url.host + "/" 
        + get_url.pathname.split('/')[1] + '/';

        $('#users_table').DataTable();

		$('.btn_delete').click(function(){
			var index_underscore = this.id.indexOf('_');
			jv_user_table_id = this.id.substring(0, index_underscore);
			jv_firts_name = this.id.substr(index_underscore+1);

			$('#confirm_msg').html('User <strong>' + jv_firts_name + '</strong> Akan dihapus?');

		});

		$('#do_delete').click(function(){
			$.post(base_url+'admin/dashboard/del_user', {user_table_id: jv_user_table_id, first_name: jv_firts_name}, 
				function(response){
					window.location.replace(base_url+'admin/dashboard');
				}

			);
		});

		$('#save_user').click(function(e){
			var jv_priv_user = $('#priv_user').val();
			if (jv_priv_user == 'unselected') {
				e.preventDefault();
				alert('Mohon untuk memilih hak akses dari user.');
			}
		});

		
	});
</script>
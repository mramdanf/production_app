<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			Edit User
		</div>
		<div class="panel-body">
			<div class="row">
			    <?php foreach ($user_details as $row) { ?>
				<form action="<?php echo base_url(); ?>admin/dashboard/do_edit" method="post" id="form_user">
					<div class="col-md-4">
						<div class="form-group">
						<label>Nama Depan</label>
							<input type="text" name="first_name" value="<?php echo $row->first_name; ?>" class="form-control" required>
							<input type="hidden" name="user_table_id" value="<?php echo $row->user_table_id; ?>">
						</div>
						<div class="form-group">
							<label>Nama Akhir</label>
							<input type="text" name="last_name" value="<?php echo $row->last_name; ?>" class="form-control" required>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Username</label>
							<input type="text" name="username" value="<?php echo $row->username; ?>" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Hak Akses</label>
							<select name="privilage" class="form-control" id="priv_user">
							    <option value="unselected">- Pilih hak akses -</option>
								<option value="End Customer" <?php if($row->privilage == 'End Customer') echo "selected"; ?>>End customer (Kasir dan Call center)</option>
								<option value="Produksi" <?php if($row->privilage == 'Produksi') echo "selected"; ?>>Produksi</option>
							</select>
						</div>
					</div>
					<div class="col-md-4" style="padding-top: 100px;">
						<div class="form-group">
							<button type="submit" form="form_user" class="btn btn-primary" id="update_user">
								<span class="glyphicon glyphicon-floppy-disk"></span> Update
							</button>
							<a href="<?php echo base_url(); ?>admin/dashboard" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span> Batal</a>
						</div>
					</div>
				</form>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
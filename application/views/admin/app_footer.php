	<style type="text/css">

        footer {
            color: #666;
            background-color: #f5f5f5;
            padding: 17px 0 18px 0;
        }
    </style>
    
    <footer class="footer">
      <div class="container">
        <p class="text-muted">Copyright © 2016 Heavenshard. All rights reserved.</p>
      </div>
    </footer>

	
	<script src="<?php echo base_url(); ?>/assets/js/bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/jquery-ui.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/jquery-ui-timepicker-addon.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/chosen.jquery.js"></script>

</body>
</html>
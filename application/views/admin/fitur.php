<div class="container" style="height: 100%;">
	<ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>admin/dashboard" style="text-decoration: none;">Admin</a></li>
      <li class="active">Fitur</li>
    </ol>
    <?php $alert = $this->session->flashdata('alert'); ?>
	<?php if ($alert != null) { ?>
	<div class="alert <?=$alert['type']?> alert-dismissable" role="alert">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
	    <?php echo $alert['msg']; ?>
	</div>
	<?php } ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			List Fitur
		</div>
		<div class="panel-body">
			<div class="row" style="margin-top: 20px;">
				<div class="col-md-12">
					<table id="fitur_table" width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama Fitur</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach ($fiturs->result() as $row) { ?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $row->name; ?></td>
								<td>
									<?php
									    $type = "label label-danger";
									    $word = "TIDAK AKTIF"; 
                                        if($row->is_active == 1){
                                        	$type = "label label-primary";
                                        	$word = "AKTIF";
                                        }
									?>
									<span class="<?=$type?>"><?=$word?></span>
								</td>
								<td>
									<?php
										$href_data=site_url('admin/fitur/activate'.'/'.$row->id); 
										$title_data="Aktifkan Fitur"; $data_class="btn-primary";
										$data_span="glyphicon glyphicon-ok";
										if($row->is_active == 1) {
											$href_data=site_url('admin/fitur/inactivate'.'/'.$row->id); 
											$title_data="Non Aktifkan Fitur"; $data_class="btn-danger";
											$data_span="glyphicon glyphicon-remove";
										}
									?>
									<a href="<?=$href_data?>" title="<?=$title_data?>" class="btn <?=$data_class?>"><span class="<?=$data_span?>"></span></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
				
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#fitur_table').DataTable();
	});
</script>
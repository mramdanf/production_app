<!DOCTYPE html>
<html>
<head>
	<title>Heavenshard</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<link href="<?php echo base_url(); ?>/assets/css/jquery.dataTables.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/css/bootstrap.css" rel="stylesheet" />

	<script src="<?php echo base_url(); ?>/assets/js/jquery-1.12.3.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/jquery.dataTables.js"></script>

	<style type="text/css">
		body {
		  padding-top: 40px;
		  padding-bottom: 40px;
		  background-color: #eee;
		}

		.form-signin {
		  max-width: 330px;
		  padding: 15px;
		  margin: 0 auto;
		}
		.form-signin .form-signin-heading,
		.form-signin .checkbox {
		  margin-bottom: 10px;
		}
		.form-signin .checkbox {
		  font-weight: normal;
		}
		.form-signin .form-control {
		  position: relative;
		  height: auto;
		  -webkit-box-sizing: border-box;
		     -moz-box-sizing: border-box;
		          box-sizing: border-box;
		  padding: 10px;
		  font-size: 16px;
		}
		.form-signin .form-control:focus {
		  z-index: 2;
		}
		.form-signin input[type="email"] {
		  margin-bottom: -1px;
		  border-bottom-right-radius: 0;
		  border-bottom-left-radius: 0;
		}
		.form-signin input[type="password"] {
		  margin-bottom: 10px;
		  border-top-left-radius: 0;
		  border-top-right-radius: 0;
		}
	</style>
	
</head>

<body>
	<div class="container">
		<form class="form-signin" method="post" action="<?php echo base_url(); ?>/admin/sign_in/check_admin">
			<?php $alert_msg = $this->session->flashdata('alert_msg'); ?>
			<?php if($alert_msg != null){ ?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" aria-hidden="true" data-dismiss="alert">&times;</button>
				<?php echo $alert_msg; ?>
			</div>
			<?php } ?>
	        <h2 class="form-signin-heading">Heavenshard admin</h2>
	        <label for="inputEmail" class="sr-only">Email address</label>
	        <input type="text" name="username" class="form-control" placeholder="Username" required autofocus>
	        
	        <label for="inputPassword" class="sr-only">Password</label>
	        <input type="password" name="password" class="form-control" placeholder="Password" required>
	        
	        <div class="checkbox">
	        	<label>
	            	<input type="checkbox" value="remember-me"> Remember me
	          	</label>
	        </div>

	        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
	        <a href="<?php echo base_url(); ?>" class="btn btn-lg btn-success btn-block">Home</a>
      </form>
	</div>
</body>
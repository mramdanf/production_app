<?php error_reporting(0); ?>
<div class="container" style="height: 100%;">
	<div class="row">
		<?php $alert = $this->session->flashdata('alert'); ?>
		<?php if ($alert != null) { ?>
		<div class="alert <?=$alert['type']?> alert-dismissable" role="alert">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $alert['msg']; ?>
		</div>
		<?php } ?>
	</div>
	
	<div class="panel panel-danger">
		<div class="panel-heading">
			Form Expired
		</div>
		<div class="panel-body">
			<form action="<?php echo base_url(); ?>production/expired/modify" method="post" class="form-horizontal">
				<div class="row">

					<div class="form-group">
						<label class="control-label col-sm-2">Kode Produk</label>
						<div class="col-md-6">
							<input type="number" class="form-control" name="belong_to_id" value="<?=$expired->belong_to_id?>" readonly>
							<input type="hidden" name="id" value="<?=$expired->id?>">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Nama Produk</label>
						<div class="col-md-6">
							<select data-placeholder="- Nama Produk -" name="prod_name" class="form-control">
								<option value="unselected"></option>
								<?php $is_selected=""; 
									foreach ($prod_name as $row) { 
										if($row->bel_id == $expired->belong_to_id) 
											$is_selected="selected";
										else $is_selected="";
								?>
								<option <?=$is_selected?> 
									value="<?php echo $row->bel_id; ?>">
									<?php echo $row->prod_name; ?> - <?php echo $row->name; ?>
								</option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Tanggal Expired</label>
						<div class="col-md-6">
							<input type="text" name="exp_date" class="form-control" value="<?=$expired->exp_date?>">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Jumlah Expired</label>
						<div class="col-md-6">
							<input type="number" name="expired_product" class="form-control" value="<?=$expired->expired_product?>">
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-primary">
								<span class="glyphicon glyphicon-floppy-disk"></span> Simpan
							</button>
							<a href="<?php echo base_url(); ?>production/expired" class="btn btn-danger">
								<span class="glyphicon glyphicon-minus-sign"></span> Batal
							</a>
						</div>
					</div>
					
				</div>
			</form>
		</div>
	</div>
	
</div>
<script>
	$(document).ready(function() {

		$("select[name='prod_name']").chosen({
		    search_contains: true
		}).change(function(){
			$("input[name='belong_to_id']").val($("select[name='prod_name'] option:selected").val())
		});

		$("input[name='exp_date']").datetimepicker({
		    dateFormat: 'dd M yy',
		    showTimepicker: false
		});
	});
</script>
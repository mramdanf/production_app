<div class="container" >
	<div class="panel panel-danger">
		<div class="panel-heading">
			Image Detail
		</div>
		<div class="panel-body">
			<input type="hidden" id="prod_id" value="<?php echo $prod_id; ?>">
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							Current Product Image
						</div>
						<div class="panel-body text-center">
							<img src="<?php echo $img_prod; ?>">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							Current Product Detail Image
						</div>
						<div class="panel-body text-center">
							<img src="<?php echo $img_prod_detail; ?>">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-danger">
				<div class="panel-heading">
					Product Image - 268x249
				</div>
				<div class="panel-body">
					<form enctype="multipart/form-data">
		                <div class="form-group">
		                    <input id="input-704" name="kartik-input-704[]" type="file" class="file-loading">
		                </div>
		            </form>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-danger">
				<div class="panel-heading">
					Product Detail Image - 266x381
				</div>
				<div class="panel-body">
					<form enctype="multipart/form-data">
		                <div class="form-group">
		                    <input id="img_prod_detail" name="img_prod_detail[]" type="file" class="file-loading">
		                </div>
		            </form>
				</div>
			</div>
		</div>
	</div>
	
</div>
<script>
	$(document).ready(function(){
		var get_url = window.location;
        var base_url = get_url .protocol + "//" + get_url.host + "/" 
        + get_url.pathname.split('/')[1] + "/";

		$("#input-704").fileinput({
		    uploadUrl: base_url+"production/products/prod_img", // server upload action
		    uploadAsync: false,
		    minFileCount: 1,
		    maxFileCount: 5,
		    showRemove: false,
		    uploadExtraData: {
		        prod_id: $('#prod_id').val()
		    }
		});

		$("#img_prod_detail").fileinput({
		    uploadUrl: base_url+"production/products/prod_detail_img", // server upload action
		    uploadAsync: false,
		    minFileCount: 1,
		    maxFileCount: 5,
		    showRemove: false,
		    uploadExtraData: {
		        prod_id: $('#prod_id').val()
		    }
		});
	})
</script>
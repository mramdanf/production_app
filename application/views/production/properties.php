<style>
	.panel-body a.btn-primary {
		margin-bottom: 30px;
	}
</style>
<div class="container">
	<ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>production/dashboard" style="text-decoration: none;">Produksi</a></li>
      <li class="active">Kategori dan Properti</li>
    </ol>
	<div class="panel panel-danger">
		<div class="panel-heading">
			Kategori produk
		</div>
		<div class="panel-body">

			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label>Cari Kategori</label>
						<select name="categories" class="form-control" placeholder="Cari Kategori">
							<option value="unselected"></option>
							<?php 
							foreach ($categories as $categorie) {
								echo "<option value='{$categorie->cat_name}'>{$categorie->cat_name}</option>";
							} ?>
						</select>
					</div>
				</div>
			</div>

			<div class="table-responsive">
				<table id="properties_table" width="100%" class="table">
					<thead>
						<tr>
							<th>No</th>
							<th>Kategori</th>
							<th>Properti</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($properties->result() as $row) { ?>
						<tr>
							<td><?php echo $no++; ?></td>
							<td><?php echo $row->cat_name; ?></td>
							<td><?php echo $row->name; ?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
				
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		var propTable = $('#properties_table').DataTable();

		$("select[name='categories']").change(function() {
			if($(this).val() == "unselected") {
				propTable
					 .search( '' )
					 .columns().search( '' )
					 .draw();
			} else {
				propTable
			        .columns( 1 )
			        .search("^" + $(this).val() + "$", true, false, true)
			        .draw();
			} 
			
		});

	});
</script>
<style>
	.panel-body a.btn-primary {
		margin-bottom: 30px;
	}
</style>
<div class="container">
	<div class="row">
		<ol class="breadcrumb">
	      <li><a href="<?php echo base_url(); ?>production/dashboard" style="text-decoration: none;">Produksi</a></li>
	      <li class="active">Data Produk</li>
	    </ol>
	</div>
	<div class="row">
		<?php $alert = $this->session->flashdata('alert'); ?>
		<?php if ($alert != null) { ?>
		<div class="alert <?=$alert['type']?> alert-dismissable" role="alert">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $alert['msg']; ?>
		</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="panel panel-danger">
			<div class="panel-heading">
				Produk
			</div>
			<div class="panel-body">
				<a href="<?=site_url('production/products/form')?>" class="btn btn-primary">Tambah Produk</a>
				<div class="table-responsive">
					<table id="products_table" width="100%" class="table">
						<thead>
							<tr>
								<th>No</th>
								<th>Kode</th>
								<th>Nama</th>
								<th>Kategori</th>
								<th>Properti</th>
								<th>Harga</th>
								<th>Stok</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; foreach ($products as $row) { ?>
							<tr>
								<td>
									<?php echo $no++; ?>
									<input type="hidden" name="prod_id" value="<?=$row->prod_id?>">
								</td>
								<td><?php echo $row->bel_id; ?></td>
								<td><?php echo $row->prod_name; ?></td>
								<td><?php echo $row->cat_name; ?></td>
								<td><?php echo $row->p_name; ?></td>
								<td><?php echo number_format($row->harga); ?></td>
								<td><?php echo $row->stocks; ?></td>
								<td>
									<a title="Hapus data" class="btn btn-danger btn_delete" style="color: white;">
										<span class="glyphicon glyphicon-trash"></span>
									</a>
									<a href="<?php echo base_url(); ?>production/products/form/<?php echo $row->bel_id; ?>" title="Edit data" class="btn btn-success" style="color: white;">
										<span class="glyphicon glyphicon-pencil"></span>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<th></th>
								<th><input type="text" name="search_code" placeholder="Cari Kode" class="form-control" style="max-width: 100px;"></th>
								<th><input type="text" name="search_name" placeholder="Cari Nama" class="form-control" style="max-width: 200px;"></th>
								<th>
									<select name="categories" class="form-control" style="max-width: 230px;">
										<option></option>
										<?php 
										foreach ($categories as $categorie) {
											echo "<option value='{$categorie->cat_name}'>{$categorie->cat_name}</option>";
										} ?>
									</select>
								</th>
								<th colspan="4"><button type="button" id="btn_refresh" class="btn btn-primary">Refresh</button></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="modal fade" id="confrim_modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Konfirmasi</h4>
			</div>
			<div class="modal-body">
				<p id="confirm_msg">Anda yakin ingin menghapus data yang dipilih?</p>
			</div>
			<div class="modal-footer">
				<a href="javascript:void(null);" class="btn btn-primary" id="do_delete">
					<span class="glyphicon glyphicon-trash"></span> Hapus
				</a>
				<a href="javascript:void(null);" class="btn btn-danger" data-dismiss="modal">
					<span class="glyphicon glyphicon-minus-sign"></span> Batal
				</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var jv_prod_id;
	var jv_prod_name;
	var jv_bel_id;

	$(document).ready(function(){
  		var base_url = "<?=site_url()?>";
		var prod_table = $('#products_table').DataTable();
		var select_categories = $("select[name='categories']");
		var $elem = $('body');


		prod_table.on("click", ".btn_delete", function(e) {

			if(!confirm("Anda yakin ?")){
				return;
			} 

		    var parentTr = $(this).closest('tr');
		    var prodId = parentTr.find("input[name='prod_id']").val();
		    var prodName = $('td:eq(2)', parentTr).text();
		    var belId = $('td:eq(1)', parentTr).text();

		    $.post(base_url + 'production/products/del_product', 
				{prod_id: prodId, prod_name: prodName, bel_id: belId},
				function(response){
					window.location.replace(base_url+'production/products');
				}
			);
		});

		$('#cat_name').change(function(){
			var jv_val = $('#cat_name').val();
			var gap_userscore = jv_val.indexOf('_');
			var jv_cat_id = jv_val.substr(gap_userscore+1);

			$('#cat_id').val(jv_cat_id);
		});

		$('#harga').keyup(function(){
			var n = parseInt($(this).val().replace(/\D/g,''),10);
			if (n>0) {
				$(this).val(n.toLocaleString());
			}
		});

		$('#print_barcode').click(function(){
			var url = $(this).attr('href');
			var querystring = '';
			if($('.checkbarcode:checked').length > 0){
				querystring = '?prod_id=';
				$('.checkbarcode:checked').each(function(){
					querystring+=''+$(this).data('id')+',';
				})
				querystring = querystring.substring(0, querystring.length-1);
			}
			window.open(url+querystring,'_blank');
			return false; 
			
		});

		// untuk validasi input, yang mana hanya angka yang di izinkan untuk diinput
		$("#harga").keydown(function (e) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	             // Allow: Ctrl+A, Command+A
	            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
	             // Allow: home, end, left, right, down, up
	            (e.keyCode >= 35 && e.keyCode <= 40)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        // Ensure that it is a number and stop the keypress
	        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
	    });

		select_categories.change(function() {
			prod_table
		        .columns( 3 )
		        .search("^" + $(this).val() + "$", true, false, true)
		        .draw();
		});

		prod_table.columns().every( function () {
	        var col = this;
            $('input', col.footer()).on('keyup change', function(){
                if (col.search() !== this.value){
                    col.search("^" + this.value + "$", true, false, true).draw();
                }
            });
	    });

	    $("#btn_refresh").click(function() {
	    	prod_table
				 .search( '' )
				 .columns().search( '' )
				 .draw();
			$("tfoot th input").each(function() {
				$(this).val('');
			});
			select_categories.val('');
	    });

	});
</script>
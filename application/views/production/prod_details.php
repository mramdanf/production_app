<div class="container" style="height: 100%;">
	<div class="row">
		<?php $alert_msg = $this->session->flashdata('alert_msg'); ?>
		<?php if ($alert_msg != null) { ?>
		<div class="alert alert-success alert-dissmisable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo $alert_msg; ?>
		</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="panel panel-danger">
			<div class="panel-heading">
				Edit Produk - <?php echo $bel_id; ?>
			</div>
			<div class="panel-body">
				<?php foreach ($product_details as $row) { ?>
				<form action="<?php echo base_url(); ?>production/products/do_edit" method="post" id="form_edit_prod">
					<div class="col-md-4">
						<div class="form-group">
							<label>Nama Produk</label>
							<input type="text" name="prod_name" class="form-control" placeholder="Nama Produk" value="<?php echo $row->prod_name; ?>" required></input>
							<input type="hidden" id="cat_id" name="cat_id" value="<?php echo $row->categori_id; ?>" />
							<input type="hidden" id="product_id" name="product_id" value="<?php echo $row->product_id; ?>" />
							<input type="hidden" id="bel_id" name="bel_id" value="<?php echo $row->bel_id; ?>" />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Kategori</label>
							<select name="cat_name" id="cat_name" class="form-control">
								<option value="unselected">- Pilih kategori -</option>
								<?php foreach ($categories as $categori) { ?>
								<?php $is_selected = ''; if ($categori->cat_name == $row->cat_name ) $is_selected = 'selected'; else $is_selected = '' ?>;
								<option value="<?php echo $categori->cat_name;?>_<?php echo $categori->cat_id; ?>" <?php echo $is_selected; ?>><?php echo $categori->cat_name; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<label>Harga</label>
						<div class="form-group">
							<input type="text" name="harga" class="form-control" placeholder="Harga (Rp)" value="<?php echo $row->harga; ?>" id="harga" required></input>
						</div>
					</div>
					<div class="col-md-2">
						<label>Stok</label>
						<div class="form-group">
							<input type="text" name="stocks" value="<?php echo $row->stocks; ?>" class="form-control" placeholder="Stok" value="0"></input>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<button type="submit" class="btn btn-primary">
								<span class="glyphicon glyphicon-floppy-disk"></span> Update
							</button>
							<a href="<?php echo base_url(); ?>production/products" class="btn btn-danger">
								<span class="glyphicon glyphicon-minus-sign"></span> Batal
							</a>
						</div>
					</div>
					
				</form>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function(){

		$('#cat_name').change(function(){
			var jv_val = $('#cat_name').val();
			var index_underscore = jv_val.indexOf('_');
			var jv_cat_id = jv_val.substr(index_underscore+1);

			$('#cat_id').val(jv_cat_id);

		});

		$('#form_edit_prod').submit(function(e){
			var jv_cat_name = $('#cat_name').val();

			if (jv_cat_name == 'unselected') {
				alert('Mohon untuk memilih kategori produk.');
				e.preventDefault();
			}
		});

		// ketika key up akan langsung di format currency
		// ex: 10000 -> 100,000
		$('#harga').keyup(function(){
			var n = parseInt($(this).val().replace(/\D/g,''),10);
			if (n>0) {
				$(this).val(n.toLocaleString());
			}
		});

		// ketika page di load filed harga langsung di format untuk currencynya
		// ex: 10000 -> 100,000
		var n = parseInt($('#harga').val().replace(/\D/g,''),10);
		$('#harga').val(n.toLocaleString());



	});
</script>
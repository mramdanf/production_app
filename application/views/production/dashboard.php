<div class="container">
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>production/dashboard" style="text-decoration: none;">Produksi</a></li>
      <li class="active">Home</li>
    </ol>
    <div class="panel panel-danger">
        <div class="panel-heading">
            Order Produksi
        </div>
        <div class="panel-body">
            <table id="orders_table" class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Order No.</th>
                        <th>Pembeli</th>
                        <th>Tgl Order</th>
                        <th>Tipe Order</th>
                        <th>Tgl Delivery</th>
                        <th>Pilihan</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="panel panel-danger">
        <div class="panel-heading">
            Order Detail
        </div>
        <div class="panel-body">
            <form action="<?php echo base_url(); ?>production/dashboard/update_pending" method="post" id="form_update_stock">
                <div class="row">
                    <div class="col-md-3">
                        <label>Order No.</label>
                        <input type="text" name="order_id" value="<?php echo $order_id; ?>" class="form-control" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Kode Produk</label>
                        <input type="text" class="form-control" id="bel_id" readonly>
                    </div>
                    <div class="col-md-4">
                        <label>Nama Produk</label>
                        <input type="text" class="form-control" id="prod_name" readonly>
                    </div>
                    <div class="col-md-4">
                        <label>Qty</label>
                        <input type="text" class="form-control" id="detail_qty" readonly>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 30px;">
                    <div class="col-md-4">
                        <label>On Progress</label>
                        <input type="text" class="form-control" id="pending" readonly>
                        <input type="hidden" class="form-control" name="order_detail_id" id="order_detail_id">
                        <input type="hidden" class="form-control" name="prod_id" id="prod_id">
                    </div>
                    <div class="col-md-4">
                        <label>Penambahan</label>
                        <input type="text" name="value_added" id="value_added" class="form-control" readonly>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" style="margin-top: 25px;">
                            <button type="submit" class="btn btn-primary" form="form_update_stock" id="btn_update">
                                <span class="glyphicon glyphicon-ok"></span> Simpan
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <table id="order_details" width="100%" class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Qty</th>
                        <th>On Progress</th>
                        <th>Wording</th>
                        <th>Decor</th>
                        <th>Pilihan</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
</div>
<script>
    var orderDetailTable;
    var inputOrderId;

    $(document).ready(function() {
        var base_url = '<?=site_url('')?>';
        var curr_length=0;
        inputOrderId = $("input[name='order_id']");


        /* ================ ON PROGRESS ORDERS ================ */
        var ordersTable = $('#orders_table').DataTable( {
            "sAjaxSource": base_url+'production/dashboard/json_orders',
            "sAjaxDataProp": "data",
            "initComplete": function( settings ) {
                var api = new $.fn.dataTable.Api( settings );
                curr_length = api.rows().count();
            }

        });

        // refreshing order table
        setInterval( function () {
            ordersTable.ajax.reload(null, false);
        }, 10000 );

        // interval for notification
        setInterval( function () {
            if(curr_length != ordersTable.rows().count()) {
                curr_length = ordersTable.rows().count();
                notifyMe("<?=site_url('production/dashboard/get_order_details/')?>"
                    + "/" + ordersTable.row(curr_length-1).data()[1].split('/').join('_'));
            }
        }, 1050);



        /* ================ ORDER DETAILS ================ */
        orderDetailTable = $('#order_details').DataTable({
            ajax: {
                type: 'POST',
                url: base_url+'production/dashboard/json_order_details',
                data: function(d) {
                    d.order_id = inputOrderId.val()
                }
                
            },
            scrollX: true
        });


        ordersTable.on("click", ".btn_view", function() { 

            var parentTr = $(this).closest('tr');

            inputOrderId.val($('td:eq(1)', parentTr).text());

            orderDetailTable.ajax.reload(null, false);
            
            setInterval( function () {
                orderDetailTable.ajax.reload(null, false);
            }, 10000);

        });

        orderDetailTable.on("click", ".btn_edit", function() { 
            var jv_order_detail_id = this.id;
            $("#value_added").prop('readonly', false);
            
            $.ajax({
                type: 'POST',
                url: base_url+'production/dashboard/order_detail_id',
                dataType: 'json',
                data: {order_detail_id: jv_order_detail_id},
                success: function(response){
                    $.map(response, function(v, i) {
                        $('#bel_id').val(v.bel_id);
                        $('#prod_name').val(v.prod_name);
                        $('#detail_qty').val(v.detail_qty);
                        $('#pending').val(v.pending);
                        $('#order_detail_id').val(v.order_detail_id);
                        $('#prod_id').val(v.prod_id);

                        // di opera .val tidak bekerja
                        $('#order_detail_id').attr("value", v.order_detail_id);
                        $('#prod_id').attr("value", v.prod_id);

                    });
                }
            });

        });

        $('#btn_update').click(function(e) {

            if ($('#value_added').val() == '' || $('#value_added').val() == 0) {

                e.preventDefault();
                alert('Mohon isi filed penambahan terlebih dahulu, field penambahan tidak boleh kosong atau nol.');

            } else if($('#value_added').val() > $('#pending').val()) {

                e.preventDefault();
                alert('Input gagal, penambahan melebihi pending.');

            }
            
        });

        /*================= ON LOAD CONFIG ================= */

        // this config fired when user click notification, so there are
        // two ways that would refresh orderDetailTable: on btn_view click
        // and on user click notification
        if(inputOrderId != "" && inputOrderId != null) {

            orderDetailTable.ajax.reload(null, false);
            setInterval( function () {
                orderDetailTable.ajax.reload(null, false);
            }, 10000);
        }

    });
</script>
<script>
    /* fitur notifikasi */

    // request permission on page load
    document.addEventListener('DOMContentLoaded', function () {
      if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.'); 
        return;
      }
      if (Notification.permission !== "granted")
        Notification.requestPermission();
    });

    function notifyMe(target_url) {
        if (Notification.permission !== "granted")
            Notification.requestPermission();
        else {
            var notification = new Notification('New Order', {
              icon: '<?=site_url('assets/images/logo_pos.png')?>',
              requireInteraction: true,
              body: "Klik notifaksi ini untuk melihat order baru",

            });

            var audio = new Audio('<?=site_url('assets/announcement.mp3')?>');
            
            notification.onshow = function () {
                audio.play();
            }

            notification.onclick = function () {
                notification.close();
                window.focus();

                window.open(target_url, "_self");
            };
        }
    }
</script>

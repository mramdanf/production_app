<?php error_reporting(0) ?>
<div class="container">
	<?php $alert = $this->session->flashdata('alert'); ?>
	<?php if ($alert != null) { ?>
	<div class="alert <?=$alert['type']?> alert-dismissable" role="alert">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $alert['msg']; ?>
	</div>
	<?php } ?>
	<div class="panel panel-danger">
		<div class="panel-heading">
			Form Kategori
		</div>
		<div class="panel-body">
			<form action="<?php echo base_url(); ?>production/categories/modify" method="post" class="form-horizontal">
				<div class="row form-container">
					<div class="form-group">
						<label class="control-label col-sm-2">Nama Kategori:</label>
					    <div class="col-sm-6">
					    	<input type="text" name="cat_name" class="form-control" value="<?=($category->cat_name?$category->cat_name:'')?>" required>
							<input type="hidden" name="cat_id" value="<?=($category->cat_id?$category->cat_id:'')?>" class="form-control">
					    </div>
					</div>

					<?php 
						if($category->cat_id)
						{
							foreach ($properties as $propertie) 
							{
								echo '<div class="form-group"> 
											<label class="control-label col-sm-2" for="email">Properti:</label> 
											<div class="col-sm-6"> 
												<input type="text" name="propertie[]" class="form-control" value='.$propertie->name.'> 
											</div> 
											<button type="button" class="btn btn-danger btn_del_prop"><span class="glyphicon glyphicon-trash"></span></button> 
										</div>';
							}
						}
					?>


					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-primary">
								<span class="glyphicon glyphicon-floppy-disk"></span> Simpan
							</button>
							<button type="button" class="btn btn-success" id="btn_add_properti">
								<span class="glyphicon glyphicon-plus"></span> Tambah Properti
							</button>
							<a href="<?php echo base_url(); ?>production/categories" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span> Batal</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	jQuery(document).ready(function($) {
		$("#btn_add_properti").click(function() {
			var htmlString = '<div class="form-group"> <label class="control-label col-sm-2" for="email">Properti:</label> <div class="col-sm-6"> <input type="text" name="propertie[]" class="form-control"> </div> <button type="button" class="btn btn-danger btn_del_prop"><span class="glyphicon glyphicon-trash"></span></button> </div>';

			$(htmlString).insertBefore('.form-container .form-group:last-child');
		});

		$("body").on('click', '.btn_del_prop', function() {
			if(confirm("Anda yakin?")){
				var btn_parent = $(this).parent('.form-group');
				btn_parent.remove();
			}
			
		});
	});
</script>
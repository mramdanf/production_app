<?php error_reporting(0) ?>
<div class="container" style="height: 100%;">
	<div class="row">
		<?php $alert = $this->session->flashdata('alert'); ?>
		<?php if ($alert != null) { ?>
		<div class="alert <?=$alert['type']?> alert-dismissable" role="alert">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $alert['msg']; ?>
		</div>
		<?php } ?>
	</div>
	
	<div class="panel panel-danger">
		<div class="panel-heading">
			Form Produk
		</div>
		<div class="panel-body">
			<form action="<?php echo base_url(); ?>production/products/modify" method="post" id="form_edit_prod" class="form-horizontal">
				<div class="row">

					<div class="form-group">
						<label class="control-label col-sm-2">Kode Produk</label>
						<div class="col-md-6">
							<input type="number" class="form-control" name="bel_id" value="<?=$product->bel_id?>">
							<input type="hidden" class="form-control" name="last_bel_id" value="<?=$product->bel_id?>">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Nama Produk</label>
						<div class="col-md-6">
							<input type="text" name="prod_name" class="form-control" value="<?=$product->prod_name?>">
						</div>
						<input type="hidden" name="product_id" value="<?=$product->product_id?>">
						<input type="hidden" name="id" value="<?=$product->id?>">
					</div>
				
					<div class="form-group">
						<label class="control-label col-sm-2">Kategori</label>
						<div class="col-md-6">
							<select name="cat_name" id="cat_name" class="form-control">
								<option value="unselected">- Pilih kategori -</option>
								<?php foreach ($categories as $categori) { ?>
								<?php $is_selected = ''; if ($categori->cat_name == $product->cat_name ) $is_selected = 'selected'; else $is_selected = '' ?>;
								<option value="<?php echo $categori->cat_name;?>_<?php echo $categori->cat_id; ?>" <?php echo $is_selected; ?>><?php echo $categori->cat_name; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Properti Kategori</label>
						<div class="col-md-6">
							<select name="prop_name" id="cat_name" class="form-control" disabled>
								<option value="unselected">- Pilih properti -</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2">Harga</label>
						<div class="col-md-6">
							<input type="text" name="harga" class="form-control" id="harga" value="<?=$product->harga?>">
						</div>
					</div>
				
			
					<div class="form-group">
						<label class="control-label col-sm-2">Stok</label>
						<div class="col-md-6">
							<input type="text" name="stocks" class="form-control" value="<?=$product->stocks?>">
						</div>
					</div>

					<!-- <div class="form-group">
						<label class="control-label col-sm-2">Diskon (dalam persen)</label>
						<div class="col-md-6">
							<input type="text" name="discount" class="form-control" value="<?=$product->discount?>">
						</div>
					</div> -->
				
				
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-primary">
								<span class="glyphicon glyphicon-floppy-disk"></span> Simpan
							</button>
							<a href="<?php echo base_url(); ?>production/products" class="btn btn-danger">
								<span class="glyphicon glyphicon-minus-sign"></span> Batal
							</a>
						</div>
					</div>
					
				</div>
			</form>
		</div>
	</div>
	
</div>
<script>
	jQuery(document).ready(function() {

		var select_prop_name = $("select[name='prop_name']");
		var selected_cat = "<?=$product->categori_id?>";
		var selected_prop = "<?=$product->properties_id?>";

		$("input[name='bel_id']").focus();

		if(selected_cat != '' && selected_cat != null) {
			select_prop_name.prop({ disabled: false });
			get_properties_cat(selected_cat);
		}

		$("select[name='cat_name']").change(function() {
			if($(this).val() != 'unselected'){
				select_prop_name.prop({ disabled: false });

				// .replace(/[^\d.]/g, '') menghapus selain angka di dalam string
				var js_cat_id = $(this).val().replace(/[^\d.]/g, '');

				get_properties_cat(js_cat_id);
			} else {
				select_prop_name.prop({ disabled: true });
			}
			
		});
		

		function get_properties_cat(id_cat) {
			var url_gpc = "<?=site_url('production/products/get_properties_cat')?>";
			$.ajax({
				url: url_gpc,
				type: 'POST',
				dataType: 'json',
				data: {cat_id: id_cat},
				success: function (json) {
					
		        	select_prop_name.empty();
		        	select_prop_name.append('<option value="unselected">- Pilih properti -</option>');

			        $.each(json.data, function(idx, propertie){

				    	select_prop_name.append('<option value=' + propertie.id + '>' + propertie.name + '</option>');

				   	});
			    }
			})
			.done(function() {
				// jika mode edit set value propertiesnya
				if(selected_prop != '' && selected_prop != null) {
					select_prop_name.val(selected_prop);
				}
			});
		}
	});
</script>

<script>

	$(document).ready(function(){

		$('#cat_name').change(function(){
			var jv_val = $('#cat_name').val();
			var index_underscore = jv_val.indexOf('_');
			var jv_cat_id = jv_val.substr(index_underscore+1);

			$('#cat_id').val(jv_cat_id);

		});

		$('#form_edit_prod').submit(function(e){
			var jv_cat_name = $('#cat_name').val();

			if (jv_cat_name == 'unselected') {
				alert('Mohon untuk memilih kategori produk.');
				e.preventDefault();
			}
		});

		// ketika key up akan langsung di format currency
		// ex: 10000 -> 100,000
		$('#harga').keyup(function(){
			var n = parseInt($(this).val().replace(/\D/g,''),10);
			if (n>0) {
				$(this).val(n.toLocaleString());
			}
		});

		// untuk validasi input, yang mana hanya angka yang di izinkan untuk diinput
		$("#harga").keydown(function (e) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	             // Allow: Ctrl+A, Command+A
	            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
	             // Allow: home, end, left, right, down, up
	            (e.keyCode >= 35 && e.keyCode <= 40)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        // Ensure that it is a number and stop the keypress
	        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
	    });

		/*
		harga akan di format ketika mode edit
		ex: 10000 -> 100,000
		*/
		if(window.location.href.length > 56) {
			var n = parseInt($('#harga').val().replace(/\D/g,''),10);
			$('#harga').val(n.toLocaleString());

		}


			
	});
</script>
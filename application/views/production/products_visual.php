<style>
	.form-inline {
		margin-bottom: 30px;
	}
</style>
<div class="container">
	<div class="row">
		<ol class="breadcrumb">
	      <li><a href="<?=site_url('production/dashboard')?>" style="text-decoration: none;">Produksi</a></li>
	      <li class="active">Products Visual</li>
	    </ol>
	</div>
	<div class="row">
		<form action="<?=site_url('production/products_visual/index')?>" class="form-inline" method="get">
			<div class="form-group">
				<label>Kategori</label>
				<select name="categorie_id" class="form-control">
					<option></option>
					<?php $sel=""; foreach ($categories as $categorie) { 
						$sel=($categorie->cat_id==$categorie_id)?"selected":"";
					?>
					<option value="<?=$categorie->cat_id?>" <?=$sel?>><?=$categorie->cat_name?></option>
					<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<input type="submit" value="Cari" class="btn btn-success">
			</div>
		</form>
		<?php $prop_len = count($properties); ?>
		<div class="table-responsive">
			<table id="tbl-products" class="table">
				<thead>
					<tr>
						<th>Item</th>
						<th>Class</th>
						<?php foreach ($properties as $propertie) { ?>
						<th><?=$propertie->name?></th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<?php foreach($products as $row) 
					{
						echo "<tr>";
						echo "<td>{$row[0]}</td>";
						echo "<td>{$row[1]}</td>";

						// this will print <td> as long as $prod_len
						for ($i=1; $i <= $prop_len; $i++) 
						{ 	
							// search if $row[2] contain specific type
							// that hold by $i, I do this to ensure
							// that all off <td> is filled, if no data found than fill with &nbsp.
							$found = 0; $data;
							foreach ($row[2] as $key => $value) 
							{
								if ($row[2][$key]["type"] == $i) 
								{
									$found = 1;
									$data = $value["bel_id"];
									break;
								}																
							}
							echo ($found) ? "<td>{$data}</td>" : "<td>&nbsp;</td>";
						}
						echo "</tr>";
					} ?>
				</tbody>
			</table>
		</div>	
	</div>
</div>
<script>
	jQuery(document).ready(function($) {
		$('#tbl-products').DataTable( {
	        "order": [[ 1, "asc" ]]
	    } );
	});
</script>
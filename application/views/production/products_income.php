<style>
	.panel-body a.btn-primary {
		margin-bottom: 30px;
	}
	#form-filter {
		margin-bottom: 20px;
	}

	#text-filter {
		font-size: 15pt;
		margin-bottom: 30px;
	}
</style>
<div class="container">
	<ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>production/dashboard" style="text-decoration: none;">Produksi</a></li>
      <li class="active">Produk Terjual</li>
    </ol>
	<div class="panel panel-danger">
		<div class="panel-heading">
			List Produk Terjual
		</div>
		<div class="panel-body">

			<form action="<?=site_url('production/products_income/search')?>" class="form-inline" id="form-filter" method="get">
				<input type="text" name="key_date" class="form-control" placeholder="Cari Tanggal">
				<input type="hidden" name="is_today">
				<input type="submit" value="Cari" class="btn btn-success">
				<input type="submit" value="Hari ini" class="btn btn-primary">
			</form>

			<p id="text-filter">Filter: <?=$date?></p>

			<div class="table-responsive">
				<table id="income_table" width="100%" class="table">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Produk</th>
							<th>Nama</th>
							<th>Terjual</th>
							<!-- <th>Tgl. Penjualan</th> -->
							<!-- <th>Harga</th>
							<th>Jml. Harga</th>
							<th>Jml. Biaya Decor</th>
							<th>Jml. Disc.</th>
							<th>Sub Total</th> -->
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach ($records->result() as $row) { ?>
						<tr>
							<td><?php echo $no++; ?></td>
							<td><?php echo $row->bel_id; ?></td>
							<td><?php echo $row->prod_name; ?></td>
							<td><?php echo $row->sum_qty; ?></td>
							<!-- <td><?php echo $row->insert_date; ?></td> -->
							<!-- <td style="text-align: right;"><?php echo number_format($row->harga); ?></td>
							<td style="text-align: right;"><?php echo number_format($row->sum_harga); ?></td>
							<td style="text-align: right;"><?php echo number_format($row->sum_decor); ?></td>
							<td style="text-align: right;"><?php echo number_format($row->sum_discount); ?></td>
							<td style="text-align: right;"><?php if($row->sum_subtotal>0) echo number_format($row->sum_subtotal);
							else echo number_format($row->sum_harga+$row->sum_decor); ?></td> -->
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
				
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#income_table').DataTable();

		$("input[value='Hari ini']").click(function() {
			$("input[name='is_today']").val("1");
		});

		$("input[value='Cari']").click(function() {
			$("input[name='is_today']").val("0");
		});

		$("input[name='key_date']").datetimepicker({
			dateFormat: 'dd M yy',
		    showTimepicker: false
		});

	});
</script>
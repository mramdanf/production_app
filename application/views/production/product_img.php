<div class="container" style="height: 100%;">
	<div class="panel panel-danger">
		<div class="panel-heading">
			Gambar Produk
		</div>
		<div class="panel-body">
			<table id="img_prod">
				<thead>
					<tr>
						<th>No.</th>
						<th>Kode Produk</th>
						<th>Nama</th>
						<th>Image Produk</th>
						<th>Detail Produk</th>
						<th>Pilihan</th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; foreach ($img_prod as $row) {  ?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $row->bel_id; ?></td>
						<td><?php echo $row->prod_name ?></td>
						<td><img src="<?php echo $row->img_prod; ?>" alt="" class="prod_img img-responsive"></td>
						<td><img src="<?php echo $row->img_prod_detail; ?>" alt="" class="prod_img img-responsive"></td>
						<td>
							<a href="<?php echo base_url(); ?>production/products/img_detail/<?php echo $row->prod_id; ?>" class="btn btn-success" title="Tambah foto">
								<span class="glyphicon glyphicon-pencil"></span>
							</a>
							<a href="<?php echo base_url(); ?>production/products/img_detail/<?php echo $row->prod_id; ?>" class="btn btn-primary" title="Tambah foto">
								<span class="glyphicon glyphicon-ok"></span>
							</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#img_prod').DataTable();
	})
</script>
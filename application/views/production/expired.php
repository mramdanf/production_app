<style>
	.panel-body a.btn-primary {
		margin-bottom: 30px;
	}
	.search-wrapper {
		margin-bottom: 30px;
	}
</style>
<div class="container">
	<div class="row">
		<ol class="breadcrumb">
	      <li><a href="<?php echo base_url(); ?>production/dashboard" style="text-decoration: none;">Produksi</a></li>
	      <li class="active">Expired Produk</li>
	    </ol>
	</div>
	<div class="row">
		<?php $alert = $this->session->flashdata('alert'); ?>
		<?php if ($alert != null) { ?>
		<div class="alert <?=$alert['type']?> alert-dismissable" role="alert">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $alert['msg']; ?>
		</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="panel panel-danger">
			<div class="panel-heading">
				Expired Produk
			</div>
			<div class="panel-body">
				<a href="<?=site_url('production/expired/form')?>" title="Tambah Data Expired" class="btn btn-primary">Tambah Data</a>
				<form id="form-search" class="form-inline search-wrapper">
					<input type="text" name="key_exp_date" placeholder="Tgl Expired" class="form-control">
					<button type="button" id="btn-do-search" class="btn btn-success">Cari</button>
					<button type="button" id="btn-refresh" class="btn btn-primary">Refresh</button>
				</form>
				<div class="table-responsive">
					<table id="exp-prod"  class="table">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode Produk</th>
								<th>Nama Produk</th>
								<th>Tanggal Expired</th>
								<th>Jumlah Expired</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						    <?php $no=1; foreach ($expired_prods as $expired_prod) { ?>
							<tr>
								<td>
									<?=$no++?>
									</td>
								<td><?=$expired_prod->belong_to_id?></td>
								<td><?=$expired_prod->prod_name?> - <?=$expired_prod->name?></td>
								<td><?=$this->app_helper->dateSimpleForm($expired_prod->exp_date)?></td>
								<td><?=$expired_prod->expired_product?></td>
								<td>
									<a href="<?=site_url('production/expired/form'.'/'.$expired_prod->id)?>" title="Edit Expired" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span></a>
									<a href="javascript:void(null)" title="Hapus Expired" class="btn btn-danger btn-delete" id="<?=$expired_prod->id?>"><span class="glyphicon glyphicon-trash"></span></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		var baseUrl = "<?=site_url('')?>";
		var expTable = $("#exp-prod").DataTable();
		var keyExpDate = $("input[name='key_exp_date']");

		$(".btn-delete").click(function(event) {
			if(!confirm("Anda yakin ?")){
				event.preventDefault();
			}
			$.post(baseUrl + "production/expired/delete", { id: this.id}, 
                function(response){
                  window.location.replace(baseUrl + "production/expired");
                }

            );	
		});

		$("#btn-do-search").click(function() {
			expTable
		        .columns( 3 )
		        .search(keyExpDate.val())
		        .draw();
		});

		$("#btn-refresh").click(function() {
	    	expTable
				 .search('')
				 .columns().search('')
				 .draw();
			$("#form-search").find("input[type=text]").val("");

	    });

		keyExpDate.datetimepicker({
		    dateFormat: 'dd M yy',
		    showTimepicker: false
		});
	});
</script>
<style>
	.panel-body a.btn-primary {
		margin-bottom: 30px;
	}
</style>
<div class="container">
	<ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>production/dashboard" style="text-decoration: none;">Produksi</a></li>
      <li class="active">Kategori</li>
    </ol>
    <?php $alert = $this->session->flashdata('alert'); ?>
	<?php if ($alert != null) { ?>
	<div class="alert <?=$alert['type']?> alert-dismissable" role="alert">
	    <button type="button" class="close" data-dismiss="alert">&times;</button>
	    <?php echo $alert['msg']; ?>
	</div>
	<?php } ?>
	<div class="panel panel-danger">
		<div class="panel-heading">
			Kategori produk
		</div>
		<div class="panel-body">
			<a href="<?=site_url('production/categories/form')?>" class="btn btn-primary">Tambah Kategori</a>
			<table id="category_table" width="100%" class="table">
				<thead>
					<tr>
						<th>No</th>
						<th>Kategori</th>
						<th>Hapus</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1; foreach ($categories as $row) { ?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $row->cat_name; ?></td>
						<td>
							<a href="#confrim_modal" title="Hapus data" class="btn btn-danger btn_delete" id="<?php echo $row->cat_id; ?>_<?php echo $row->cat_name; ?>" style="color: white;" data-toggle="modal">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</td>
						<td>
							<a href="<?php echo base_url(); ?>production/categories/form/<?php echo $row->cat_id; ?>" title="Edit data" class="btn btn-success" style="color: white;">
								<span class="glyphicon glyphicon-pencil"></span>
							</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="confrim_modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Konfirmasi</h4>
			</div>
			<div class="modal-body">
				<p id="confirm_msg">message</p>
			</div>
			<div class="modal-footer">
				<a href="javascript:void(null);" class="btn btn-primary" id="do_delete">
					<span class="glyphicon glyphicon-trash"></span> Hapus
				</a>
				<a href="javascript:void(null);" class="btn btn-danger" data-dismiss="modal">
					<span class="glyphicon glyphicon-minus-sign"></span> Batal
				</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var jv_cat_id;
	var jv_cat_name;
	$(document).ready(function(){
  		 var base_url = "<?=site_url('')?>";

		$('#category_table').DataTable();
		$('#properties_table').DataTable();

		$('.btn_delete').click(function(){
			var index_underscore = this.id.indexOf('_');
			jv_cat_name = this.id.substr(index_underscore+1);
			jv_cat_id = this.id.substring(0, index_underscore);

			// alert(jv_cat_name + ' ' + jv_cat_id)
			$('#confirm_msg').html('Kategori <strong>' + jv_cat_name + '</strong> akan dihapus?');

		});

		$('#do_delete').click(function(){
			$.post(base_url+'production/categories/del_cat', {cat_id: jv_cat_id, cat_name: jv_cat_name}, 
				function(response){
					window.location.replace(base_url+'production/categories');
				}
			);
		});
	});
</script>
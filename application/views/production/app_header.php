<!DOCTYPE html>
<html>
<head>
	<title><?=($title)?$title:"Heavenshard"?></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<link href="<?php echo base_url(); ?>/assets/css/jquery.dataTables.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/css/bootstrap.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/css/jquery-ui.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/css/bootstrap-chosen.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>/assets/css/fileinput.css" rel="stylesheet" />

	<script src="<?php echo base_url(); ?>/assets/js/jquery-1.12.3.min.js"></script>

	<style type="text/css">

		html {
		  position: relative;
		  min-height: 100%;
		}
		body {
		  /* Margin bottom by footer height */
		  margin-bottom: 60px;
		}
		.footer {
		  position: absolute;
		  bottom: 0;
		  width: 100%;
		  /* Set the fixed height of the footer here */
		  height: 60px;
		  background-color: #f5f5f5;
		}
		
	</style>
	
</head>

<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="<?php echo base_url(); ?>production/dashboard">Heavenshard</a>
		    	</div>
		    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		    		<ul class="nav navbar-nav">
			    		<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">Produk
							<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="<?=site_url('production/products')?>">Data Produk</a></li>
								<li><a href="<?=site_url('production/products_income')?>">Produk Terjual</a></li>
								<li><a href="<?=site_url('production/products_visual')?>">Products Visual</a></li>
							</ul>
						</li>
			    		<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">Kategori
							<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="<?=site_url('production/categories')?>">Kategori Produk</a></li>
								<li><a href="<?=site_url('production/categories/properties')?>">Produk dan Properti</a></li>

							</ul>
						</li>
			    		<li><a href="<?php echo base_url(); ?>production/expired">Expired Products</a></li>
			      	</ul>
			      	<div class="navbar-form navbar-right">
	                    <form action="<?php echo base_url(); ?>production/dashboard/sign_out" method="post" id="form_logout">
	                    	<div class="form-group">
	                    		<button type="submit" for="form_logout" class="btn btn-danger">
	                    		<span class="glyphicon glyphicon-log-out"></span> Sign out
	                    	</button>
	                    	</div>
	                    	
	                    </form>
	                </div>
			    </div>

			</div>
		</div>
			
	</nav>

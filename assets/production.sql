-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2016 at 12:59 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `production`
--

-- --------------------------------------------------------

--
-- Table structure for table `belong_to`
--

CREATE TABLE `belong_to` (
  `id` int(11) NOT NULL,
  `bel_id` varchar(100) NOT NULL,
  `categori_id` varchar(30) NOT NULL,
  `product_id` varchar(40) NOT NULL,
  `properties_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `belong_to`
--

INSERT INTO `belong_to` (`id`, `bel_id`, `categori_id`, `product_id`, `properties_id`) VALUES
(5, '511', '1', '5', 1),
(6, '512', '1', '6', 2),
(7, '521', '1', '7', 1),
(8, '522', '1', '8', 2),
(9, '531', '1', '9', 1),
(10, '532', '1', '10', 2),
(11, '541', '1', '11', 1),
(12, '542', '1', '12', 2),
(13, '551', '1', '13', 1),
(14, '552', '1', '14', 2),
(15, '561', '1', '15', 1),
(16, '562', '1', '16', 2),
(17, '571', '1', '18', 1),
(18, '572', '1', '18', 2),
(19, '581', '1', '19', 1),
(20, '582', '1', '20', 2),
(21, '591', '1', '21', 1),
(22, '592', '1', '22', 2),
(23, '5101', '1', '23', 1),
(24, '5102', '1', '24', 2),
(25, '5111', '1', '25', 1),
(26, '5112', '1', '26', 2),
(27, '5121', '1', '27', 1),
(28, '5122', '1', '28', 2),
(29, '5131', '1', '29', 1),
(30, '5132', '1', '30', 2),
(31, '5141', '1', '31', 1),
(32, '5142', '1', '32', 2),
(33, '5151', '1', '33', 1),
(34, '5152', '1', '34', 2),
(35, '5161', '1', '35', 1),
(36, '5162', '1', '36', 2),
(37, '5171', '1', '37', 1),
(38, '5172', '1', '38', 2),
(39, '5181', '1', '39', 1),
(40, '5182', '1', '40', 2),
(41, '5191', '1', '41', 1),
(42, '5192', '1', '42', 2),
(43, '5201', '1', '43', 1),
(44, '5202', '1', '44', 2),
(45, '5211', '1', '45', 1),
(46, '5212', '1', '46', 2),
(47, '5221', '1', '47', 1),
(48, '5222', '1', '48', 2),
(49, '5231', '1', '49', 1),
(50, '5232', '1', '50', 2),
(51, '5241', '1', '51', 1),
(52, '5242', '1', '52', 2),
(53, '5251', '1', '53', 1),
(54, '5252', '1', '54', 2),
(55, '5261', '1', '55', 1),
(56, '5262', '1', '56', 2),
(57, '5271', '1', '57', 1),
(58, '5272', '1', '58', 2),
(59, '5281', '1', '59', 1),
(60, '5282', '1', '60', 2),
(61, '5291', '1', '61', 1),
(62, '5292', '1', '62', 2),
(63, '5301', '1', '63', 1),
(64, '5302', '1', '64', 2),
(65, '5311', '1', '65', 1),
(66, '5312', '1', '66', 2),
(67, '5321', '1', '67', 1),
(68, '5322', '1', '68', 2),
(69, '5331', '1', '69', 1),
(70, '5332', '1', '70', 2),
(71, '5341', '1', '71', 1),
(72, '5342', '1', '72', 2),
(73, '5351', '1', '73', 1),
(74, '5352', '1', '74', 2),
(75, '5361', '1', '75', 1),
(76, '5362', '1', '76', 2),
(77, '5371', '1', '77', 1),
(78, '5381', '1', '78', 1),
(81, '113', '2', '81', 5),
(82, '114', '2', '82', 6),
(83, '115', '2', '83', 7),
(84, '116', '2', '84', 8),
(85, '117', '2', '85', 9),
(86, '118', '2', '86', 10),
(87, '119', '2', '87', 11),
(88, '121', '2', '88', 3),
(89, '122', '2', '89', 4),
(90, '123', '2', '90', 5),
(91, '124', '2', '91', 6),
(92, '125', '2', '92', 7),
(93, '126', '2', '93', 7),
(94, '127', '2', '94', 9),
(95, '128', '2', '95', 10),
(96, '129', '2', '96', 11),
(97, '131', '2', '97', 3),
(98, '132', '2', '98', 4),
(99, '133', '2', '99', 5),
(100, '134', '2', '100', 6),
(101, '135', '2', '101', 7),
(102, '136', '2', '102', 8),
(103, '137', '2', '103', 9),
(104, '138', '2', '104', 10),
(105, '139', '2', '105', 11),
(106, '141', '2', '106', 3),
(107, '142', '2', '107', 4),
(108, '143', '2', '108', 5),
(109, '144', '2', '109', 6),
(110, '145', '2', '110', 7),
(111, '146', '2', '111', 8),
(112, '147', '2', '112', 9),
(113, '148', '2', '113', 10),
(114, '149', '2', '114', 11),
(115, '151', '2', '115', 3),
(116, '152', '2', '116', 4),
(117, '153', '2', '117', 5),
(118, '154', '2', '118', 6),
(119, '155', '2', '119', 7),
(120, '156', '2', '120', 8),
(121, '157', '2', '121', 9),
(122, '158', '2', '122', 10),
(123, '159', '2', '123', 11),
(124, '161', '2', '124', 3),
(125, '162', '2', '125', 4),
(126, '163', '2', '126', 5),
(127, '164', '2', '127', 6),
(128, '165', '2', '128', 7),
(129, '166', '2', '129', 8),
(130, '167', '2', '130', 9),
(131, '168', '2', '131', 10),
(132, '169', '2', '132', 11),
(133, '171', '2', '133', 3),
(134, '172', '2', '134', 4),
(135, '173', '2', '135', 5),
(136, '174', '2', '136', 6),
(137, '175', '2', '137', 7),
(138, '176', '2', '138', 8),
(139, '177', '2', '139', 9),
(140, '178', '2', '141', 10),
(141, '179', '2', '141', 11),
(142, '181', '2', '142', 3),
(143, '182', '2', '143', 4),
(144, '183', '2', '144', 5),
(145, '184', '2', '145', 6),
(146, '185', '2', '146', 7),
(147, '186', '2', '147', 8),
(148, '187', '2', '148', 9),
(149, '188', '2', '149', 10),
(150, '189', '2', '150', 11),
(151, '191', '2', '151', 3),
(152, '192', '2', '152', 4),
(153, '193', '2', '153', 5),
(154, '194', '2', '154', 6),
(155, '195', '2', '155', 7),
(156, '196', '2', '156', 8),
(157, '197', '2', '157', 9),
(158, '198', '2', '158', 10),
(159, '199', '2', '159', 11),
(160, '1101', '2', '160', 3),
(161, '1102', '2', '161', 4),
(162, '1103', '2', '162', 5),
(163, '1104', '2', '163', 6),
(164, '1105', '2', '164', 7),
(165, '1106', '2', '165', 8),
(166, '1107', '2', '166', 9),
(167, '1108', '2', '167', 10),
(168, '1109', '2', '168', 11),
(169, '1111', '2', '169', 3),
(170, '1112', '2', '170', 4),
(171, '1113', '2', '171', 5),
(172, '1114', '2', '172', 6),
(173, '1115', '2', '173', 7),
(174, '1116', '2', '174', 8),
(175, '1117', '2', '175', 9),
(176, '1118', '2', '176', 10),
(177, '1119', '2', '177', 11),
(178, '211', '3', '178', 14),
(179, '212', '3', '179', 15),
(180, '221', '3', '180', 14),
(181, '222', '3', '181', 15),
(182, '231', '3', '182', 14),
(183, '232', '3', '183', 15),
(184, '241', '3', '184', 14),
(185, '242', '3', '185', 15),
(186, '611', '4', '186', 16),
(187, '612', '4', '187', 17),
(188, '621', '4', '188', 16),
(189, '622', '4', '189', 17),
(190, '631', '4', '190', 16),
(191, '632', '4', '191', 17),
(192, '641', '4', '192', 16),
(193, '642', '4', '193', 17),
(194, '643', '4', '194', 18),
(195, '651', '4', '195', 16),
(196, '652', '4', '196', 17),
(197, '661', '4', '197', 17),
(198, '662', '4', '198', 17),
(199, '671', '4', '199', 16),
(200, '672', '4', '200', 17),
(201, '681', '4', '201', 16),
(202, '682', '4', '202', 17),
(203, '691', '4', '203', 16),
(204, '692', '4', '204', 17),
(205, '6101', '4', '205', 16),
(206, '6102', '4', '206', 17),
(207, '711', '5', '207', 19),
(208, '721', '5', '208', 19),
(209, '731', '5', '209', 19),
(210, '741', '5', '210', 19),
(211, '751', '5', '211', 19),
(212, '761', '5', '212', 19),
(213, '771', '5', '213', 19),
(214, '781', '5', '214', 19),
(215, '791', '5', '215', 19),
(216, '811', '6', '216', 20),
(217, '812', '6', '217', 21),
(218, '813', '6', '218', 22),
(219, '821', '6', '219', 20),
(220, '822', '6', '220', 21),
(221, '823', '6', '221', 22),
(222, '831', '6', '222', 20),
(223, '832', '6', '223', 21),
(224, '833', '6', '224', 22),
(225, '841', '6', '225', 20),
(226, '842', '6', '226', 21),
(227, '843', '6', '227', 22),
(228, '851', '6', '228', 20),
(229, '852', '6', '229', 21),
(230, '853', '6', '230', 22),
(231, '861', '6', '231', 20),
(232, '862', '6', '232', 21),
(233, '863', '6', '233', 22),
(235, '912', '7', '235', 26),
(236, '911', '7', '236', 25),
(237, '921', '7', '237', 25),
(238, '922', '7', '238', 26),
(239, '931', '7', '239', 25),
(240, '932', '7', '240', 26),
(241, '941', '7', '241', 25),
(242, '942', '7', '242', 26),
(243, '951', '7', '243', 25),
(245, '961', '7', '245', 25),
(246, '971', '7', '246', 25),
(247, '981', '7', '247', 25),
(248, '1011', '8', '248', 0),
(249, '1021', '8', '249', 0),
(250, '1031', '8', '250', 0),
(251, '1041', '8', '251', 0),
(252, '1110', '9', '252', 0),
(253, '1121', '9', '253', 0),
(254, '1131', '9', '254', 0),
(255, '1141', '9', '255', 0),
(256, '1151', '9', '256', 0),
(257, '1211', '10', '257', 0),
(259, '111', '2', '79', 3),
(260, '112', '2', '80', 4),
(261, '5391', '1', '260', 1),
(262, '5392', '1', '261', 2),
(263, '1221', '10', '262', 0),
(264, '1311', '11', '263', 0),
(265, '1321', '11', '264', 0),
(266, '1331', '11', '265', 0),
(267, '1341', '11', '266', 0),
(268, '1351', '11', '267', 0),
(269, '1361', '11', '268', 0),
(270, '1371', '11', '269', 0),
(271, '1381', '11', '270', 0),
(272, '1391', '11', '271', 0),
(273, '13101', '11', '272', 0),
(276, '13111', '11', '275', 0),
(277, '1122', '2', '276', 4),
(278, '1124', '2', '277', 6),
(279, '1125', '2', '278', 7),
(280, '1126', '2', '279', 8),
(281, '1127', '2', '280', 9),
(282, '1128', '2', '281', 10),
(283, '1132', '2', '282', 0),
(284, '1142', '2', '283', 3),
(285, '1152', '2', '284', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categori`
--

CREATE TABLE `categori` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categori`
--

INSERT INTO `categori` (`cat_id`, `cat_name`) VALUES
(1, 'Bread'),
(2, 'Cake'),
(3, 'Sand Cake'),
(4, 'Savourie'),
(5, 'Cookie'),
(6, 'Idul Fitri Spesial'),
(7, 'X''mas Spesial'),
(8, 'Easter Special'),
(9, 'Valentine Cake'),
(10, 'Imlek Spesial'),
(11, 'Drink');

-- --------------------------------------------------------

--
-- Table structure for table `decoration`
--

CREATE TABLE `decoration` (
  `id` int(11) NOT NULL,
  `order_detail_id` int(11) DEFAULT NULL,
  `decor_id` varchar(100) DEFAULT NULL COMMENT 'ini sebenernya bel_id, tp supaya tidak tertukar namanya diganti',
  `qty` int(11) NOT NULL,
  `sub_total` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `decoration`
--

INSERT INTO `decoration` (`id`, `order_detail_id`, `decor_id`, `qty`, `sub_total`) VALUES
(1, 1, '1122', 1, 30000),
(2, 1, '1142', 1, 40000),
(3, 3, '1122', 1, 30000),
(4, 8, '1122', 1, 30000),
(5, 13, '1122', 1, 30000),
(6, 13, '1142', 1, 40000),
(7, 12, '1122', 1, 30000),
(8, 12, '1142', 2, 80000);

-- --------------------------------------------------------

--
-- Table structure for table `decoration_product`
--

CREATE TABLE `decoration_product` (
  `id` varchar(100) NOT NULL,
  `name` text,
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `id` int(11) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `persen` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL COMMENT '0-inactive; 1-active;'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`id`, `last_update`, `created_date`, `persen`, `is_active`) VALUES
(1, '2016-11-20 11:11:25', '2016-11-08 00:00:00', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `expired`
--

CREATE TABLE `expired` (
  `id` int(11) NOT NULL,
  `belong_to_id` int(11) DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `expired_product` int(11) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expired`
--

INSERT INTO `expired` (`id`, `belong_to_id`, `exp_date`, `expired_product`, `last_update`) VALUES
(19, 521, '2016-11-09', 4, '2016-11-09 21:11:10'),
(24, 531, '2016-11-08', 1, '2016-11-09 21:11:47'),
(27, 1101, '2016-11-09', 1, '2016-11-09 21:11:34'),
(28, 671, '2016-11-20', 3, '2016-11-20 12:04:22');

-- --------------------------------------------------------

--
-- Table structure for table `fitur`
--

CREATE TABLE `fitur` (
  `id` int(11) NOT NULL,
  `name` text,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fitur`
--

INSERT INTO `fitur` (`id`, `name`, `is_active`) VALUES
(1, 'Edit Order', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` text NOT NULL,
  `order_name` varchar(100) DEFAULT NULL,
  `order_recipient` text,
  `order_add` varchar(200) DEFAULT NULL,
  `order_phone` varchar(30) DEFAULT NULL,
  `order_phone_recipient` varchar(16) DEFAULT NULL,
  `order_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `order_deliv_date` timestamp NULL DEFAULT NULL,
  `order_tax` float DEFAULT NULL,
  `order_type` varchar(50) DEFAULT NULL,
  `order_amount` float DEFAULT NULL,
  `order_paid` float DEFAULT NULL,
  `order_paid_return` float DEFAULT NULL,
  `ongkir` int(11) DEFAULT NULL,
  `kelunasan` int(11) DEFAULT NULL COMMENT 'field ini blm digunakan',
  `status` varchar(50) DEFAULT NULL,
  `close_time` datetime DEFAULT NULL,
  `shift` int(11) DEFAULT NULL COMMENT '0-pagi; 1-siang;'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `order_name`, `order_recipient`, `order_add`, `order_phone`, `order_phone_recipient`, `order_date`, `order_deliv_date`, `order_tax`, `order_type`, `order_amount`, `order_paid`, `order_paid_return`, `ongkir`, `kelunasan`, `status`, `close_time`, `shift`) VALUES
(1, 'HVSRD/ORDER/20161130/1', 'syamsudin', 'siti', 'Cileungsi', '087333444555', '087333444555', '2016-11-30 09:38:21', '2016-12-01 04:00:00', 0, 'Delivery', 343500, 350000, 6500, 30000, NULL, 'COMPLETE', '2016-11-30 21:11:37', 1),
(2, 'HVSRD/ORDER/20161130/2', '', '', '', '', '', '2016-11-30 09:54:21', NULL, 0, 'Pickup', 275500, 300000, 24500, 0, NULL, 'COMPLETE', '2016-11-30 21:11:37', 1),
(3, 'HVSRD/ORDER/20161201/3', '', '', '', '', '', '2016-12-01 02:43:33', NULL, 0, 'Pickup', 38000, 40000, 2000, 0, NULL, 'COMPLETE', '2016-12-01 13:12:43', 0),
(4, 'HVSRD/ORDER/20161201/4', 'Sulaiman', 'Abdi', 'Pancoran', '085111222333', '083555666777', '2016-12-01 02:48:13', '2016-12-02 06:00:00', 0, 'Delivery', 297000, 300000, 3000, 20000, NULL, 'COMPLETE', '2016-12-01 17:12:43', 1),
(5, 'HVSRD/ORDER/20161202/5', 'Ibu Uju', 'Bapak Umar', 'Jl Suyakencana', '085111222333', '083555666777', '2016-12-02 03:37:58', '2016-12-03 06:00:00', 0, 'Delivery', 827000, 830000, 3000, 200000, NULL, 'COMPLETE', '2016-12-02 13:00:00', 0),
(6, 'HVSRD/ORDER/20161202/6', '', '', '', '', '', '2016-12-02 03:58:39', NULL, 0, 'Pickup', 28000, 30000, 2000, 0, NULL, 'COMPLETE', '2016-12-02 21:00:00', 1),
(8, '', NULL, NULL, NULL, NULL, NULL, '2016-12-03 23:56:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-12-04 06:12:28', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `order_detail_id` int(11) NOT NULL,
  `order_id` text NOT NULL,
  `bel_id` varchar(50) NOT NULL,
  `detail_price` float NOT NULL COMMENT 'price*additional_price (if exist)',
  `detail_qty` int(11) NOT NULL,
  `pending` int(11) NOT NULL,
  `status` varchar(30) NOT NULL,
  `insert_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `notes` text,
  `total_decor` float DEFAULT '0',
  `discount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`order_detail_id`, `order_id`, `bel_id`, `detail_price`, `detail_qty`, `pending`, `status`, `insert_date`, `notes`, `total_decor`, `discount`) VALUES
(1, 'HVSRD/ORDER/20161130/1', '132', 240000, 1, 0, 'COMPLETE', '2016-11-30 09:36:59', 'happy aniv', 70000, 5),
(2, 'HVSRD/ORDER/20161130/1', '671', 20000, 2, 0, 'COMPLETE', '2016-11-30 09:37:30', '', 0, 5),
(3, 'HVSRD/ORDER/20161130/2', '132', 240000, 1, 0, 'COMPLETE', '2016-11-30 09:53:44', 'happy aniv', 30000, 5),
(4, 'HVSRD/ORDER/20161130/2', '671', 19000, 2, 0, 'COMPLETE', '2016-11-30 09:54:02', '', 0, 5),
(5, 'HVSRD/ORDER/20161201/3', '521', 14000, 2, 0, 'COMPLETE', '2016-12-01 02:40:43', '', 0, NULL),
(6, 'HVSRD/ORDER/20161201/3', '511', 14000, 2, 0, 'COMPLETE', '2016-12-01 02:43:10', '', 0, NULL),
(7, 'HVSRD/ORDER/20161201/3', '671', 10000, 1, 0, 'COMPLETE', '2016-12-01 02:43:24', '', 0, NULL),
(8, 'HVSRD/ORDER/20161201/4', '132', 240000, 1, 0, 'COMPLETE', '2016-12-01 02:44:22', 'happy wedding', 30000, NULL),
(11, 'HVSRD/ORDER/20161201/4', '511', 7000, 1, 0, 'COMPLETE', '2016-12-01 02:46:35', '', 0, NULL),
(12, 'HVSRD/ORDER/20161202/5', '132', 240000, 1, 0, 'COMPLETE', '2016-12-02 03:34:56', 'happy aniv', 110000, 5),
(13, 'HVSRD/ORDER/20161202/5', '122', 240000, 1, 0, 'COMPLETE', '2016-12-02 03:35:30', 'selamat ultah', 70000, 5),
(14, 'HVSRD/ORDER/20161202/6', '531', 14000, 2, 0, 'COMPLETE', '2016-12-02 03:58:00', '', 0, NULL),
(15, 'HVSRD/ORDER/20161202/6', '551', 14000, 2, 0, 'COMPLETE', '2016-12-02 03:58:28', '', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `prod_id` int(11) NOT NULL,
  `prod_name` varchar(100) NOT NULL,
  `harga` float NOT NULL,
  `stocks` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `img_hot_list` varchar(200) NOT NULL,
  `img_recomended` varchar(100) NOT NULL,
  `img_prod_detail` varchar(200) NOT NULL,
  `img_prod` varchar(200) NOT NULL,
  `barcode` varchar(200) NOT NULL,
  `singkron_flag` varchar(50) NOT NULL,
  `discount` float DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `alias` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`prod_id`, `prod_name`, `harga`, `stocks`, `description`, `img_hot_list`, `img_recomended`, `img_prod_detail`, `img_prod`, `barcode`, `singkron_flag`, `discount`, `last_update`, `alias`) VALUES
(3, 'Test', 1, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(5, 'Cheese Plait', 7000, 5, '', '', '', '', '', '', '', 0, '2016-11-19 16:12:30', NULL),
(6, 'Cheese Plait', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(7, 'Banana Cheese Bun', 7000, 0, '', '', '', '', '', '', '', 0, '2016-11-16 14:21:43', NULL),
(8, 'Banana Cheese Bun', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(9, 'Banana Choco Bun', 7000, 8, '', '', '', '', '', '', '', 0, '2016-12-02 10:57:48', NULL),
(10, 'Banana Choco Bun', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(11, 'Red Bean Bun', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(12, 'Red Bean Bun', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(13, 'Green Bean Bun', 7000, 8, '', '', '', '', '', '', '', 0, '2016-11-13 17:17:14', NULL),
(14, 'Green Bean Bun', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(15, 'Sarikaya Bun', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(16, 'Sarikaya Bun', 6000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(17, 'Coconut Bun', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(18, 'Coconut Bun', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(19, 'Choco Chipp Bun', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(20, 'Choco Chipp Bun', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(21, 'Greenteea Bun', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(22, 'Greentea Bun', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(23, 'Mixed Feel Bun', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(24, 'Mixed Feel Bun', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(25, 'Coffee Bun', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(26, 'Coffee Bun', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(27, 'Beff Bun', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(28, 'Beff Bun', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(29, 'Abbon Bread', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(30, 'Abbon Bread', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(31, 'Plain Croissant', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(32, 'Plain Croissant', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(33, 'Choco Croissant', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(34, 'Choco Croissant', 6000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(35, 'Cheese Croissant', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(36, 'Cheese Croissant', 6000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(37, 'Almond Croissant', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(38, 'Almond Croissant', 6000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(39, 'Cinamon Roll Danish', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(40, 'Cinamon Roll Danish', 6000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(41, 'Fruit Danish', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(42, 'Fruit Danish', 6000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(43, 'Cheese Bun', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(44, 'Cheess Bun', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(45, 'Raissin Plait', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(46, 'Raissin Plait', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(47, 'Raissin Roll', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(48, 'Raissin Roll', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(49, 'Choco Mocca', 13000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(50, 'Choco Mocca', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(51, 'Blueberry Cream Cheese', 9000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(52, 'Blueberry Cream Cheese', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(53, 'Choco Cheese', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(54, 'Choco Cheese', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(55, 'Creamy Butter Cream', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(56, 'Creamy Butter Cream', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(57, 'Creamy Custard Bun', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(58, 'Creamy Custard Bun', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(59, 'Choco Makuro', 15000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(60, 'Choco Makuro', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(61, 'Choco Bun', 9000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(62, 'Choco Bun', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(63, 'Mexican Choco Bun', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(64, 'Mexican Choco Bun', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(65, 'Raissin Cheese Bun', 7000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(66, 'Raissin Cheese Bun', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(67, 'Manggo Bun', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(68, 'Manggo Bun', 6000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(69, 'Strawberry Bun', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(70, 'Strawberry Bun', 6000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(71, 'Durian Split', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(72, 'Durian Split', 6000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(73, 'Lemon Bun', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(74, 'Lemon Bun', 6000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(75, 'Banana Choco Cheese', 7000, 13, '', '', '', '', '', '', '', 0, NULL, NULL),
(76, 'Banana Choco Cheese', 5000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(77, 'White Toast Bread', 14000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(78, 'Whole Wheat Tost', 17000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(79, 'New York Cheese', 25000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(80, 'New York Cheese', 240000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(81, 'New York Cheese', 135000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(82, 'New York Cheese', 245000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(83, 'New York Cheese', 350000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(84, 'New York Cheese', 525000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(85, 'New York Cheese', 680000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(86, 'New York Cheese', 920000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(87, 'New York Cheese', 1380000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(88, 'Strawberry Cheese', 25000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(89, 'Strawberry Cheese', 240000, 8, '', '', '', '', '', '', '', 0, '2016-11-22 13:20:09', NULL),
(90, 'Strawberry Cheese', 135000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(91, 'Strawberry Cheese', 240000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(92, 'Strawberry Cheese', 350000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(93, 'Strawberry Cheese', 525000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(94, 'Strawberry Cheese', 680000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(95, 'Strawberry Cheese', 920000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(96, 'Strawberry Cheese', 1380000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(97, 'Blueberry Cheese', 25000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(98, 'Blueberry Cheese', 240000, 8, '', '', '', '', '', '', '', 0, '2016-11-25 14:36:08', NULL),
(99, 'Blueberry Cheese', 135000, 3, '', '', '', '', '', '', '', 0, '2016-11-20 11:59:41', NULL),
(100, 'Blueberry Cheese', 245000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(101, 'Blueberry Cheese', 350000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(102, 'Blueberry Cheese', 525000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(103, 'Blueberry Cheese', 680000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(104, 'Blueberry Cheese', 920000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(105, 'Blueberry Cheese', 1380000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(106, 'Tiramisu', 25000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(107, 'Tiramisu', 240000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(108, 'Tiramisu', 135000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(109, 'Tiramisu', 245000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(110, 'Tiramisu', 350000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(111, 'Tiramisu', 525000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(112, 'Tiramisu', 680000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(113, 'Tiramisu', 920000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(114, 'Tiramisu', 1380000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(115, 'Opera', 20000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(116, 'Opera', 200000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(117, 'Opera', 125000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(118, 'Opera', 220000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(119, 'Opera', 335000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(120, 'Opera', 465000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(121, 'Opera', 625000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(122, 'Opera', 840000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(123, 'Opera', 1350000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(124, 'Black Forest', 20000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(125, 'Black Forest', 200000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(126, 'Black Forest', 125000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(127, 'Black Forest', 220000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(128, 'Black Forest', 335000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(129, 'Black Forest', 465000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(130, 'Black Forest', 625000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(131, 'Black Forest', 840000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(132, 'Black Forest', 1235000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(133, 'Duo Choco Mousse', 20000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(134, 'Duo Choco Mousse', 200000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(135, 'Duo Choco Mousse', 125000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(136, 'Duo Choco Mousse', 220000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(137, 'Duo Choco Mousse', 335000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(138, 'Duo Choco Mousse', 465000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(139, 'Dou Choco Mousse', 625000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(140, 'Duo Choco Mousse', 840000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(141, 'Duo Choco Mousse', 1235000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(142, 'Double Chocolate', 20000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(143, 'Double Chocolate', 200000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(144, 'Double Chocolate', 125000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(145, 'Double Chocolate', 220000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(146, 'Double Chocolate', 335000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(147, 'Double Chocolate', 465000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(148, 'Double Chocolate', 625000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(149, 'Double Chocolate', 840000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(150, 'Double Chocolate', 1235000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(151, 'Oreo Cheese', 25000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(152, 'Oreo Cheese', 240000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(153, 'Oreo Cheese', 135000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(154, 'Oreo Cheese', 245000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(155, 'Oreo Cheese', 350000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(156, 'Oreo Cheese', 525000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(157, 'Oreo Cheese', 680000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(158, 'Oreo Cheese', 920000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(159, 'Oreo Cheese', 1380000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(160, 'African Chocolate', 20000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(161, 'African Chocolate', 200000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(162, 'African Chocolate', 125000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(163, 'African Chocolate', 220000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(164, 'African Chocolate', 335000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(165, 'African Chocolate', 465000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(166, 'African Chocolate', 625000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(167, 'African Chocolate', 840000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(168, 'African Chocolate', 1235000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(169, 'Red Velvet', 25000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(170, 'Red Velvet', 240000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(171, 'Red Velvet', 135000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(172, 'Red Velvet', 245000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(173, 'Red Velvet', 350000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(174, 'Red Velvet', 525000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(175, 'Red Velvet', 680000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(176, 'Red Velvet', 920000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(177, 'Red Velvet', 1380000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(178, 'Banana', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(179, 'Banana', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(180, 'Chocolate Chip', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(181, 'Chocolate Chip', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(182, 'English Fruit', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(183, 'Chocolate Chip', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(184, 'Cassava', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(185, 'Cassava', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(186, 'Berliner', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(187, 'Berliner', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(188, 'Donat', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(189, 'Donat', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(190, 'Cronut', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(191, 'Cronut', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(192, 'Muffin', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(193, 'Muffin', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(194, 'Muffin', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(195, 'Apple Pie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(196, 'Apple Pie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(197, 'Banana Pie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(198, 'Banana Pie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(199, 'Beef Sausage Bun', 10000, 6, '', '', '', '', '', '', '', 0, '2016-11-24 16:49:44', NULL),
(200, 'Beef Sausage Bun', 8000, -2, '', '', '', '', '', '', '', NULL, NULL, NULL),
(201, 'Chicken Sausage Bun', 10000, 10, '', '', '', '', '', '', '', 0, '2016-11-19 16:11:33', NULL),
(202, 'Chicken Sausage Bun', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(203, 'Hot Dog', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(204, 'Hot Dog', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(205, 'Burger', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(206, 'Burger', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(207, 'Vanilla Cookie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(208, 'Almond Cookie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(209, 'Choco Chip Cookie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(210, 'Peanut Cookie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(211, 'Ginger Snap Cookie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(212, 'Outmeal Cookie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(213, 'Giant Cookie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(214, 'Cheese Finger', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(215, 'Almond Tuiles', 100000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(216, 'Lapis Surabaya Plain', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(217, 'Lapis Surabaya Plain', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(218, 'Lapis Surabaya Plain', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(219, 'Lapis Surabaya Prunes', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(220, 'Lapis Surabaya Prunes', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(221, 'Lapis Surabaya Prunes', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(222, 'Lapis Legit Plain', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(223, 'Lapis Legit Plain', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(224, 'Lapis Legin Plain', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(225, 'Lapis Legit Prunes', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(226, 'Lapis Legit Prunes', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(227, 'Lapis Legit Prunes', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(228, 'Lapis Legit Cheese', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(229, 'Lapis Legit Cheese', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(230, 'Lapis Legit Cheese', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(231, 'Nastar', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(232, 'Nastar', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(233, 'Nastar', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(235, 'X''mas Stollen', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(236, 'X''mas Stollen', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(237, 'Ginger X''mas Bread', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(238, 'Ginger X''mas Bread', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(239, 'Ginger X''mas Man', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(240, 'Ginger X''mas Man', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(241, 'Ginger Bread House', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(242, 'Ginger Bread House', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(243, 'X''mas Cookie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(245, 'Panetone', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(246, 'X''mas Wreath', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(247, 'X''log', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(248, 'Hot Cross Bun', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(249, 'Easter Chocolate', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(250, 'Easter Egg', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(251, 'Mince Pie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(252, 'Valentine Cake', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(253, 'Choco Valentine', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(254, 'Praline Valentine', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(255, 'Valentine Cookie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(256, 'Valentine Choco Rose', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(257, 'Fortune Cookie', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(260, 'Blueberry Split', 8000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(261, 'Blueberry Split', 6000, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(262, 'Imlek Cake', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(263, 'Espresso', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(264, 'Cappucino', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(265, 'Caffe Latte', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(266, 'Caffe Mocha', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(267, 'Black Caffe', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(268, 'English Tea', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(269, 'Cammomile Tea', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(270, 'Darjeling Tea', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(271, 'The Kotak', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(272, 'Mineral Water', 0, 0, '', '', '', '', '', '', '', NULL, NULL, NULL),
(275, 'Soft Drink', 0, 0, '', '', '', '', '', '', '', 0, '2016-11-18 10:06:32', NULL),
(276, 'Decor Cream', 30000, 2, '', '', '', '', '', '', '', 0, '2016-11-21 08:31:08', NULL),
(277, 'Decor Cream', 50000, 0, '', '', '', '', '', '', '', 0, '2016-11-21 08:31:30', NULL),
(278, 'Decor Cream', 50000, 0, '', '', '', '', '', '', '', 0, '2016-11-21 08:31:53', NULL),
(279, 'Decor Cream', 60000, 0, '', '', '', '', '', '', '', 0, '2016-11-21 08:32:04', NULL),
(280, 'Decor Cream', 80000, 0, '', '', '', '', '', '', '', 0, '2016-11-21 08:32:22', NULL),
(281, 'Decor Cream', 100000, 0, '', '', '', '', '', '', '', 0, '2016-11-21 08:32:50', NULL),
(282, 'Boneka Karakter', 50000, 0, '', '', '', '', '', '', '', 0, '2016-11-21 09:26:45', NULL),
(283, 'Kembang', 40000, 0, '', '', '', '', '', '', '', 0, '2016-11-22 10:20:08', NULL),
(284, 'Boneka Icing', 40000, 0, '', '', '', '', '', '', '', 0, '2016-11-21 09:33:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `categori_id` int(11) DEFAULT NULL,
  `name` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `categori_id`, `name`) VALUES
(1, 1, 'STD'),
(2, 1, 'MINI'),
(3, 2, 'SLICE/CUP'),
(4, 2, 'D20'),
(5, 2, '10X20'),
(6, 2, '20X20'),
(7, 2, '20X30'),
(8, 2, '30X30'),
(9, 2, '30X40'),
(10, 2, '40X40'),
(11, 2, '40X60'),
(14, 3, 'SLICE'),
(15, 3, 'LOAF'),
(16, 4, 'STD'),
(17, 4, 'MINI'),
(18, 4, 'DECOR'),
(19, 5, 'STD'),
(20, 6, '20X20'),
(21, 6, '10X20'),
(22, 6, 'SLICE'),
(25, 7, 'SMALL'),
(26, 7, 'LARGE'),
(34, 12, 'D20'),
(35, 12, '20x20'),
(36, 12, '20x30'),
(37, 12, '30x30'),
(38, 12, '30x40'),
(39, 12, '40x40'),
(40, 12, '40x60');

-- --------------------------------------------------------

--
-- Table structure for table `singkronisasi`
--

CREATE TABLE `singkronisasi` (
  `id` int(11) NOT NULL,
  `table_id` varchar(100) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL,
  `action_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `singkronisasi`
--

INSERT INTO `singkronisasi` (`id`, `table_id`, `table_name`, `action`, `action_time`) VALUES
(1, '17', 'products', 'delete', '2016-05-20 06:22:17'),
(2, 'HVSRD-000011', 'belong_to', 'delete', '2016-05-20 06:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `user_table`
--

CREATE TABLE `user_table` (
  `user_table_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `privilage` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_table`
--

INSERT INTO `user_table` (`user_table_id`, `first_name`, `last_name`, `privilage`, `username`, `password`) VALUES
(2, 'heaven', 'heaven', 'admin', 'heaven', '21232f297a57a5a743894a0e4a801fc3'),
(41, 'produksi', 'produksi', 'Produksi', 'produksi', '202cb962ac59075b964b07152d234b70'),
(50, 'kasir', 'kasir', 'End Customer', 'kasir', '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `belong_to`
--
ALTER TABLE `belong_to`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categori`
--
ALTER TABLE `categori`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `decoration`
--
ALTER TABLE `decoration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `decoration_product`
--
ALTER TABLE `decoration_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expired`
--
ALTER TABLE `expired`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fitur`
--
ALTER TABLE `fitur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_detail_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`prod_id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `singkronisasi`
--
ALTER TABLE `singkronisasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_table`
--
ALTER TABLE `user_table`
  ADD PRIMARY KEY (`user_table_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `belong_to`
--
ALTER TABLE `belong_to`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=286;
--
-- AUTO_INCREMENT for table `categori`
--
ALTER TABLE `categori`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `decoration`
--
ALTER TABLE `decoration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `expired`
--
ALTER TABLE `expired`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `fitur`
--
ALTER TABLE `fitur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `order_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=285;
--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `singkronisasi`
--
ALTER TABLE `singkronisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_table`
--
ALTER TABLE `user_table`
  MODIFY `user_table_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
